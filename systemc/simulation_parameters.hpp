#ifndef _SIMULATION_PARAMETERS_
#define _SIMULATION_PARAMETERS_

#include <logitherm>

struct simulation_parameters
{
    std::string id;

    /* proc core merete, "margok" */
    // const unit::xy_length_t core_size = {525_um, 559_um};
    unit::length_t core_width = 525_um;
    unit::length_t core_height = 559_um;
    unit::length_t h_margin = 3.5_um;
    unit::length_t v_margin = 13_um;
    unit::length_t h_core_space = 3.5_um;
    unit::length_t v_core_space = 13_um;

    double h_core_count = 2.0;
    double v_core_count = 2.0;

    /* chip xy merete */
    layout::xy_length_t layout_size; //(h_margin*2.0+core_width*h_core_count+h_core_space*(h_core_count-1.0), v_margin*2.0+core_height*v_core_count+v_core_space*(v_core_count-1.0));

    /* lapka vastagsaga */
    unit::length_t die_thickness = 100_um;

    /* disszipalo layer vastagsaga */
    unit::length_t dissipating_layer_thickness = 0.3_um;
    unit::length_t insulating_layer_thickness = 0.5_um;

    /* co-szimulacio idolepteke */
    unit::time_t timestep = 1_ms;
    unit::frequency_t clock_frequency = 1_MHz;
    unit::time_t clock_period;
    unit::time_t simulation_time = 10_s;

    /* stopping criteria */
    std::pair<unit::power_t, unit::temperature_t> stopping_criterium = {1_nW, 1_mK};

    /* layer nevek */
    std::string dissipating_layer = "dissipating_layer";
    std::string insulating_layer = "insulating_layer";
    std::string bulk_layer = "bulk_layer";

    /* die anyag neve, hovezetese es hokapacitasa */
    // hovezetes -> Wm^-1K^-1
    // hokapacitas -> JKg^-1K^-1
    std::tuple<std::string,double,double> die_material = std::make_tuple("silicon", 156.3, 1.596e6);
    std::tuple<std::string,double,double> insulator_material = std::make_tuple("oxide", 1.4, 1.7e6);


    bool soi_die = false;

    /* bulk layerek szama,osszesen number_of_bulk_layers+1 reteg lesz */
    size_t number_of_bulk_layers = 1;
    // static_assert(number_of_bulk_layers > 3, "legalabb 4 bulk layer kell");
    
    unit::length_t bulk_layer_thickness; // = (die_thickness - dissipating_layer_thickness)/number_of_bulk_layers;

    /* 10W/m^2K természetes konvekcio hoatadasa levego kozeg eseten */
    double boundary_condition_value = 10.0;

    /* lapka xy felbontasa **/
    layout::xy_pitch_t layout_resolution = {200, 200};

    /* trace-k utvonala */
    std::string trace_path = "./traces/";

    /* konfiguracio */
    bool add_dissipators = true;
    bool trace_dissipation = true;
    bool trace_activity = false;
    bool trace_temperature = true;
    bool trace_layer_temperature = true;
    bool zero_delay = false;
    bool trace_critical_path = true;


    //Memory initializers
    std::array<sc_lv<16>, 256> program_initializer = {
        0x0000,
        0x0101,
        0x02FF,
        0x0304,
        0x0402,
        0x0503,
        0x1F00,
        0x1E01,
        0x3EE0,
        0x9020,
        0x0D00,
        0x0C00,
        0x0A00,
        0x3FF0,
        0xA002,
        0x8004,
        0x0D01,
        0x7FF2,
        0x3FF1,
        0x3EE0,
        0xA002,
        0x8004,
        0x0C01,
        0x7EE2,
        0x3EE1,
        0x4FFE,
        0xA003,
        0x3AA1,
        0x80FD,
        0x3FFE,
        0x48CD,
        0x9003,
        0x7AA2,
        0x3AA1,
        0x3DD0,
        0x9003,
        0x7FF2,
        0x3FF1,
        0x2A04,
        0x2F05,
        0xB000,
        0x2130,
        0x80FC 
    };

    std::array<sc_lv<8>, 256> data_initializer = {
        0x85,
        0xfc
    };

    simulation_parameters() = delete;
    simulation_parameters(const std::string& id);

    double get_wall_time() const;
    double get_cpu_time() const;
};

// extern std::unordered_map<std::string, simulation_parameters> parameters;
extern const simulation_parameters* parameter;

#endif //_SIMULATION_PARAMETERS_