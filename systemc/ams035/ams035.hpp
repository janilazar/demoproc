#ifndef _AMS_035_HPP_
#define _AMS_035_HPP_

#include <logitherm>
#include <cmath>

namespace ams035
{

	const unit::voltage_t vdd = 3.3_V;

	inline unit::energy_t interpolate(const unit::temperature_t temp, const unit::energy_t array[])
	{
		if((0_celsius < temp) && (temp < 80_celsius))
		{
			size_t index = static_cast<double>(temp - 0_celsius) / 10;

			/** interpolalunk a ket legkozelebbi pont kozott**/
			unit::energy_t gradient = (array[index+1] - array[index]) / 10;
			unit::energy_t result = gradient*fmod(static_cast<double>(temp-0_celsius),10.0) + array[index];
			if(result < 0_J) throw("Negative dissipation");
			return result;
		}
		else return (temp < 0_celsius) ? array[0] : array[8];
	}
	
	inline unit::current_t interpolate(const unit::temperature_t temp, const unit::current_t array[])
	{
		if((0_celsius < temp) && (temp < 80_celsius))
		{
			size_t index = static_cast<double>(temp - 0_celsius) / 10;

			/** interpolalunk a ket legkozelebbi pont kozott**/
			unit::current_t gradient = (array[index+1]-array[index]) / 10;
			unit::current_t result = gradient*fmod(static_cast<double>(temp-0_celsius),10.0) + array[index];
			if(result < 0_A) throw("Negative current");
			return result;
		}
		else return (temp < 0_celsius) ? array[0] : array[8];
	}
	
	inline unit::time_t interpolate(const unit::temperature_t temp, const unit::time_t array[])
	{
		if((0_celsius < temp) && (temp < 80_celsius))
		{
			size_t index = static_cast<double>(temp - 0_celsius) / 10;

			/** interpolalunk a ket legkozelebbi pont kozott**/
			unit::time_t gradient = (array[index+1]-array[index]) / 10;
			unit::time_t result = gradient*fmod(static_cast<double>(temp-0_celsius),10.0) + array[index];
			if(result < 0_s) throw("Negative delay");
			return result;
		}
		else return (temp < 0_celsius) ? array[0] : array[8];
	}

	SC_MODULE(nand20)
	{		
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		unit::energy_t q_neg_dissipation();

		nand20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(nand30)
	{
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_in<sc_logic> C;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;

		unit::energy_t q_pos_dissipation();
		unit::energy_t q_neg_dissipation();

		nand30(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(nand40)
	{
		
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_in<sc_logic>	C;
		sc_in<sc_logic>	D;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		unit::energy_t q_neg_dissipation();

		nand40(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};


	SC_MODULE(nor20)
	{
		
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		nor20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(nor30)
	{
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_in<sc_logic>	C;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
	
		nor30(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(nor40)
	{	
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_in<sc_logic>	C;
		sc_in<sc_logic>	D;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();

		nor40(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(xor20)
	{
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;

		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();

		xor20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(xor30)
	{
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_in<sc_logic>	C;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		xor30(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};


	SC_MODULE(xnr20)
	{
		sc_in<sc_logic>	A;
		sc_in<sc_logic>	B;
		sc_out<sc_logic> Q;

		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];

		void function();

		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();

		xnr20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(inv0)
	{
		sc_in<sc_logic> A;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		inv0(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(oai210)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		oai210(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(oai2110)
	{

		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		oai2110(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(oai220)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		oai220(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(oai310)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		oai310(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(aoi210)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		aoi210(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(aoi2110)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		aoi2110(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(aoi220)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
			
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
			
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		aoi220(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(aoi310)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		layout::sunred::component_t layout_item;
			
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
				
		aoi310(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	//d flip-flop
	SC_MODULE(df3)
	{
		sc_in<sc_logic> D;
		sc_in<bool> C;
		sc_out<sc_logic> Q;
		sc_out<sc_logic> QN;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::sc_delayed_signal<sc_logic> qn_delay;
		
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		logic::systemc::activity_t qn_pos;
		logic::systemc::activity_t qn_neg;
		logic::systemc::activity_t d_pos;
		logic::systemc::activity_t d_neg;
		logic::systemc::activity_t c_pos;
		logic::systemc::activity_t c_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		static const unit::energy_t qn_pos_energy[9];
		static const unit::energy_t qn_neg_energy[9];
		static const unit::energy_t d_pos_energy[9];
		static const unit::energy_t d_neg_energy[9];
		static const unit::energy_t c_pos_energy[9];
		static const unit::energy_t c_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		static const unit::time_t qn_pos_delay[9];
		static const unit::time_t qn_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		sc_time qn_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		unit::energy_t qn_pos_dissipation();
		
		unit::energy_t qn_neg_dissipation();
		
		unit::energy_t d_pos_dissipation();
		
		unit::energy_t d_neg_dissipation();
		
		unit::energy_t c_pos_dissipation();
		
		unit::energy_t c_neg_dissipation();
		
		df3(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	//d flip-flop
	SC_MODULE(dfe1)
	{
		sc_in<sc_logic> D;
		sc_in<sc_logic> E;
		sc_in<bool> C;
		sc_out<sc_logic> Q;
		sc_out<sc_logic> QN;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::sc_delayed_signal<sc_logic> qn_delay;
		
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		logic::systemc::activity_t qn_pos;
		logic::systemc::activity_t qn_neg;
		logic::systemc::activity_t e_pos;
		logic::systemc::activity_t e_neg;
		logic::systemc::activity_t d_pos;
		logic::systemc::activity_t d_neg;
		logic::systemc::activity_t c_pos;
		logic::systemc::activity_t c_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		static const unit::energy_t qn_pos_energy[9];
		static const unit::energy_t qn_neg_energy[9];
		static const unit::energy_t sample_pos_energy[9];
		static const unit::energy_t sample_neg_energy[9];
		static const unit::energy_t e_pos_energy[9];
		static const unit::energy_t e_neg_energy[9];
		static const unit::energy_t c_pos_energy[9];
		static const unit::energy_t c_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		static const unit::time_t qn_pos_delay[9];
		static const unit::time_t qn_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		sc_time qn_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		unit::energy_t qn_pos_dissipation();
		
		unit::energy_t qn_neg_dissipation();
		
		unit::energy_t d_pos_dissipation();
		
		unit::energy_t d_neg_dissipation();
		
		unit::energy_t e_pos_dissipation();
		
		unit::energy_t e_neg_dissipation();
		
		unit::energy_t c_pos_dissipation();
		
		unit::energy_t c_neg_dissipation();
		
		dfe1(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};

	SC_MODULE(imux20)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> S;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		logic::systemc::activity_t s_pos;
		logic::systemc::activity_t s_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		static const unit::energy_t s_pos_energy[9];
		static const unit::energy_t s_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		unit::energy_t s_pos_dissipation();
		
		unit::energy_t s_neg_dissipation();
		
		imux20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(mux21)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> S;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		logic::systemc::activity_t s_pos;
		logic::systemc::activity_t s_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		static const unit::energy_t s_pos_energy[9];
		static const unit::energy_t s_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		unit::energy_t s_pos_dissipation();
		
		unit::energy_t s_neg_dissipation();
		
		mux21(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(mux31)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> S0;
		sc_in<sc_logic> S1;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		logic::systemc::activity_t s0_pos;
		logic::systemc::activity_t s0_neg;
		logic::systemc::activity_t s1_pos;
		logic::systemc::activity_t s1_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		static const unit::energy_t s0_pos_energy[9];
		static const unit::energy_t s0_neg_energy[9];
		static const unit::energy_t s1_pos_energy[9];
		static const unit::energy_t s1_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
	
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		unit::energy_t s0_pos_dissipation();
		
		unit::energy_t s0_neg_dissipation();
		
		unit::energy_t s1_pos_dissipation();
		
		unit::energy_t s1_neg_dissipation();
		
		mux31(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(mux41)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> C;
		sc_in<sc_logic> D;
		sc_in<sc_logic> S0;
		sc_in<sc_logic> S1;
		sc_out<sc_logic> Q;
		
		logic::systemc::sc_delayed_signal<sc_logic> q_delay;
		logic::systemc::activity_t q_pos;
		logic::systemc::activity_t q_neg;
		logic::systemc::activity_t s0_pos;
		logic::systemc::activity_t s0_neg;
		logic::systemc::activity_t s1_pos;
		logic::systemc::activity_t s1_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t q_pos_energy[9];
		static const unit::energy_t q_neg_energy[9];
		static const unit::energy_t s0_pos_energy[9];
		static const unit::energy_t s0_neg_energy[9];
		static const unit::energy_t s1_pos_energy[9];
		static const unit::energy_t s1_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t q_pos_delay[9];
		static const unit::time_t q_neg_delay[9];
		
		void function();
		
		sc_time q_delay_func();
		
		unit::power_t component_static_dissipation() const override;
		
		unit::energy_t q_pos_dissipation();
		
		unit::energy_t q_neg_dissipation();
		
		unit::energy_t s0_pos_dissipation();
		
		unit::energy_t s0_neg_dissipation();
		
		unit::energy_t s1_pos_dissipation();
		
		unit::energy_t s1_neg_dissipation();
		
		mux41(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(add31)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_in<sc_logic> CI;
		sc_out<sc_logic> S;
		sc_out<sc_logic> CO;
		
		
		logic::systemc::sc_delayed_signal<sc_logic> s_delay;
		logic::systemc::sc_delayed_signal<sc_logic> co_delay;
		logic::systemc::activity_t s_pos;
		logic::systemc::activity_t s_neg;
		logic::systemc::activity_t co_pos;
		logic::systemc::activity_t co_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t s_pos_energy[9];
		static const unit::energy_t s_neg_energy[9];
		static const unit::energy_t co_pos_energy[9];
		static const unit::energy_t co_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t s_pos_delay[9];
		static const unit::time_t s_neg_delay[9];
		static const unit::time_t co_pos_delay[9];
		static const unit::time_t co_neg_delay[9];
		
		void function();
		
		sc_time s_delay_func();
		
		sc_time co_delay_func();
		
		unit::power_t component_static_dissipation() const override;
				
		unit::energy_t s_pos_dissipation();
		
		unit::energy_t s_neg_dissipation();
		
		unit::energy_t co_pos_dissipation();
		
		unit::energy_t co_neg_dissipation();
		
		add31(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
	SC_MODULE(add21)
	{
		sc_in<sc_logic> A;
		sc_in<sc_logic> B;
		sc_out<sc_logic> S;
		sc_out<sc_logic> CO;
		
		logic::systemc::sc_delayed_signal<sc_logic> s_delay;
		logic::systemc::sc_delayed_signal<sc_logic> co_delay;
		logic::systemc::activity_t s_pos;
		logic::systemc::activity_t s_neg;
		logic::systemc::activity_t co_pos;
		logic::systemc::activity_t co_neg;
		layout::sunred::component_t layout_item;
		
		static const unit::energy_t s_pos_energy[9];
		static const unit::energy_t s_neg_energy[9];
		static const unit::energy_t co_pos_energy[9];
		static const unit::energy_t co_neg_energy[9];
		
		static const unit::current_t leakage_current[9];
		
		static const unit::time_t s_pos_delay[9];
		static const unit::time_t s_neg_delay[9];
		static const unit::time_t co_pos_delay[9];
		static const unit::time_t co_neg_delay[9];
		
		void function();
		
		sc_time s_delay_func();
		
		sc_time co_delay_func();
		
		unit::power_t component_static_dissipation() const override;
				
		unit::energy_t s_pos_dissipation();
		
		unit::energy_t s_neg_dissipation();
		
		unit::energy_t co_pos_dissipation();
		
		unit::energy_t co_neg_dissipation();
		
		add21(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter = nullptr);
	};
	
}
#endif //_AMS_035_HPP_
