#ifndef _MUX2_8_HPP_
#define _MUX2_8_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035
{

	SC_MODULE(mux2_8)
	{
		sc_in<sc_lv<8>> input0, input1;
		sc_in<sc_logic> select;
		sc_out<sc_lv<8>> output;
	  
		sc_vector<sc_signal<sc_logic>> in0, in1, out;		
		
		layout::sunred::component_t layout_item;
		
		ams035::mux21 g137;
		ams035::mux21 g142;
		ams035::mux21 g143;
		ams035::mux21 g138;
		ams035::mux21 g141;
		ams035::mux21 g144;
		ams035::mux21 g139;
		ams035::mux21 g140;

		
		void write_in0()
		{
			for(size_t it = 0; it < in0.size(); ++it) in0[it] = input0.read()[it];
		}
		
		void write_in1()
		{
			for(size_t it = 0; it < in1.size(); ++it) in1[it] = input1.read()[it];
		}
	   
		void write_output()
		{
			sc_lv<8> tmp;
			for(size_t it = 0; it < out.size(); ++it) tmp[it] = out[it]; //ez itt nem lesz jo[it][0]
			output.write(tmp);
		}
				
		mux2_8(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			input0("input0"),
			input1("input1"),
			select("select"),
			output("output"),
			layout_item(id, layer_name, position, layout::xy_length_t(28_um,39_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			in0("in0",8),
			in1("in1",8),
			out("out",8),
			g137("g137", layer_name, layout::xy_length_t(0_um,0_um), &layout_item, systemc_adapter),
			g142("g142", layer_name, layout::xy_length_t(7_um,0_um), &layout_item, systemc_adapter),
			g143("g143", layer_name, layout::xy_length_t(14_um,0_um), &layout_item, systemc_adapter),
			g138("g138", layer_name, layout::xy_length_t(21_um,0_um), &layout_item, systemc_adapter),
			g141("g141", layer_name, layout::xy_length_t(0_um,26_um), &layout_item, systemc_adapter),
			g144("g144", layer_name, layout::xy_length_t(7_um,26_um), &layout_item, systemc_adapter),
			g139("g139", layer_name, layout::xy_length_t(14_um,26_um), &layout_item, systemc_adapter),
			g140("g140", layer_name, layout::xy_length_t(21_um,26_um), &layout_item, systemc_adapter)
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(g137.id);
				adapter->manager->add_dissipator_component(g142.id);
				adapter->manager->add_dissipator_component(g143.id);
				adapter->manager->add_dissipator_component(g138.id);
				adapter->manager->add_dissipator_component(g141.id);
				adapter->manager->add_dissipator_component(g144.id);
				adapter->manager->add_dissipator_component(g139.id);
				adapter->manager->add_dissipator_component(g140.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input0, parameter->id);
				add_activity_trace(parameter->trace_path, &input1, parameter->id);
				add_activity_trace(parameter->trace_path, &select, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(mux2_8);
			
			g137.A(in0[7]);
			g137.B(in1[7]);
			g137.S(select);
			g137.Q(out[7]);
			
			g142.A(in0[6]);
			g142.B(in1[6]);
			g142.S(select);
			g142.Q(out[6]);
			
			g143.A(in0[5]);
			g143.B(in1[5]);
			g143.S(select);
			g143.Q(out[5]);
			
			g138.A(in0[4]);
			g138.B(in1[4]);
			g138.S(select);
			g138.Q(out[4]);
			
			g141.A(in0[3]);
			g141.B(in1[3]);
			g141.S(select);
			g141.Q(out[3]);
			
			g144.A(in0[2]);
			g144.B(in1[2]);
			g144.S(select);
			g144.Q(out[2]);
			
			g139.A(in0[1]);
			g139.B(in1[1]);
			g139.S(select);
			g139.Q(out[1]);
			
			g140.A(in0[0]);
			g140.B(in1[0]);
			g140.S(select);
			g140.Q(out[0]);
			
			SC_METHOD(write_in0);
			sensitive << input0;
			dont_initialize();
			
			SC_METHOD(write_in1);
			sensitive << input1;
			dont_initialize();
			
			SC_METHOD(write_output);
			for(size_t it = 0; it < out.size(); ++it) sensitive << out[it];
			dont_initialize();
		}
	};
}	//nag140mespace demoproc_gates
#endif //_MUX2_8_HPP_
