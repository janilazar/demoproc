#ifndef _INSTRUCTION_DECODER_HPP_
#define _INSTRUCTION_DECODER_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

extern const simulation_parameters parameters;

namespace demoproc_ams035
{
	SC_MODULE(instruction_decoder)
	{
		sc_in<sc_lv<4>> opcode;
		sc_out<sc_lv<3>> alu_func;
		
		sc_vector<sc_signal<sc_logic>> opcode_bus;
		sc_vector<sc_signal<sc_logic>> alu_func_bus;
		
		sc_signal<sc_logic> n_0, n_1, n_2, n_4;
		
		layout::sunred::component_t layout_item;
		
		ams035::oai2110 g73;
		ams035::nand20 g75;
		ams035::nand20 g74;
		ams035::nor20 g76;
		ams035::xnr20 g78;
		ams035::nand30 g77;
		ams035::nor20 g79;
		
		void write_opcode_bus()
		{
			for(size_t it = 0; it < 4; ++it) opcode_bus[it] = opcode.read()[it];
		}
		
		void write_alu_func()
		{
			sc_lv<3> tmp;
			for(size_t it = 0; it < 3; ++it) tmp[it] = alu_func_bus[it];
			alu_func.write(tmp);
		}
		
		instruction_decoder(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			opcode("opcode"),
			alu_func("alu_func"),
			layout_item(id, layer_name, position, layout::xy_length_t(35_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			opcode_bus("opcode_bus", 4),
			alu_func_bus("alu_func_bus", 3),
			g73("g73", layer_name, layout::xy_length_t(0_um,0_um), &layout_item, systemc_adapter),
			g75("g75", layer_name, layout::xy_length_t(7_um,0_um), &layout_item, systemc_adapter),
			g74("g74", layer_name, layout::xy_length_t(10.5_um,0_um), &layout_item, systemc_adapter),
			g76("g76", layer_name, layout::xy_length_t(14_um,0_um), &layout_item, systemc_adapter),
			g78("g78", layer_name, layout::xy_length_t(17.5_um,0_um), &layout_item, systemc_adapter),
			g77("g77", layer_name, layout::xy_length_t(24.5_um,0_um), &layout_item, systemc_adapter),
			g79("g79", layer_name, layout::xy_length_t(31.5_um,0_um), &layout_item, systemc_adapter)
			

		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(g73.id);
				adapter->manager->add_dissipator_component(g75.id);
				adapter->manager->add_dissipator_component(g74.id);
				adapter->manager->add_dissipator_component(g76.id);
				adapter->manager->add_dissipator_component(g78.id);
				adapter->manager->add_dissipator_component(g77.id);
				adapter->manager->add_dissipator_component(g79.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &opcode, parameter->id);
				add_activity_trace(parameter->trace_path, &alu_func, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(instruction_decoder);
			
			g73.A(opcode_bus[0]);
			g73.B(opcode_bus[2]);
			g73.C(n_4);
			g73.D(n_1);
			g73.Q(alu_func_bus[2]);
			
			g75.A(n_2);
			g75.B(n_4);
			g75.Q(alu_func_bus[1]);
			
			g74.A(n_4);
			g74.B(opcode_bus[0]);
			g74.Q(alu_func_bus[0]);
			
			g76.A(opcode_bus[3]);
			g76.B(n_0);
			g76.Q(n_4);
			
			g78.A(opcode_bus[1]);
			g78.B(opcode_bus[0]);
			g78.Q(n_2);
			
			g77.A(opcode_bus[0]);
			g77.B(opcode_bus[2]);
			g77.C(opcode_bus[1]);
			g77.Q(n_1);
			
			g79.A(opcode_bus[1]);
			g79.B(opcode_bus[2]);
			g79.Q(n_0);
			
			SC_METHOD(write_opcode_bus);
			sensitive << opcode;
			dont_initialize();
			
			SC_METHOD(write_alu_func);
			for(size_t it = 0; it < 3; ++it) sensitive << alu_func_bus[it];
			dont_initialize();
		}
	};
}	//namespace demoproc_gates

#endif //_INSTRUCTION_DECODER_HPP_
