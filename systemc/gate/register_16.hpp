#ifndef _REGISTER_16_HPP_
#define _REGISTER_16_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035
{
	SC_MODULE(register_16)
	{
		sc_in<bool> clock;
		sc_in<sc_logic> reset, enable;
		sc_in<sc_lv<16>> input;
		sc_out<sc_lv<16>> output;
		
		sc_signal<sc_logic> UNCONNECTED0, UNCONNECTED1, UNCONNECTED2, UNCONNECTED3, UNCONNECTED4, UNCONNECTED5, UNCONNECTED6, UNCONNECTED7;
		sc_signal<sc_logic> UNCONNECTED8, UNCONNECTED9, UNCONNECTED10, UNCONNECTED11, UNCONNECTED12, UNCONNECTED13, UNCONNECTED14, UNCONNECTED15;
		
		sc_signal<sc_logic> n_0, n_1, n_2, n_3, n_4, n_5, n_6, n_7, n_8, n_9, n_10, n_11, n_12, n_13, n_14, n_15;
		sc_signal<sc_logic> n_16, n_17, n_18, n_19, n_20, n_21, n_22, n_23, n_24, n_25, n_26, n_27, n_28, n_29, n_30, n_31;
		
		sc_vector<sc_signal<sc_logic>> in, out;
		
		layout::sunred::component_t layout_item;
		
		ams035::df3 dout_reg0;
		ams035::df3 dout_reg1;
		ams035::df3 dout_reg2;
		ams035::df3 dout_reg3;
		ams035::df3 dout_reg4;
		ams035::df3 dout_reg5;
		ams035::df3 dout_reg6;
		ams035::df3 dout_reg7;
		ams035::df3 dout_reg8;
		ams035::df3 dout_reg9;
		ams035::df3 dout_reg10;
		ams035::df3 dout_reg11;
		ams035::df3 dout_reg12;
		ams035::df3 dout_reg13;
		ams035::df3 dout_reg14;
		ams035::df3 dout_reg15;
		
		ams035::nor20 nor_0;
		ams035::nor20 nor_1;
		ams035::nor20 nor_2;
		ams035::nor20 nor_3;
		ams035::nor20 nor_4;
		ams035::nor20 nor_5;
		ams035::nor20 nor_6;
		ams035::nor20 nor_7;
		ams035::nor20 nor_8;
		ams035::nor20 nor_9;
		ams035::nor20 nor_10;
		ams035::nor20 nor_11;
		ams035::nor20 nor_12;
		ams035::nor20 nor_13;
		ams035::nor20 nor_14;
		ams035::nor20 nor_15;
		
		ams035::imux20 imux_0;
		ams035::imux20 imux_1;
		ams035::imux20 imux_2;
		ams035::imux20 imux_3;
		ams035::imux20 imux_4;
		ams035::imux20 imux_5;
		ams035::imux20 imux_6;
		ams035::imux20 imux_7;
		ams035::imux20 imux_8;
		ams035::imux20 imux_9;
		ams035::imux20 imux_10;
		ams035::imux20 imux_11;
		ams035::imux20 imux_12;
		ams035::imux20 imux_13;
		ams035::imux20 imux_14;
		ams035::imux20 imux_15;
		
		
		void write_in()
		{
			for(size_t it = 0; it < in.size(); ++it) in[it] = input.read()[it];
		}
		
		void write_output()
		{
			sc_lv<16> tmp;
			for(size_t it = 0; it < out.size(); ++it) tmp[it] = out[it];
			output.write(tmp);
		}		
		
		register_16(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			layout_item(id, layer_name, position, layout::xy_length_t(84_um,143_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)), 
			in("in",16),
			out("out",16),
			dout_reg0("dout_reg0",   layer_name, layout::xy_length_t(0_um,0_um),  &layout_item, systemc_adapter),
			dout_reg1("dout_reg1",   layer_name, layout::xy_length_t(21_um,0_um),  &layout_item, systemc_adapter),
			dout_reg2("dout_reg2",   layer_name, layout::xy_length_t(42_um,0_um), &layout_item, systemc_adapter),
			dout_reg3("dout_reg3",   layer_name, layout::xy_length_t(63_um,0_um), &layout_item, systemc_adapter),
			dout_reg4("dout_reg4",   layer_name, layout::xy_length_t(0_um,26_um),  &layout_item, systemc_adapter),
			dout_reg5("dout_reg5",   layer_name, layout::xy_length_t(21_um,26_um),  &layout_item, systemc_adapter),
			dout_reg6("dout_reg6",   layer_name, layout::xy_length_t(42_um,26_um), &layout_item, systemc_adapter),
			dout_reg7("dout_reg7",   layer_name, layout::xy_length_t(63_um,26_um), &layout_item, systemc_adapter),
			dout_reg8("dout_reg8",   layer_name, layout::xy_length_t(0_um,52_um), &layout_item, systemc_adapter),
			dout_reg9("dout_reg9",   layer_name, layout::xy_length_t(21_um,52_um), &layout_item, systemc_adapter),
			dout_reg10("dout_reg10", layer_name, layout::xy_length_t(42_um,52_um), &layout_item, systemc_adapter),
			dout_reg11("dout_reg11", layer_name, layout::xy_length_t(63_um,52_um), &layout_item, systemc_adapter),
			dout_reg12("dout_reg12", layer_name, layout::xy_length_t(0_um,78_um),  &layout_item, systemc_adapter),
			dout_reg13("dout_reg13", layer_name, layout::xy_length_t(21_um,78_um),  &layout_item, systemc_adapter),
			dout_reg14("dout_reg14", layer_name, layout::xy_length_t(42_um,78_um), &layout_item, systemc_adapter),
			dout_reg15("dout_reg15", layer_name, layout::xy_length_t(63_um,78_um), &layout_item, systemc_adapter),
			
			nor_0("nor_0",   layer_name, layout::xy_length_t(56_um,104_um),    &layout_item, systemc_adapter),
			nor_1("nor_1",   layer_name, layout::xy_length_t(59.5_um,104_um),    &layout_item, systemc_adapter),
			nor_2("nor_2",   layer_name, layout::xy_length_t(63_um,104_um),    &layout_item, systemc_adapter),
			nor_3("nor_3",   layer_name, layout::xy_length_t(66.5_um,104_um),    &layout_item, systemc_adapter),
			nor_4("nor_4",   layer_name, layout::xy_length_t(70_um,104_um),    &layout_item, systemc_adapter),
			nor_5("nor_5",   layer_name, layout::xy_length_t(73.5_um,104_um),    &layout_item, systemc_adapter),
			nor_6("nor_6",   layer_name, layout::xy_length_t(77_um,104_um),    &layout_item, systemc_adapter),
			nor_7("nor_7",   layer_name, layout::xy_length_t(80.5_um,104_um),    &layout_item, systemc_adapter),
			nor_8("nor_8",   layer_name, layout::xy_length_t(56_um,130_um),   &layout_item, systemc_adapter),
			nor_9("nor_9",   layer_name, layout::xy_length_t(59.5_um,130_um),   &layout_item, systemc_adapter),
			nor_10("nor_10", layer_name, layout::xy_length_t(63_um,130_um),   &layout_item, systemc_adapter),
			nor_11("nor_11", layer_name, layout::xy_length_t(66.5_um,130_um),   &layout_item, systemc_adapter),
			nor_12("nor_12", layer_name, layout::xy_length_t(70_um,130_um),   &layout_item, systemc_adapter),
			nor_13("nor_13", layer_name, layout::xy_length_t(73.5_um,130_um),   &layout_item, systemc_adapter),
			nor_14("nor_14", layer_name, layout::xy_length_t(77_um,130_um),   &layout_item, systemc_adapter),
			nor_15("nor_15", layer_name, layout::xy_length_t(80.5_um,130_um),   &layout_item, systemc_adapter),
			
			imux_0("imux_0",   layer_name, layout::xy_length_t(0_um,104_um),   &layout_item, systemc_adapter),
			imux_1("imux_1",   layer_name, layout::xy_length_t(7_um,104_um),   &layout_item, systemc_adapter),
			imux_2("imux_2",   layer_name, layout::xy_length_t(14_um,104_um),   &layout_item, systemc_adapter),
			imux_3("imux_3",   layer_name, layout::xy_length_t(21_um,104_um),   &layout_item, systemc_adapter),
			imux_4("imux_4",   layer_name, layout::xy_length_t(28_um,104_um),   &layout_item, systemc_adapter),
			imux_5("imux_5",   layer_name, layout::xy_length_t(35_um,104_um),  &layout_item, systemc_adapter),
			imux_6("imux_6",   layer_name, layout::xy_length_t(42_um,104_um),  &layout_item, systemc_adapter),
			imux_7("imux_7",   layer_name, layout::xy_length_t(49_um,104_um),  &layout_item, systemc_adapter),
			imux_8("imux_8",   layer_name, layout::xy_length_t(56_um,130_um), &layout_item, systemc_adapter),
			imux_9("imux_9",   layer_name, layout::xy_length_t(59.5_um,130_um), &layout_item, systemc_adapter),
			imux_10("imux_10", layer_name, layout::xy_length_t(63_um,130_um), &layout_item, systemc_adapter),
			imux_11("imux_11", layer_name, layout::xy_length_t(66.5_um,130_um), &layout_item, systemc_adapter),
			imux_12("imux_12", layer_name, layout::xy_length_t(70_um,130_um), &layout_item, systemc_adapter),
			imux_13("imux_13", layer_name, layout::xy_length_t(73.5_um,130_um), &layout_item, systemc_adapter),
			imux_14("imux_14", layer_name, layout::xy_length_t(77_um,130_um), &layout_item, systemc_adapter),
			imux_15("imux_15", layer_name, layout::xy_length_t(80.5_um,130_um), &layout_item, systemc_adapter)
			
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(dout_reg0.id);
				adapter->manager->add_dissipator_component(dout_reg1.id);
				adapter->manager->add_dissipator_component(dout_reg2.id);
				adapter->manager->add_dissipator_component(dout_reg3.id);
				adapter->manager->add_dissipator_component(dout_reg4.id);
				adapter->manager->add_dissipator_component(dout_reg5.id);
				adapter->manager->add_dissipator_component(dout_reg6.id);
				adapter->manager->add_dissipator_component(dout_reg7.id);
				adapter->manager->add_dissipator_component(dout_reg8.id);
				adapter->manager->add_dissipator_component(dout_reg9.id);
				adapter->manager->add_dissipator_component(dout_reg10.id);
				adapter->manager->add_dissipator_component(dout_reg11.id);
				adapter->manager->add_dissipator_component(dout_reg12.id);
				adapter->manager->add_dissipator_component(dout_reg13.id);
				adapter->manager->add_dissipator_component(dout_reg14.id);
				adapter->manager->add_dissipator_component(dout_reg15.id);
				
				adapter->manager->add_dissipator_component(nor_0.id);
				adapter->manager->add_dissipator_component(nor_1.id);
				adapter->manager->add_dissipator_component(nor_2.id);
				adapter->manager->add_dissipator_component(nor_3.id);
				adapter->manager->add_dissipator_component(nor_4.id);
				adapter->manager->add_dissipator_component(nor_5.id);
				adapter->manager->add_dissipator_component(nor_6.id);
				adapter->manager->add_dissipator_component(nor_7.id);
				adapter->manager->add_dissipator_component(nor_8.id);
				adapter->manager->add_dissipator_component(nor_9.id);
				adapter->manager->add_dissipator_component(nor_10.id);
				adapter->manager->add_dissipator_component(nor_11.id);
				adapter->manager->add_dissipator_component(nor_12.id);
				adapter->manager->add_dissipator_component(nor_13.id);
				adapter->manager->add_dissipator_component(nor_14.id);
				adapter->manager->add_dissipator_component(nor_15.id);
				
				adapter->manager->add_dissipator_component(imux_0.id);
				adapter->manager->add_dissipator_component(imux_1.id);
				adapter->manager->add_dissipator_component(imux_2.id);
				adapter->manager->add_dissipator_component(imux_3.id);
				adapter->manager->add_dissipator_component(imux_4.id);
				adapter->manager->add_dissipator_component(imux_5.id);
				adapter->manager->add_dissipator_component(imux_6.id);
				adapter->manager->add_dissipator_component(imux_7.id);
				adapter->manager->add_dissipator_component(imux_8.id);
				adapter->manager->add_dissipator_component(imux_9.id);
				adapter->manager->add_dissipator_component(imux_10.id);
				adapter->manager->add_dissipator_component(imux_11.id);
				adapter->manager->add_dissipator_component(imux_12.id);
				adapter->manager->add_dissipator_component(imux_13.id);
				adapter->manager->add_dissipator_component(imux_14.id);
				adapter->manager->add_dissipator_component(imux_15.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_16);
			
			dout_reg0.C (clock);
			dout_reg0.D (n_0);
			dout_reg0.Q (out[0]);
			dout_reg0.QN(UNCONNECTED0);

			dout_reg1.C (clock);
			dout_reg1.D (n_1);
			dout_reg1.Q (out[1]);
			dout_reg1.QN(UNCONNECTED1);

			dout_reg2.C (clock);
			dout_reg2.D (n_2);
			dout_reg2.Q (out[2]);
			dout_reg2.QN(UNCONNECTED2);

			dout_reg3.C (clock);
			dout_reg3.D (n_3);
			dout_reg3.Q (out[3]);
			dout_reg3.QN(UNCONNECTED3);

			dout_reg4.C (clock);
			dout_reg4.D (n_4);
			dout_reg4.Q (out[4]);
			dout_reg4.QN(UNCONNECTED4);

			dout_reg5.C (clock);
			dout_reg5.D (n_5);
			dout_reg5.Q (out[5]);
			dout_reg5.QN(UNCONNECTED5);

			dout_reg6.C (clock);
			dout_reg6.D (n_6);
			dout_reg6.Q (out[6]);
			dout_reg6.QN(UNCONNECTED6);

			dout_reg7.C (clock);
			dout_reg7.D (n_7);
			dout_reg7.Q (out[7]);
			dout_reg7.QN(UNCONNECTED7);

			dout_reg8.C (clock);
			dout_reg8.D (n_8);
			dout_reg8.Q (out[8]);
			dout_reg8.QN(UNCONNECTED8);

			dout_reg9.C (clock);
			dout_reg9.D (n_9);
			dout_reg9.Q (out[9]);
			dout_reg9.QN(UNCONNECTED9);

			dout_reg10.C (clock);
			dout_reg10.D (n_10);
			dout_reg10.Q (out[10]);
			dout_reg10.QN(UNCONNECTED10);

			dout_reg11.C (clock);
			dout_reg11.D (n_11);
			dout_reg11.Q (out[11]);
			dout_reg11.QN(UNCONNECTED11);

			dout_reg12.C (clock);
			dout_reg12.D (n_12);
			dout_reg12.Q (out[12]);
			dout_reg12.QN(UNCONNECTED12);

			dout_reg13.C (clock);
			dout_reg13.D (n_13);
			dout_reg13.Q (out[13]);
			dout_reg13.QN(UNCONNECTED13);

			dout_reg14.C (clock);
			dout_reg14.D (n_14);
			dout_reg14.Q (out[14]);
			dout_reg14.QN(UNCONNECTED14);

			dout_reg15.C (clock);
			dout_reg15.D (n_15);
			dout_reg15.Q (out[15]);
			dout_reg15.QN(UNCONNECTED15);

			nor_0.A (reset);
			nor_0.B (n_16);
			nor_0.Q (n_0);

			nor_1.A (reset);
			nor_1.B (n_17);
			nor_1.Q (n_1);

			nor_2.A (reset);
			nor_2.B (n_18);
			nor_2.Q (n_2);

			nor_3.A (reset);
			nor_3.B (n_19);
			nor_3.Q (n_3);

			nor_4.A (reset);
			nor_4.B (n_20);
			nor_4.Q (n_4);

			nor_5.A (reset);
			nor_5.B (n_21);
			nor_5.Q (n_5);

			nor_6.A (reset);
			nor_6.B (n_22);
			nor_6.Q (n_6);

			nor_7.A (reset);
			nor_7.B (n_23);
			nor_7.Q (n_7);

			nor_8.A (reset);
			nor_8.B (n_24);
			nor_8.Q (n_8);

			nor_9.A (reset);
			nor_9.B (n_25);
			nor_9.Q (n_9);

			nor_10.A (reset);
			nor_10.B (n_26);
			nor_10.Q (n_10);

			nor_11.A (reset);
			nor_11.B (n_27);
			nor_11.Q (n_11);

			nor_12.A (reset);
			nor_12.B (n_28);
			nor_12.Q (n_12);

			nor_13.A (reset);
			nor_13.B (n_29);
			nor_13.Q (n_13);

			nor_14.A (reset);
			nor_14.B (n_30);
			nor_14.Q (n_14);

			nor_15.A (reset);
			nor_15.B (n_31);
			nor_15.Q (n_15);

			imux_0.A (out[0]);
			imux_0.B (in[0]);
			imux_0.S (enable);
			imux_0.Q (n_16);

			imux_1.A (out[1]);
			imux_1.B (in[1]);
			imux_1.S (enable);
			imux_1.Q (n_17);

			imux_2.A (out[2]);
			imux_2.B (in[2]);
			imux_2.S (enable);
			imux_2.Q (n_18);

			imux_3.A (out[3]);
			imux_3.B (in[3]);
			imux_3.S (enable);
			imux_3.Q (n_19);

			imux_4.A (out[4]);
			imux_4.B (in[4]);
			imux_4.S (enable);
			imux_4.Q (n_20);

			imux_5.A (out[5]);
			imux_5.B (in[5]);
			imux_5.S (enable);
			imux_5.Q (n_21);

			imux_6.A (out[6]);
			imux_6.B (in[6]);
			imux_6.S (enable);
			imux_6.Q (n_22);

			imux_7.A (out[7]);
			imux_7.B (in[7]);
			imux_7.S (enable);
			imux_7.Q (n_23);

			imux_8.A (out[8]);
			imux_8.B (in[8]);
			imux_8.S (enable);
			imux_8.Q (n_24);

			imux_9.A (out[9]);
			imux_9.B (in[9]);
			imux_9.S (enable);
			imux_9.Q (n_25);

			imux_10.A (out[10]);
			imux_10.B (in[10]);
			imux_10.S (enable);
			imux_10.Q (n_26);

			imux_11.A (out[11]);
			imux_11.B (in[11]);
			imux_11.S (enable);
			imux_11.Q (n_27);

			imux_12.A (out[12]);
			imux_12.B (in[12]);
			imux_12.S (enable);
			imux_12.Q (n_28);

			imux_13.A (out[13]);
			imux_13.B (in[13]);
			imux_13.S (enable);
			imux_13.Q (n_29);

			imux_14.A (out[14]);
			imux_14.B (in[14]);
			imux_14.S (enable);
			imux_14.Q (n_30);

			imux_15.A (out[15]);
			imux_15.B (in[15]);
			imux_15.S (enable);
			imux_15.Q (n_31);

			SC_METHOD(write_in);
			sensitive << input;
			dont_initialize();
			
			SC_METHOD(write_output);
			for(size_t it = 0; it < out.size(); ++it) sensitive << out[it];
			dont_initialize();
		
		}
	};
}	//namespaenable demoproc_ams035
#endif //_REGISTER_16_HPP_

