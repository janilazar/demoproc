#ifndef _ADDER_8_HPP_
#define _ADDER_8_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035
{

	SC_MODULE(adder_8)
	{
		
		sc_in<sc_lv<8>> input0, input1;
		sc_out<sc_lv<8>> result;
		
		sc_vector<sc_signal<sc_logic>> in0, in1, sum;

		sc_signal<sc_logic> n_0, n_2, n_4, n_6, n_8, n_10, n_12;
		
		layout::sunred::component_t layout_item;
		
		ams035::xor30 g433;
		ams035::add31 g434;
		ams035::add31 g435;
		ams035::add31 g436;
		ams035::add31 g437;
		ams035::add31 g438;
		ams035::add31 g439;
		ams035::add21 g440;
		
		void write_in0()
		{
			for(size_t it = 0; it < in0.size(); ++it) in0[it] = input0.read()[it];
		}
		
		void write_in1()
		{
			for(size_t it = 0; it < in1.size(); ++it) in1[it] = input1.read()[it];
		}
		
		void write_result()
		{
			sc_lv<8> tmp;
			for(size_t it = 0; it < sum.size(); ++it) tmp[it] = sum[it];
			result.write(tmp);
		}
		
		
		adder_8(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			input0("input0"),
			input1("input1"),
			result("result"),
			layout_item(id, layer_name, position, layout::xy_length_t(56_um,65_um),parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			in0("in0",8),
			in1("in1",8),
			sum("sum",8),
			g433("g433", layer_name, layout::xy_length_t(0_um,0_um),	&layout_item, systemc_adapter),
			g434("g434", layer_name, layout::xy_length_t(14_um,0_um),	&layout_item, systemc_adapter),
			g435("g435", layer_name, layout::xy_length_t(35_um,0_um),&layout_item, systemc_adapter),
			g436("g436", layer_name, layout::xy_length_t(0_um,26_um),	&layout_item, systemc_adapter),
			g437("g437", layer_name, layout::xy_length_t(21_um,26_um),	&layout_item, systemc_adapter),
			g438("g438", layer_name, layout::xy_length_t(0_um,52_um),	&layout_item, systemc_adapter),
			g439("g439", layer_name, layout::xy_length_t(21_um,52_um),	&layout_item, systemc_adapter),
			g440("g440", layer_name, layout::xy_length_t(42_um,52_um),&layout_item, systemc_adapter)
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(g433.id);
				adapter->manager->add_dissipator_component(g434.id);
				adapter->manager->add_dissipator_component(g435.id);
				adapter->manager->add_dissipator_component(g436.id);
				adapter->manager->add_dissipator_component(g437.id);
				adapter->manager->add_dissipator_component(g438.id);
				adapter->manager->add_dissipator_component(g439.id);
				adapter->manager->add_dissipator_component(g440.id);
				adapter->manager->add_display_component(id);
			}

			if(parameter->trace_activity)	
			{
				add_activity_trace(parameter->trace_path, &input0, parameter->id);
				add_activity_trace(parameter->trace_path, &input1, parameter->id);
				add_activity_trace(parameter->trace_path, &result, parameter->id);
			}

			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(adder_8);
			
			g433.A(in1[7]);
			g433.B(in0[7]);
			g433.C(n_12);
			g433.Q(sum[7]);
			
			g434.A(in1[6]);
			g434.B(in0[6]);
			g434.CI(n_10);
			g434.S(sum[6]);
			g434.CO(n_12);
			
			g435.A(in1[5]);
			g435.B(in0[5]);
			g435.CI(n_8);
			g435.S(sum[5]);
			g435.CO(n_10);
			
			g436.A(in1[4]);
			g436.B(in0[4]);
			g436.CI(n_6);
			g436.S(sum[4]);
			g436.CO(n_8);
			
			g437.A(in1[3]);
			g437.B(in0[3]);
			g437.CI(n_4);
			g437.S(sum[3]);
			g437.CO(n_6);
			
			g438.A(in1[2]);
			g438.B(in0[2]);
			g438.CI(n_2);
			g438.S(sum[2]);
			g438.CO(n_4);
			
			g439.A(in1[1]);
			g439.B(in0[1]);
			g439.CI(n_0);
			g439.S(sum[1]);
			g439.CO(n_2);
			
			g440.A(in1[0]);
			g440.B(in0[0]);
			g440.S(sum[0]);
			g440.CO(n_0);
			
			SC_METHOD(write_in0);
			sensitive << input0;
			dont_initialize();
			
			SC_METHOD(write_in1);
			sensitive << input1;
			dont_initialize();
			
			SC_METHOD(write_result);
			for(size_t it = 0; it < 8; ++it) sensitive << sum[it];
			dont_initialize();
		}
	};
}	//namespace demoproc_gates

#endif //_ADDER_8_HPP_
