#ifndef _REGISTER_4_HPP_
#define _REGISTER_4_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035{
	
	SC_MODULE(register_4){
		sc_in<bool> clock;
		sc_in<sc_logic> reset, enable;
		sc_in<sc_lv<4>> input;
		sc_out<sc_lv<4>> output;
		
		sc_signal<sc_logic> UNCONNECTED, UNCONNECTED0, UNCONNECTED1, UNCONNECTED3;
		
		sc_signal<sc_logic> n_0, n_1, n_3,  n_6,  n_8, n_10, n_11, n_14;
		
		sc_vector<sc_signal<sc_logic>> in, out;
		
		layout::sunred::component_t layout_item;
		
		ams035::df3 dout_reg0;
		ams035::df3 dout_reg1;
		ams035::df3 dout_reg2;
		ams035::df3 dout_reg3;
		ams035::nor20 nor_0;
		ams035::nor20 nor_1;
		ams035::nor20 nor_2;
		ams035::nor20 nor_3;
		ams035::imux20 imux_0;
		ams035::imux20 imux_1;
		ams035::imux20 imux_2;
		ams035::imux20 imux_3;
		
		void write_in()
		{
			for(size_t it = 0; it < in.size(); ++it) in[it] = input.read()[it];
		}
		
		void write_output()
		{
			sc_lv<4> tmp;
			for(size_t it = 0; it < out.size(); ++it) tmp[it] = out[it];
			output.write(tmp);
		}
		
		register_4(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			layout_item(id, layer_name, position, layout::xy_length_t(42_um,65_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			in("in",4),
			out("out",4),
			dout_reg0("dout_reg0", layer_name, layout::xy_length_t(0_um,0_um), &layout_item, systemc_adapter),
			dout_reg1("dout_reg1", layer_name, layout::xy_length_t(21_um,0_um), &layout_item, systemc_adapter),
			dout_reg2("dout_reg2", layer_name, layout::xy_length_t(0_um,26_um), &layout_item, systemc_adapter),
			dout_reg3("dout_reg3", layer_name, layout::xy_length_t(21_um,26_um), &layout_item, systemc_adapter),
			nor_0("nor_0", layer_name, layout::xy_length_t(0_um,52_um), &layout_item, systemc_adapter),
			nor_1("nor_1", layer_name, layout::xy_length_t(3.5_um,52_um), &layout_item, systemc_adapter),
			nor_2("nor_2", layer_name, layout::xy_length_t(7_um,52_um), &layout_item, systemc_adapter),
			nor_3("nor_3", layer_name, layout::xy_length_t(10.5_um,52_um), &layout_item, systemc_adapter),
			imux_0("imux_0", layer_name, layout::xy_length_t(14_um,52_um), &layout_item, systemc_adapter),
			imux_1("imux_1", layer_name, layout::xy_length_t(21_um,52_um), &layout_item, systemc_adapter),
			imux_2("imux_2", layer_name, layout::xy_length_t(28_um,52_um), &layout_item, systemc_adapter),
			imux_3("imux_3", layer_name, layout::xy_length_t(35_um,52_um), &layout_item, systemc_adapter)
			
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(dout_reg0.id);
				adapter->manager->add_dissipator_component(dout_reg1.id);
				adapter->manager->add_dissipator_component(dout_reg2.id);
				adapter->manager->add_dissipator_component(dout_reg3.id);
				
				adapter->manager->add_dissipator_component(nor_0.id);
				adapter->manager->add_dissipator_component(nor_1.id);
				adapter->manager->add_dissipator_component(nor_2.id);
				adapter->manager->add_dissipator_component(nor_3.id);
				
				adapter->manager->add_dissipator_component(imux_0.id);
				adapter->manager->add_dissipator_component(imux_1.id);
				adapter->manager->add_dissipator_component(imux_2.id);
				adapter->manager->add_dissipator_component(imux_3.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_4);
			
			dout_reg0.C (clock);
			dout_reg0.D (n_11);
			dout_reg0.Q (out[0]);
			dout_reg0.QN(UNCONNECTED);

			dout_reg2.C (clock);
			dout_reg2.D (n_10);
			dout_reg2.Q (out[2]);
			dout_reg2.QN(UNCONNECTED0);

			dout_reg3.C (clock);
			dout_reg3.D (n_14);
			dout_reg3.Q (out[3]);
			dout_reg3.QN(UNCONNECTED1);

			dout_reg1.C (clock);
			dout_reg1.D (n_8);
			dout_reg1.Q (out[1]);
			dout_reg1.QN(UNCONNECTED3);

			nor_3.A (reset);
			nor_3.B (n_0);
			nor_3.Q (n_14);

			nor_0.A (reset);
			nor_0.B (n_6);
			nor_0.Q (n_11);

			nor_2.A (reset);
			nor_2.B (n_1);
			nor_2.Q (n_10);

			nor_1.A (reset);
			nor_1.B (n_3);
			nor_1.Q (n_8);

			imux_0.A (out[0]);
			imux_0.B (in[0]);
			imux_0.S (enable);
			imux_0.Q (n_6);

			imux_1.A (out[1]);
			imux_1.B (in[1]);
			imux_1.S (enable);
			imux_1.Q (n_3);

			imux_2.A (out[2]);
			imux_2.B (in[2]);
			imux_2.S (enable);
			imux_2.Q (n_1);

			imux_3.A (out[3]);
			imux_3.B (in[3]);
			imux_3.S (enable);
			imux_3.Q (n_0);


			SC_METHOD(write_in);
			sensitive << input;
			dont_initialize();
			
			SC_METHOD(write_output);
			for(size_t it = 0; it < out.size(); ++it) sensitive << out[it];
			dont_initialize();
		
		}
	};
	
}	//namespace demoproc_ams035
#endif //_REGISTER_8_HPP_

