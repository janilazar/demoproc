#ifndef _ALU_HPP_
#define _ALU_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "mux2_8.hpp"
#include "mux4_8.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035
{

	SC_MODULE(alu_slice)
	{
		
		sc_in<sc_logic> cin;
		sc_in<sc_logic> sub;
		sc_in<sc_logic> ina;
		sc_in<sc_logic> inb;
		sc_out<sc_logic> axorb;
		sc_out<sc_logic> aorb;
		sc_out<sc_logic> aandb;
		sc_out<sc_logic> sumdiff;
		sc_out<sc_logic> cout;
		
		layout::sunred::component_t layout_item;
		
		ams035::xor20 xor_1, xor_2, xor_3;
		ams035::nor20 nor_1, nor_2;
		ams035::inv0 inv_1, inv_2, inv_3, inv_4;
		ams035::nand20 nand_1, nand_2;
		
		sc_signal<sc_logic> xor_1_out, xor_2_out, xor_3_out;
		sc_signal<sc_logic> nor_1_out, nor_2_out;
		sc_signal<sc_logic> inv_1_out, inv_2_out, inv_3_out, inv_4_out;
		sc_signal<sc_logic> nand_1_out, nand_2_out;
		

		
		void write_axorb_out()
		{
			axorb.write(xor_2_out);
		}
		
		void write_aandb_out()
		{
			aandb.write(inv_2_out);
		}
		
		//constructor plz
		alu_slice(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			layout_item(id, layer_name, position, layout::xy_length_t(31.5_um,39_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			xor_1("xor_1",	layer_name, layout::xy_length_t(0_um,0_um), &layout_item, systemc_adapter),
			xor_2("xor_2",	layer_name, layout::xy_length_t(10.5_um,0_um), &layout_item, systemc_adapter),
			xor_3("xor_3",	layer_name, layout::xy_length_t(21_um,0_um), &layout_item, systemc_adapter),
			nor_1("nor_1",	layer_name, layout::xy_length_t(0_um,26_um), &layout_item, systemc_adapter),
			nor_2("nor_2",	layer_name, layout::xy_length_t(3.5_um,26_um), &layout_item, systemc_adapter),
			inv_1("inv_1",	layer_name, layout::xy_length_t(7_um,26_um), &layout_item, systemc_adapter),
			inv_2("inv_2",	layer_name, layout::xy_length_t(10.5_um,26_um), &layout_item, systemc_adapter),
			inv_3("inv_3",	layer_name, layout::xy_length_t(14_um,26_um), &layout_item, systemc_adapter),
			inv_4("inv_4",	layer_name, layout::xy_length_t(17.5_um,26_um), &layout_item, systemc_adapter),
			nand_1("nand_1",layer_name,	layout::xy_length_t(21_um,26_um), &layout_item, systemc_adapter),
			nand_2("nand_2",layer_name,	layout::xy_length_t(24.5_um,26_um), &layout_item, systemc_adapter)
			
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(xor_1.id);
				adapter->manager->add_dissipator_component(xor_2.id);
				adapter->manager->add_dissipator_component(xor_3.id);
				adapter->manager->add_dissipator_component(nor_1.id);
				adapter->manager->add_dissipator_component(nor_2.id);
				adapter->manager->add_dissipator_component(inv_1.id);
				adapter->manager->add_dissipator_component(inv_2.id);
				adapter->manager->add_dissipator_component(inv_3.id);
				adapter->manager->add_dissipator_component(inv_4.id);
				adapter->manager->add_dissipator_component(nand_1.id);
				adapter->manager->add_dissipator_component(nand_2.id);
			}
			
			SC_HAS_PROCESS(alu_slice);
			
			xor_1.A(sub);
			xor_1.B(inb);
			xor_1.Q(xor_1_out);
			
			xor_2.A(xor_1_out);
			xor_2.B(ina);
			xor_2.Q(xor_2_out);
			
			xor_3.A(xor_2_out);
			xor_3.B(cin);
			xor_3.Q(sumdiff); //repul a nehez ko, ki tudja hol all meg
			
			nand_1.A(cin);
			nand_1.B(xor_2_out);
			nand_1.Q(nand_1_out);
			
			inv_1.A(nand_1_out);
			inv_1.Q(inv_1_out);
			
			nand_2.A(xor_1_out);
			nand_2.B(ina);
			nand_2.Q(nand_2_out);
			
			inv_2.A(nand_2_out);
			inv_2.Q(inv_2_out);
			
			nor_1.A(ina);
			nor_1.B(inb);
			nor_1.Q(nor_1_out);
			
			inv_3.A(nor_1_out);
			inv_3.Q(aorb);
			
			nor_2.A(inv_1_out);
			nor_2.B(inv_2_out);
			nor_2.Q(nor_2_out);
			
			inv_4.A(nor_2_out);
			inv_4.Q(cout);
			
			
			SC_METHOD(write_axorb_out);
			sensitive << xor_2_out;
			dont_initialize();
			
			SC_METHOD(write_aandb_out);
			sensitive << inv_2_out;
			dont_initialize();
		}
		
	};

	SC_MODULE(alu_8)
	{
		
		sc_in<sc_lv<8>> inputa;
		sc_in<sc_lv<8>> inputb;
		sc_in<sc_lv<3>> alu_function;
		sc_out<sc_lv<8>> output;
		sc_out<sc_logic> z_out;
		sc_out<sc_logic> n_out;
		
		sc_vector<sc_signal<sc_logic>> ina;
		sc_vector<sc_signal<sc_logic>> inb;
		sc_vector<sc_signal<sc_logic>> func;
		sc_vector<sc_signal<sc_logic>> axorb;
		sc_vector<sc_signal<sc_logic>> aorb;
		sc_vector<sc_signal<sc_logic>> aandb;
		sc_vector<sc_signal<sc_logic>> sumdiff;
		sc_vector<sc_signal<sc_logic>> result;
		sc_vector<sc_signal<sc_logic>> cout;
		
		sc_signal<sc_lv<8>> axorb_bus;
		sc_signal<sc_lv<8>> aorb_bus;
		sc_signal<sc_lv<8>> aandb_bus;
		sc_signal<sc_lv<8>> sumdiff_bus;
		sc_signal<sc_lv<8>> result_bus;
			
		sc_signal<sc_lv<2>>	func21;
		sc_signal<sc_logic>	func0;
		
		layout::sunred::component_t layout_item;
		
		alu_slice alu_slice0;
		alu_slice alu_slice1;
		alu_slice alu_slice2;
		alu_slice alu_slice3;
		alu_slice alu_slice4;
		alu_slice alu_slice5;
		alu_slice alu_slice6;
		alu_slice alu_slice7;
		
		ams035::nand30 nand3_1;
		ams035::inv0 inv_1;
		sc_signal<sc_logic> nand3_1_out;
		sc_signal<sc_logic> inv_1_out;
		
		ams035::inv0 inv_2, inv_3;
		sc_signal<sc_logic> inv_2_out, inv_3_out;

		ams035::nor40 nor4_1, nor4_2;
		sc_signal<sc_logic> nor4_1_out, nor4_2_out;
		
		ams035::nand20 nand2_1;
		sc_signal<sc_logic> nand2_1_out;
		ams035::inv0 inv_4;
		
		demoproc_ams035::mux2_8 mux2_1, mux2_2;
		demoproc_ams035::mux4_8 mux4_1;
		sc_signal<sc_lv<8>> mux2_1_output;
		sc_signal<sc_lv<8>> mux2_2_output;
		
				
		void write_ina()
		{
			for(size_t it = 0; it < ina.size(); ++it) ina[it] = inputa.read()[it];	//lehet, hogy nem lesz jo (tipussal problemak)
		}

		void write_inb()
		{
			for(size_t it = 0; it < inb.size(); ++it) inb[it] = inputb.read()[it]; 
		}
		
		void write_func()
		{
			func21.write(alu_function.read().range(2,1));
			func0.write(alu_function.read()[0]);
			for(size_t it = 0; it < 3; ++it) func[it].write(alu_function.read()[it]);
		}

		void write_axorb_bus()
		{
			sc_lv<8> tmp;
			for(size_t it = 0; it < axorb.size(); ++it) tmp[it] = axorb[it]; 
			axorb_bus.write(tmp);
		}
		
		void write_aorb_bus()
		{
			sc_lv<8> tmp;
			for(size_t it = 0; it < aorb.size(); ++it) tmp[it] = aorb[it];
			aorb_bus.write(tmp);		
		}
		
		void write_aandb_bus(){
			sc_lv<8> tmp;
			for(size_t it = 0; it < aandb.size(); ++it) tmp[it] = aandb[it];
			aandb_bus.write(tmp);
		}
		
		void write_sumdiff_bus()
		{
			sc_lv<8> tmp;
			for(size_t it = 0; it < sumdiff.size(); ++it) tmp[it] = sumdiff[it];
			sumdiff_bus.write(tmp);
		}

		void write_result()
		{
			for(size_t it = 0; it < result.size(); ++it) result[it].write(result_bus.read()[it]);
			n_out.write(result_bus.read()[result.size()-1]);
			output.write(result_bus.read());
		}
		
		//constructor plz
		alu_8(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			inputa("inputa"),
			inputb("inputb"),
			alu_function("alu_function"),
			output("output"),
			z_out("z_out"),
			n_out("n_out"),			
			ina("ina",8),
			inb("inb",8),
			func("func",3),
			axorb("axorb",8),
			aorb("aorb",8),
			aandb("aandb",8),
			sumdiff("sumdiff",8),
			result("result",8),
			cout("cout",8),
			layout_item(id,layer_name, position, layout::xy_length_t(126_um,169_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			alu_slice0("alu_slice0", 	layer_name, layout::xy_length_t(0_um,0_um), 	&layout_item, systemc_adapter),
			alu_slice1("alu_slice1", 	layer_name, layout::xy_length_t(31.5_um,0_um), 	&layout_item, systemc_adapter),
			alu_slice2("alu_slice2", 	layer_name, layout::xy_length_t(63_um,0_um), 	&layout_item, systemc_adapter),
			alu_slice3("alu_slice3", 	layer_name, layout::xy_length_t(94.5_um,0_um), 	&layout_item, systemc_adapter),
			alu_slice4("alu_slice4", 	layer_name, layout::xy_length_t(0_um,52_um), 	&layout_item, systemc_adapter),
			alu_slice5("alu_slice5", 	layer_name, layout::xy_length_t(31.5_um,52_um), 	&layout_item, systemc_adapter),
			alu_slice6("alu_slice6", 	layer_name, layout::xy_length_t(63_um,52_um), 	&layout_item, systemc_adapter),
			alu_slice7("alu_slice7", 	layer_name, layout::xy_length_t(94.5_um,52_um), 	&layout_item, systemc_adapter),
			nand3_1("nand3_1",			layer_name, layout::xy_length_t(108.5_um,104_um), 	&layout_item, systemc_adapter),
			inv_1("inv_1",				layer_name, layout::xy_length_t(115.5_um,104_um), 	&layout_item, systemc_adapter),
			inv_2("inv_2",				layer_name, layout::xy_length_t(119_um,104_um), 	&layout_item, systemc_adapter),
			inv_3("inv_3",				layer_name, layout::xy_length_t(122.5_um,104_um), 	&layout_item, systemc_adapter),
			inv_4("inv_4",				layer_name, layout::xy_length_t(70_um,156_um), 	&layout_item, systemc_adapter),
			nor4_1("nor4_1",			layer_name, layout::xy_length_t(52.5_um,156_um), &layout_item, systemc_adapter),
			nor4_2("nor4_2",			layer_name, layout::xy_length_t(59.5_um,156_um), &layout_item, systemc_adapter),
			nand2_1("nand2_1",			layer_name, layout::xy_length_t(66.5_um,156_um), &layout_item, systemc_adapter),
			mux2_1("mux2_1",			layer_name, layout::xy_length_t(52.5_um,104_um), &layout_item, systemc_adapter),
			mux2_2("mux2_2",			layer_name, layout::xy_length_t(80.5_um,104_um), &layout_item, systemc_adapter),
			mux4_1("mux4_1",			layer_name, layout::xy_length_t(0_um,104_um), 	&layout_item, systemc_adapter)
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(nand3_1.id);
				adapter->manager->add_dissipator_component(inv_1.id);
				adapter->manager->add_dissipator_component(inv_2.id);
				adapter->manager->add_dissipator_component(inv_3.id);
				adapter->manager->add_dissipator_component(inv_4.id);
				adapter->manager->add_dissipator_component(nor4_1.id);
				adapter->manager->add_dissipator_component(nor4_2.id);
				adapter->manager->add_dissipator_component(nand2_1.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{	
				add_activity_trace(parameter->trace_path, &inputa, parameter->id);
				add_activity_trace(parameter->trace_path, &inputb, parameter->id);
				add_activity_trace(parameter->trace_path, &alu_function, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
				add_activity_trace(parameter->trace_path, &z_out, parameter->id);
				add_activity_trace(parameter->trace_path, &n_out, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
						
			SC_HAS_PROCESS(alu_8);
			
			inv_2.A(func[1]);
			inv_2.Q(inv_2_out);
			inv_3.A(func[2]);
			inv_3.Q(inv_3_out);
			
			nand3_1.A(func[0]);
			nand3_1.B(inv_2_out);
			nand3_1.C(inv_3_out);
			nand3_1.Q(nand3_1_out);
			
			inv_1.A(nand3_1_out);
			inv_1.Q(inv_1_out);
	
			alu_slice0.cin(inv_1_out);
			alu_slice0.ina(ina[0]);
			alu_slice0.inb(inb[0]);
			alu_slice0.sub(inv_1_out);
			alu_slice0.axorb(axorb[0]);
			alu_slice0.aorb(aorb[0]);
			alu_slice0.aandb(aandb[0]);
			alu_slice0.sumdiff(sumdiff[0]);
			alu_slice0.cout(cout[0]);
		
			alu_slice1.cin(cout[0]);
			alu_slice1.ina(ina[1]);
			alu_slice1.inb(inb[1]);
			alu_slice1.sub(inv_1_out);
			alu_slice1.axorb(axorb[1]);
			alu_slice1.aorb(aorb[1]);
			alu_slice1.aandb(aandb[1]);
			alu_slice1.sumdiff(sumdiff[1]);
			alu_slice1.cout(cout[1]);
				
			alu_slice2.cin(cout[1]);
			alu_slice2.ina(ina[2]);
			alu_slice2.inb(inb[2]);
			alu_slice2.sub(inv_1_out);
			alu_slice2.axorb(axorb[2]);
			alu_slice2.aorb(aorb[2]);
			alu_slice2.aandb(aandb[2]);
			alu_slice2.sumdiff(sumdiff[2]);
			alu_slice2.cout(cout[2]);
			
			alu_slice3.cin(cout[2]);
			alu_slice3.ina(ina[3]);
			alu_slice3.inb(inb[3]);
			alu_slice3.sub(inv_1_out);
			alu_slice3.axorb(axorb[3]);
			alu_slice3.aorb(aorb[3]);
			alu_slice3.aandb(aandb[3]);
			alu_slice3.sumdiff(sumdiff[3]);
			alu_slice3.cout(cout[3]);
			
			alu_slice4.cin(cout[3]);
			alu_slice4.ina(ina[4]);
			alu_slice4.inb(inb[4]);
			alu_slice4.sub(inv_1_out);
			alu_slice4.axorb(axorb[4]);
			alu_slice4.aorb(aorb[4]);
			alu_slice4.aandb(aandb[4]);
			alu_slice4.sumdiff(sumdiff[4]);
			alu_slice4.cout(cout[4]);
			
			alu_slice5.cin(cout[4]);
			alu_slice5.ina(ina[5]);
			alu_slice5.inb(inb[5]);
			alu_slice5.sub(inv_1_out);
			alu_slice5.axorb(axorb[5]);
			alu_slice5.aorb(aorb[5]);
			alu_slice5.aandb(aandb[5]);
			alu_slice5.sumdiff(sumdiff[5]);
			alu_slice5.cout(cout[5]);
			
			alu_slice6.cin(cout[5]);
			alu_slice6.ina(ina[6]);
			alu_slice6.inb(inb[6]);
			alu_slice6.sub(inv_1_out);
			alu_slice6.axorb(axorb[6]);
			alu_slice6.aorb(aorb[6]);
			alu_slice6.aandb(aandb[6]);
			alu_slice6.sumdiff(sumdiff[6]);
			alu_slice6.cout(cout[6]);
			
			alu_slice7.cin(cout[6]);
			alu_slice7.ina(ina[7]);
			alu_slice7.inb(inb[7]);
			alu_slice7.sub(inv_1_out);
			alu_slice7.axorb(axorb[7]);
			alu_slice7.aorb(aorb[7]);
			alu_slice7.aandb(aandb[7]);
			alu_slice7.sumdiff(sumdiff[7]);
			alu_slice7.cout(cout[7]);
			
			mux2_1.input0(aandb_bus);
			mux2_1.input1(aorb_bus);
			mux2_1.select(func0);
			mux2_1.output(mux2_1_output);
			
			mux2_2.input0(axorb_bus);
			mux2_2.input1(inputb);
			mux2_2.select(func0);
			mux2_2.output(mux2_2_output);
			
			mux4_1.input0(sumdiff_bus);
			mux4_1.input1(mux2_1_output);
			mux4_1.input2(mux2_2_output);
			mux4_1.input3(inputb);
			mux4_1.select(func21); //ketto lett, maradhat?
			mux4_1.output(result_bus); //lehet, hogy ez igy nem fog menni, akkor kulon madzagot kell bevezetnem
			
			nor4_1.A(result[0]);
			nor4_1.B(result[1]);
			nor4_1.C(result[2]);
			nor4_1.D(result[3]);
			nor4_1.Q(nor4_1_out);
			
			nor4_2.A(result[4]);
			nor4_2.B(result[5]);
			nor4_2.C(result[6]);
			nor4_2.D(result[7]);
			nor4_2.Q(nor4_2_out);
			
			nand2_1.A(nor4_1_out);
			nand2_1.B(nor4_2_out);
			nand2_1.Q(nand2_1_out);
			
			inv_4.A(nand2_1_out);
			inv_4.Q(z_out);
			
			SC_METHOD(write_ina);
			sensitive << inputa;
			dont_initialize();
			
			SC_METHOD(write_inb);
			sensitive << inputb;
			dont_initialize();
			
			SC_METHOD(write_func);
			sensitive << alu_function;
			dont_initialize();
			
			SC_METHOD(write_axorb_bus);
			for (size_t it = 0; it < axorb.size(); ++it) sensitive << axorb[it];
			dont_initialize();
			
			SC_METHOD(write_aorb_bus);
			for (size_t it = 0; it < aorb.size(); ++it) sensitive << aorb[it];
			dont_initialize();
			
			SC_METHOD(write_aandb_bus);
			for (size_t it = 0; it < aandb.size(); ++it) sensitive << aandb[it];
			dont_initialize();
			
			SC_METHOD(write_sumdiff_bus);
			for (size_t it = 0; it < sumdiff.size(); ++it) sensitive << sumdiff[it];
			dont_initialize();
			
			SC_METHOD(write_result);
			sensitive << result_bus;
			dont_initialize();		
		}
	};

}	//namespace demoproc_ams035

#endif //_ALU_HPP_
