#ifndef _REGISTER_1_HPP_
#define _REGISTER_1_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035
{
	
	SC_MODULE(register_1)
	{
		sc_in<bool> clock;
		sc_in<sc_logic> input, reset, enable;
		sc_out<sc_logic> output;
		
		sc_signal<sc_logic> UNCONNECTED, n_0, n_1;
		
		layout::sunred::component_t layout_item;
		
		ams035::df3 dout_reg0;
		ams035::nor20 g8;
		ams035::imux20 g9;
		
		register_1(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			layout_item(id, layer_name, position, layout::xy_length_t(31.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			dout_reg0("dout_reg0", layer_name, layout::xy_length_t(0_um,0_um), &layout_item, systemc_adapter),
			g8("g8", layer_name, layout::xy_length_t(21_um,0_um), &layout_item, systemc_adapter),
			g9("g9", layer_name, layout::xy_length_t(24.5_um,0_um), &layout_item, systemc_adapter)
			
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(dout_reg0.id);
				adapter->manager->add_dissipator_component(g8.id);
				adapter->manager->add_dissipator_component(g9.id);
				adapter->manager->add_display_component(id);
			}
						
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			dout_reg0.C(clock);
			dout_reg0.D(n_1);
			dout_reg0.Q(output);
			dout_reg0.QN(UNCONNECTED);
			
			g8.A(reset);
			g8.B(n_0);
			g8.Q(n_1);
			
			g9.A(output);
			g9.B(input);
			g9.S(enable);
			g9.Q(n_0);
		}
	};
	
}	//namespace demoproc_ams035
#endif //_REGISTER_1_HPP_
