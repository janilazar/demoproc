#ifndef _CONTROLLER_HPP_
#define _CONTROLLER_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

// extern const simulation_parameters parameters;

namespace demoproc_ams035
{
	SC_MODULE(controller)
	{
		sc_in<bool> clock;
		sc_in<sc_logic> reset;
		sc_in<sc_logic> run;
		sc_in<sc_logic> ss_Z;
		sc_in<sc_logic> ss_N;
		sc_in<sc_lv<4>> ss_instruction_code;
		sc_out<sc_logic> end_of_job;
		sc_out<sc_logic> write_data_cache;
		sc_out<sc_logic> ce_IR;
		sc_out<sc_logic> ce_IP;
		sc_out<sc_logic> ce_instruction_code;
		sc_out<sc_logic> ce_destination;
		sc_out<sc_logic> ce_OP_A;
		sc_out<sc_logic> ce_OP_B;
		sc_out<sc_logic> ce_IMM;
		sc_out<sc_logic> ce_MAR;
		sc_out<sc_logic> ce_MDR;
		sc_out<sc_logic> ce_Z;
		sc_out<sc_logic> ce_N;
		sc_out<sc_logic> we_RF;
		sc_out<sc_logic> sel_IP;
		sc_out<sc_logic> sel_IP_add;
		sc_out<sc_lv<2>> sel_ALU;

		sc_vector<sc_signal<sc_logic>> sel_ALU_bus; //constructor plz
		sc_vector<sc_signal<sc_logic>> ss_instruction_code_bus; //constructor plz
		sc_vector<sc_signal<sc_logic>> state; //constructor plz
		
		sc_signal<sc_logic> UNCONNECTED, UNCONNECTED0, UNCONNECTED1, UNCONNECTED2, UNCONNECTED3, UNCONNECTED4, UNCONNECTED5, n_3;
		
		sc_signal<sc_logic> n_4, n_5, n_6, n_7, n_8, n_9, n_10, n_11;
		sc_signal<sc_logic> n_12, n_13, n_14, n_15, n_16, n_17, n_18, n_19;
		sc_signal<sc_logic> n_20, n_21, n_22, n_23, n_24, n_25, n_26, n_27;
		sc_signal<sc_logic> n_28, n_29, n_30, n_31, n_32, n_33, n_34, n_35;
		sc_signal<sc_logic> n_36, n_37, n_38, n_39, n_40, n_41, n_42, n_43;
		sc_signal<sc_logic> n_44, n_45, n_46, n_47, n_48, n_49, n_50, n_51;
		sc_signal<sc_logic> n_52, n_53, n_54, n_55, n_56, n_57, n_58, n_59;
		sc_signal<sc_logic> n_60, n_61, n_62, n_63, n_64, n_65, n_66, n_67;
		sc_signal<sc_logic> n_68, n_69, n_70, n_71, n_72, n_73, n_74, n_75;
		sc_signal<sc_logic> n_76, n_77, n_78, n_79, n_80, n_81, n_82, n_83;
		sc_signal<sc_logic> n_84, n_85, n_86, n_87, n_88, n_89, n_90, n_91;
		sc_signal<sc_logic> n_92, n_93, n_94, n_95, n_96, n_97, n_98, n_99;
		sc_signal<sc_logic> n_100, n_101, n_102, n_122, n_123, n_124;
		
		layout::sunred::component_t layout_item;
				
		ams035::df3 	state_reg0;
		ams035::df3 	sel_IP_add_reg;
		ams035::nand40 	g2528;
		ams035::nand20 	g2529;
		ams035::df3 	ce_Z_reg;
		ams035::df3 	sel_ALU_reg0;
		ams035::df3 	state_reg1;
		ams035::df3 	state_reg2;
		ams035::df3 	end_of_job_reg;
		ams035::df3 	ce_IP_reg;
		ams035::df3 	ce_IR_reg;
		ams035::df3 	write_data_cache_reg;
		ams035::aoi210 	g2542;
		ams035::oai2110 g2554;
		ams035::oai210 	g2532;
		ams035::df3 	sel_IP_reg;
		ams035::oai210 	g2552;
		ams035::df3 	sel_ALU_reg1;
		ams035::oai210 	g2543;
		ams035::oai210 	g2544;
		ams035::nand20 	g2546;
		ams035::oai210 	g2565;
		ams035::nand20 	g2550;
		ams035::nand20 	g2569;
		ams035::aoi210 	g2551;
		ams035::df3 	ce_instruction_code_reg;
		ams035::inv0 	g2555;
		ams035::aoi220 	g2556;
		ams035::oai2110 g2557;
		ams035::oai210 	g2549;
		ams035::inv0 	g2567;
		ams035::df3 	state_reg3;
		ams035::oai310 	g2553;
		ams035::oai310 	g2561;
		ams035::aoi220 	g2547;
		ams035::nand20 	g2548;
		ams035::aoi210 	g2564;
		ams035::oai220 	g2568;
		ams035::aoi220 	g2576;
		ams035::inv0 	g2577;
		ams035::oai220 	g2558;
		ams035::oai210 	g2559;
		ams035::oai210 	g2560;
		ams035::inv0 	g2562;
		ams035::aoi220 	g2575;
		ams035::nor20 	g2578;
		ams035::oai210 	g2583;
		ams035::oai310 	g2563;
		ams035::oai310 	g2566;
		ams035::nand30 	g2571;
		ams035::oai210 	g2573;
		ams035::nand30 	g2584;
		ams035::inv0 	g2589;
		ams035::inv0 	g2591;
		ams035::oai310 	g2570;
		ams035::nor40 	g2572;
		ams035::oai210 	g2580;
		ams035::inv0 	g2581;
		ams035::oai210 	g2585;
		ams035::aoi210 	g2586;
		ams035::nand20 	g2590;
		ams035::nor20 	g2592;
		ams035::nor30 	g2595;
		ams035::oai210 	g2598;
		ams035::oai310 	g2574;
		ams035::nand20 	g2579;
		ams035::nor30 	g2582;
		ams035::nand20 	g2587;
		ams035::nand20 	g2594;
		ams035::nand30 	g2596;
		ams035::inv0 	g2604;
		ams035::inv0 	g2608;
		ams035::nand20 	g2588;
		ams035::nand20 	g2609;
		ams035::nor20 	g2593;
		ams035::oai210 	g2597;
		ams035::oai220 	g2599;
		ams035::inv0 	g2600;
		ams035::inv0 	g2610;
		ams035::nand20 	g2612;
		ams035::nand20 	g2605;
		ams035::nor20 	g2601;
		ams035::nand20 	g2603;
		ams035::inv0 	g2606;
		ams035::inv0 	g2613;
		ams035::nand20 	g2611;
		ams035::nand20 	g2607;
		ams035::aoi220 	g2614;
		ams035::imux20 	g2615;
		ams035::inv0 	g2617;
		ams035::inv0 	g2624;
		ams035::inv0 	g2626;
		ams035::inv0 	g2628;
		ams035::inv0 	g2630;
		ams035::nand20 	g2602;
		ams035::nand20 	g2625;
		ams035::nand20 	g2616;
		ams035::nor20 	g2619;
		ams035::inv0 	g2620;
		ams035::nand20	g2627;
		ams035::nor20 	g2631;
		ams035::nor20 	g2618;
		ams035::nor20 	g2629;
		ams035::nor20 	g2623;
		ams035::nor20 	g2621;
		ams035::nor20 	g2622;
		ams035::inv0 	g2641;
		ams035::inv0 	g2635;
		ams035::inv0 	g2639;
		ams035::inv0 	g2632;
		
		
		void write_ss_instruction_code_bus()
		{
			for(size_t it = 0; it < 4; ++it) ss_instruction_code_bus[it] = ss_instruction_code.read()[it];
		}
		
		void write_sel_ALU()
		{
			sc_lv<2> tmp;
			for(size_t it = 0; it < 2; ++it) tmp[it] = sel_ALU_bus[it];
			sel_ALU.write(tmp);
		}
		
		void write_we_RF()
		{
			we_RF.write(ce_Z);
		}
		
		void write_ce_N()
		{
			ce_N.write(ce_Z);
		}
		
		void write_ce_MDR()
		{
			ce_MDR.write(ce_instruction_code);
		}
		
		void write_ce_MAR()
		{
			ce_MAR.write(ce_instruction_code);
		}
		
		void write_ce_IMM()
		{
			ce_IMM.write(ce_instruction_code);
		}
		
		void write_ce_OP_B()
		{
			ce_OP_B.write(ce_instruction_code);
		}
		
		void write_ce_OP_A()
		{
			ce_OP_A.write(ce_instruction_code);
		}
		
		void write_ce_destination()
		{
			ce_destination.write(ce_instruction_code);
		}
		
		controller(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			clock("clock"),
			reset("reset"),
			run("run"),
			ss_Z("ss_Z"),
			ss_N("ss_N"),
			ss_instruction_code("ss_instruction_code"),
			end_of_job("end_of_job"),
			write_data_cache("write_data_cache"),
			ce_IR("ce_IR"),
			ce_IP("ce_IP"),
			ce_instruction_code("ce_instruction_code"),
			ce_destination("ce_destination"),
			ce_OP_A("ce_OP_A"),
			ce_OP_B("ce_OP_B"), 
			ce_IMM("ce_IMM"),
			ce_MAR("ce_MAR"),
			ce_MDR("ce_MDR"),
			ce_Z("ce_Z"),
			ce_N("ce_N"),
			we_RF("we_RF"),
			sel_IP("sel_IP"),
			sel_IP_add("sel_IP_add"),
			sel_ALU("sel_ALU"),
			layout_item(id, layer_name, position, layout::xy_length_t(140_um,143_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			sel_ALU_bus("sel_ALU_bus",2),
			ss_instruction_code_bus("ss_instruction_code_bus",4),
			state("state",4),
			state_reg0("state_reg0", 								layer_name, layout::xy_length_t(0_um,0_um), 	&layout_item, systemc_adapter),
			sel_IP_add_reg("sel_IP_add_reg",						layer_name, layout::xy_length_t(21_um,0_um), 	&layout_item, systemc_adapter),
			g2528("g2528",											layer_name, layout::xy_length_t(42_um,0_um), &layout_item, systemc_adapter),
			g2529("g2529",											layer_name, layout::xy_length_t(49_um,0_um), &layout_item, systemc_adapter),
			ce_Z_reg("ce_Z_reg",									layer_name, layout::xy_length_t(52.5_um,0_um), &layout_item, systemc_adapter),
			sel_ALU_reg0("sel_ALU_reg0",							layer_name, layout::xy_length_t(73.5_um,0_um), &layout_item, systemc_adapter),
			state_reg1("state_reg1",								layer_name, layout::xy_length_t(94.5_um,0_um), &layout_item, systemc_adapter),
			state_reg2("state_reg2",								layer_name, layout::xy_length_t(115.5_um,0_um), &layout_item, systemc_adapter),
			end_of_job_reg("end_of_job_reg",						layer_name, layout::xy_length_t(0_um,26_um), 	&layout_item, systemc_adapter),
			ce_IP_reg("ce_IP_reg",									layer_name, layout::xy_length_t(21_um,26_um), 	&layout_item, systemc_adapter),
			ce_IR_reg("ce_IR_reg",									layer_name, layout::xy_length_t(42_um,26_um), &layout_item, systemc_adapter),
			write_data_cache_reg("write_data_cache_reg",			layer_name, layout::xy_length_t(63_um,26_um), &layout_item, systemc_adapter),
			g2542("g2542",											layer_name, layout::xy_length_t(84_um,26_um), &layout_item, systemc_adapter),
			g2554("g2554",											layer_name, layout::xy_length_t(91_um,26_um), &layout_item, systemc_adapter),
			g2532("g2532",											layer_name, layout::xy_length_t(98_um,26_um), &layout_item, systemc_adapter),
			sel_IP_reg("sel_IP_reg",								layer_name, layout::xy_length_t(105_um,26_um), &layout_item, systemc_adapter),
			g2552("g2552",											layer_name, layout::xy_length_t(0_um,52_um), 	&layout_item, systemc_adapter),
			sel_ALU_reg1("sel_ALU_reg1",							layer_name, layout::xy_length_t(7_um,52_um), 	&layout_item, systemc_adapter),
			g2543("g2543",											layer_name, layout::xy_length_t(28_um,52_um), 	&layout_item, systemc_adapter),
			g2544("g2544",											layer_name, layout::xy_length_t(35_um,52_um), &layout_item, systemc_adapter),
			g2546("g2546",											layer_name, layout::xy_length_t(42_um,52_um), &layout_item, systemc_adapter),
			g2565("g2565",											layer_name, layout::xy_length_t(45.5_um,52_um), &layout_item, systemc_adapter),
			g2550("g2550",											layer_name, layout::xy_length_t(52.5_um,52_um), &layout_item, systemc_adapter),
			g2569("g2569",											layer_name, layout::xy_length_t(56_um,52_um), &layout_item, systemc_adapter),
			g2551("g2551",											layer_name, layout::xy_length_t(59.5_um,52_um), &layout_item, systemc_adapter),
			ce_instruction_code_reg("ce_instruction_code_reg",		layer_name, layout::xy_length_t(66.5_um,52_um), &layout_item, systemc_adapter),
			g2555("g2555",											layer_name, layout::xy_length_t(87.5_um,52_um), &layout_item, systemc_adapter),
			g2556("g2556",											layer_name, layout::xy_length_t(91_um,52_um), &layout_item, systemc_adapter),
			g2557("g2557",											layer_name, layout::xy_length_t(98_um,52_um), &layout_item, systemc_adapter),
			g2549("g2549",											layer_name, layout::xy_length_t(105_um,52_um), &layout_item, systemc_adapter),
			g2567("g2567",											layer_name, layout::xy_length_t(112_um,52_um), &layout_item, systemc_adapter),
			state_reg3("state_reg3",								layer_name, layout::xy_length_t(0_um,78_um), 	&layout_item, systemc_adapter),
			g2553("g2553",											layer_name, layout::xy_length_t(21_um,78_um), 	&layout_item, systemc_adapter),
			g2561("g2561",											layer_name, layout::xy_length_t(28_um,78_um), 	&layout_item, systemc_adapter),
			g2547("g2547",											layer_name, layout::xy_length_t(35_um,78_um), &layout_item, systemc_adapter),
			g2548("g2548",											layer_name, layout::xy_length_t(42_um,78_um), &layout_item, systemc_adapter),
			g2564("g2564",											layer_name, layout::xy_length_t(45.5_um,78_um), &layout_item, systemc_adapter),
			g2568("g2568",											layer_name, layout::xy_length_t(52.5_um,78_um), &layout_item, systemc_adapter),
			g2576("g2576",											layer_name, layout::xy_length_t(59.5_um,78_um), &layout_item, systemc_adapter),
			g2577("g2577",											layer_name, layout::xy_length_t(66.5_um,78_um), &layout_item, systemc_adapter),
			g2558("g2558",											layer_name, layout::xy_length_t(70_um,78_um), &layout_item, systemc_adapter),
			g2559("g2559",											layer_name, layout::xy_length_t(77_um,78_um), &layout_item, systemc_adapter),
			g2560("g2560",											layer_name, layout::xy_length_t(84_um,78_um), &layout_item, systemc_adapter),
			g2562("g2562",											layer_name, layout::xy_length_t(91_um,78_um), &layout_item, systemc_adapter),
			g2575("g2575",											layer_name, layout::xy_length_t(94.5_um,78_um), &layout_item, systemc_adapter),
			g2578("g2578",											layer_name, layout::xy_length_t(101.5_um,78_um), &layout_item, systemc_adapter),
			g2583("g2583",											layer_name, layout::xy_length_t(105_um,78_um), &layout_item, systemc_adapter),
			g2563("g2563",											layer_name, layout::xy_length_t(112_um,78_um), &layout_item, systemc_adapter),
			g2566("g2566",											layer_name, layout::xy_length_t(119_um,78_um), &layout_item, systemc_adapter),
			g2571("g2571",											layer_name, layout::xy_length_t(126_um,78_um), &layout_item, systemc_adapter),
			g2573("g2573",											layer_name, layout::xy_length_t(133_um,78_um), &layout_item, systemc_adapter),
			g2584("g2584",											layer_name, layout::xy_length_t(0_um,104_um), 	&layout_item, systemc_adapter),
			g2589("g2589",											layer_name, layout::xy_length_t(7_um,104_um), 	&layout_item, systemc_adapter),
			g2591("g2591",											layer_name, layout::xy_length_t(10.5_um,104_um), 	&layout_item, systemc_adapter),
			g2570("g2570",											layer_name, layout::xy_length_t(14_um,104_um), 	&layout_item, systemc_adapter),
			g2572("g2572",											layer_name, layout::xy_length_t(21_um,104_um), 	&layout_item, systemc_adapter),
			g2580("g2580",											layer_name, layout::xy_length_t(28_um,104_um), 	&layout_item, systemc_adapter),
			g2581("g2581",											layer_name, layout::xy_length_t(35_um,104_um), &layout_item, systemc_adapter),
			g2585("g2585",											layer_name, layout::xy_length_t(38.5_um,104_um), &layout_item, systemc_adapter),
			g2586("g2586",											layer_name, layout::xy_length_t(45.5_um,104_um), &layout_item, systemc_adapter),
			g2590("g2590",											layer_name, layout::xy_length_t(52.5_um,104_um), &layout_item, systemc_adapter),
			g2592("g2592",											layer_name, layout::xy_length_t(56_um,104_um), &layout_item, systemc_adapter),
			g2595("g2595",											layer_name, layout::xy_length_t(59.5_um,104_um), &layout_item, systemc_adapter),
			g2598("g2598",											layer_name, layout::xy_length_t(66.5_um,104_um), &layout_item, systemc_adapter),
			g2574("g2574",											layer_name, layout::xy_length_t(73.5_um,104_um), &layout_item, systemc_adapter),
			g2579("g2579",											layer_name, layout::xy_length_t(80.5_um,104_um), &layout_item, systemc_adapter),
			g2582("g2582",											layer_name, layout::xy_length_t(84_um,104_um), &layout_item, systemc_adapter),
			g2587("g2587",											layer_name, layout::xy_length_t(91_um,104_um), &layout_item, systemc_adapter),
			g2594("g2594",											layer_name, layout::xy_length_t(94.5_um,104_um), &layout_item, systemc_adapter),
			g2596("g2596",											layer_name, layout::xy_length_t(98_um,104_um), &layout_item, systemc_adapter),
			g2604("g2604",											layer_name, layout::xy_length_t(105_um,104_um), &layout_item, systemc_adapter),
			g2608("g2608",											layer_name, layout::xy_length_t(108.5_um,104_um), &layout_item, systemc_adapter),
			g2588("g2588",											layer_name, layout::xy_length_t(112_um,104_um), &layout_item, systemc_adapter),
			g2609("g2609",											layer_name, layout::xy_length_t(115.5_um,104_um), &layout_item, systemc_adapter),
			g2593("g2593",											layer_name, layout::xy_length_t(119_um,104_um), &layout_item, systemc_adapter),
			g2597("g2597",											layer_name, layout::xy_length_t(122.5_um,104_um), &layout_item, systemc_adapter),
			g2599("g2599",											layer_name, layout::xy_length_t(129.5_um,104_um), &layout_item, systemc_adapter),
			g2600("g2600",											layer_name, layout::xy_length_t(136.5_um,104_um), &layout_item, systemc_adapter),
			g2610("g2610",											layer_name, layout::xy_length_t(0_um,130_um), &layout_item, systemc_adapter),
			g2612("g2612",											layer_name, layout::xy_length_t(3.5_um,130_um), &layout_item, systemc_adapter),
			g2605("g2605",											layer_name, layout::xy_length_t(7_um,130_um), &layout_item, systemc_adapter),
			g2601("g2601",											layer_name, layout::xy_length_t(10.5_um,130_um), &layout_item, systemc_adapter),
			g2603("g2603",											layer_name, layout::xy_length_t(14_um,130_um), &layout_item, systemc_adapter),
			g2606("g2606",											layer_name, layout::xy_length_t(17.5_um,130_um), &layout_item, systemc_adapter),
			g2613("g2613",											layer_name, layout::xy_length_t(21_um,130_um), &layout_item, systemc_adapter),
			g2611("g2611",											layer_name, layout::xy_length_t(24.5_um,130_um), &layout_item, systemc_adapter),
			g2607("g2607",											layer_name, layout::xy_length_t(28_um,130_um), &layout_item, systemc_adapter),
			g2614("g2614",											layer_name, layout::xy_length_t(31.5_um,130_um), &layout_item, systemc_adapter),
			g2615("g2615",											layer_name, layout::xy_length_t(38.5_um,130_um),&layout_item, systemc_adapter),
			g2617("g2617",											layer_name, layout::xy_length_t(45.5_um,130_um),&layout_item, systemc_adapter),
			g2624("g2624",											layer_name, layout::xy_length_t(49_um,130_um),&layout_item, systemc_adapter),
			g2626("g2626",											layer_name, layout::xy_length_t(52.5_um,130_um),&layout_item, systemc_adapter),
			g2628("g2628",											layer_name, layout::xy_length_t(56_um,130_um),&layout_item, systemc_adapter),
			g2630("g2630",											layer_name, layout::xy_length_t(59.5_um,130_um),&layout_item, systemc_adapter),
			g2602("g2602",											layer_name, layout::xy_length_t(63_um,130_um),&layout_item, systemc_adapter),
			g2625("g2625",											layer_name, layout::xy_length_t(66.5_um,130_um),&layout_item, systemc_adapter),
			g2616("g2616",											layer_name, layout::xy_length_t(70_um,130_um),&layout_item, systemc_adapter),
			g2619("g2619",											layer_name, layout::xy_length_t(73.5_um,130_um),&layout_item, systemc_adapter),
			g2620("g2620",											layer_name, layout::xy_length_t(77_um,130_um),&layout_item, systemc_adapter),
			g2627("g2627",											layer_name, layout::xy_length_t(80.5_um,130_um),&layout_item, systemc_adapter),
			g2631("g2631",											layer_name, layout::xy_length_t(84_um,130_um),&layout_item, systemc_adapter),
			g2618("g2618",											layer_name, layout::xy_length_t(87.5_um,130_um),&layout_item, systemc_adapter),
			g2629("g2629",											layer_name, layout::xy_length_t(91_um,130_um),&layout_item, systemc_adapter),
			g2623("g2623",											layer_name, layout::xy_length_t(94.5_um,130_um),&layout_item, systemc_adapter),
			g2621("g2621",											layer_name, layout::xy_length_t(98_um,130_um),&layout_item, systemc_adapter),
			g2622("g2622",											layer_name, layout::xy_length_t(101.5_um,130_um),&layout_item, systemc_adapter),
			g2641("g2641",											layer_name, layout::xy_length_t(105_um,130_um),&layout_item, systemc_adapter),
			g2635("g2635",											layer_name, layout::xy_length_t(108.5_um,130_um),&layout_item, systemc_adapter),
			g2639("g2639",											layer_name, layout::xy_length_t(112_um,130_um),&layout_item, systemc_adapter),
			g2632("g2632",											layer_name, layout::xy_length_t(115.5_um,130_um),&layout_item, systemc_adapter)
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(state_reg0.id);
				adapter->manager->add_dissipator_component(sel_IP_add_reg.id);
				adapter->manager->add_dissipator_component(g2528.id);
				adapter->manager->add_dissipator_component(g2529.id);
				adapter->manager->add_dissipator_component(ce_Z_reg.id);
				adapter->manager->add_dissipator_component(sel_ALU_reg0.id);
				adapter->manager->add_dissipator_component(state_reg1.id);
				adapter->manager->add_dissipator_component(state_reg2.id);
				adapter->manager->add_dissipator_component(end_of_job_reg.id);
				adapter->manager->add_dissipator_component(ce_IP_reg.id);
				adapter->manager->add_dissipator_component(ce_IR_reg.id);
				adapter->manager->add_dissipator_component(write_data_cache_reg.id);
				adapter->manager->add_dissipator_component(g2542.id);
				adapter->manager->add_dissipator_component(g2554.id);
				adapter->manager->add_dissipator_component(g2532.id);
				adapter->manager->add_dissipator_component(sel_IP_reg.id);
				adapter->manager->add_dissipator_component(g2552.id);
				adapter->manager->add_dissipator_component(sel_ALU_reg1.id);
				adapter->manager->add_dissipator_component(g2543.id);
				adapter->manager->add_dissipator_component(g2544.id);
				adapter->manager->add_dissipator_component(g2546.id);
				adapter->manager->add_dissipator_component(g2565.id);
				adapter->manager->add_dissipator_component(g2550.id);
				adapter->manager->add_dissipator_component(g2569.id);
				adapter->manager->add_dissipator_component(g2551.id);
				adapter->manager->add_dissipator_component(ce_instruction_code_reg.id);
				adapter->manager->add_dissipator_component(g2555.id);
				adapter->manager->add_dissipator_component(g2556.id);
				adapter->manager->add_dissipator_component(g2557.id);
				adapter->manager->add_dissipator_component(g2549.id);
				adapter->manager->add_dissipator_component(g2567.id);
				adapter->manager->add_dissipator_component(state_reg3.id);
				adapter->manager->add_dissipator_component(g2553.id);
				adapter->manager->add_dissipator_component(g2561.id);
				adapter->manager->add_dissipator_component(g2547.id);
				adapter->manager->add_dissipator_component(g2548.id);
				adapter->manager->add_dissipator_component(g2564.id);
				adapter->manager->add_dissipator_component(g2568.id);
				adapter->manager->add_dissipator_component(g2576.id);
				adapter->manager->add_dissipator_component(g2577.id);
				adapter->manager->add_dissipator_component(g2558.id);
				adapter->manager->add_dissipator_component(g2559.id);
				adapter->manager->add_dissipator_component(g2560.id);
				adapter->manager->add_dissipator_component(g2562.id);
				adapter->manager->add_dissipator_component(g2575.id);
				adapter->manager->add_dissipator_component(g2578.id);
				adapter->manager->add_dissipator_component(g2583.id);
				adapter->manager->add_dissipator_component(g2563.id);
				adapter->manager->add_dissipator_component(g2566.id);
				adapter->manager->add_dissipator_component(g2571.id);
				adapter->manager->add_dissipator_component(g2573.id);
				adapter->manager->add_dissipator_component(g2584.id);
				adapter->manager->add_dissipator_component(g2589.id);
				adapter->manager->add_dissipator_component(g2591.id);
				adapter->manager->add_dissipator_component(g2570.id);
				adapter->manager->add_dissipator_component(g2572.id);
				adapter->manager->add_dissipator_component(g2580.id);
				adapter->manager->add_dissipator_component(g2581.id);
				adapter->manager->add_dissipator_component(g2585.id);
				adapter->manager->add_dissipator_component(g2586.id);
				adapter->manager->add_dissipator_component(g2590.id);
				adapter->manager->add_dissipator_component(g2592.id);
				adapter->manager->add_dissipator_component(g2595.id);
				adapter->manager->add_dissipator_component(g2598.id);
				adapter->manager->add_dissipator_component(g2574.id);
				adapter->manager->add_dissipator_component(g2579.id);
				adapter->manager->add_dissipator_component(g2582.id);
				adapter->manager->add_dissipator_component(g2587.id);
				adapter->manager->add_dissipator_component(g2594.id);
				adapter->manager->add_dissipator_component(g2596.id);
				adapter->manager->add_dissipator_component(g2604.id);
				adapter->manager->add_dissipator_component(g2608.id);
				adapter->manager->add_dissipator_component(g2588.id);
				adapter->manager->add_dissipator_component(g2609.id);
				adapter->manager->add_dissipator_component(g2593.id);
				adapter->manager->add_dissipator_component(g2597.id);
				adapter->manager->add_dissipator_component(g2599.id);
				adapter->manager->add_dissipator_component(g2600.id);
				adapter->manager->add_dissipator_component(g2610.id);
				adapter->manager->add_dissipator_component(g2612.id);
				adapter->manager->add_dissipator_component(g2605.id);
				adapter->manager->add_dissipator_component(g2601.id);
				adapter->manager->add_dissipator_component(g2603.id);
				adapter->manager->add_dissipator_component(g2606.id);
				adapter->manager->add_dissipator_component(g2613.id);
				adapter->manager->add_dissipator_component(g2611.id);
				adapter->manager->add_dissipator_component(g2607.id);
				adapter->manager->add_dissipator_component(g2614.id);
				adapter->manager->add_dissipator_component(g2615.id);
				adapter->manager->add_dissipator_component(g2617.id);
				adapter->manager->add_dissipator_component(g2624.id);
				adapter->manager->add_dissipator_component(g2626.id);
				adapter->manager->add_dissipator_component(g2628.id);
				adapter->manager->add_dissipator_component(g2630.id);
				adapter->manager->add_dissipator_component(g2602.id);
				adapter->manager->add_dissipator_component(g2625.id);
				adapter->manager->add_dissipator_component(g2616.id);
				adapter->manager->add_dissipator_component(g2619.id);
				adapter->manager->add_dissipator_component(g2620.id);
				adapter->manager->add_dissipator_component(g2627.id);
				adapter->manager->add_dissipator_component(g2631.id);
				adapter->manager->add_dissipator_component(g2618.id);
				adapter->manager->add_dissipator_component(g2629.id);
				adapter->manager->add_dissipator_component(g2623.id);
				adapter->manager->add_dissipator_component(g2621.id);
				adapter->manager->add_dissipator_component(g2622.id);
				adapter->manager->add_dissipator_component(g2641.id);
				adapter->manager->add_dissipator_component(g2635.id);
				adapter->manager->add_dissipator_component(g2639.id);
				adapter->manager->add_dissipator_component(g2632.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &run, parameter->id);
				add_activity_trace(parameter->trace_path, &ss_Z, parameter->id);
				add_activity_trace(parameter->trace_path, &ss_N, parameter->id);
				add_activity_trace(parameter->trace_path, &ss_instruction_code, parameter->id);
				add_activity_trace(parameter->trace_path, &end_of_job, parameter->id);
				add_activity_trace(parameter->trace_path, &write_data_cache, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_IR, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_IP, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_instruction_code, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_destination, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_OP_A, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_OP_B, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_IMM, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_MAR, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_MDR, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_Z, parameter->id);
				add_activity_trace(parameter->trace_path, &ce_N, parameter->id);
				add_activity_trace(parameter->trace_path, &we_RF, parameter->id);
				add_activity_trace(parameter->trace_path, &sel_IP_add, parameter->id);
				add_activity_trace(parameter->trace_path, &sel_ALU, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			// #ifdef TRACE_COMPONENT_DELAY
			// 	add_stopper_trace(ss_Z.id, &clock.pos(),&ss_Z);
			// 	add_stopper_trace(ss_N.id, &clock.pos(),&ss_N);
			// 	add_stopper_trace(ss_instruction_code.id, &clock.pos(),&ss_instruction_code);
			// 	add_stopper_trace(end_of_job.id, &clock.pos(),&end_of_job);
			// 	add_stopper_trace(write_data_cache.id, &clock.pos(),&write_data_cache);
			// 	add_stopper_trace(ce_IR.id, &clock.pos(),&ce_IR);
			// 	add_stopper_trace(ce_IP.id, &clock.pos(),&ce_IP);
			// 	add_stopper_trace(ce_instruction_code.id, &clock.pos(),&ce_instruction_code);
			// 	add_stopper_trace(ce_destination.id, &clock.pos(),&ce_destination);
			// 	add_stopper_trace(ce_OP_A.id, &clock.pos(),&ce_OP_A);
			// 	add_stopper_trace(ce_OP_B.id, &clock.pos(),&ce_OP_B);
			// 	add_stopper_trace(ce_IMM.id, &clock.pos(),&ce_IMM);
			// 	add_stopper_trace(ce_MAR.id, &clock.pos(),&ce_MAR);
			// 	add_stopper_trace(ce_MDR.id, &clock.pos(),&ce_MDR);
			// 	add_stopper_trace(ce_Z.id, &clock.pos(),&ce_Z);
			// 	add_stopper_trace(ce_N.id, &clock.pos(),&ce_N);
			// 	add_stopper_trace(we_RF.id, &clock.pos(),&we_RF);
			// 	add_stopper_trace(sel_IP.id, &clock.pos(),&sel_IP_add);
			// 	add_stopper_trace(sel_ALU.id, &clock.pos(),&sel_ALU);
			// #endif
			
				
			SC_HAS_PROCESS(controller);

			state_reg0.D(n_102);
			state_reg0.C(clock);
			state_reg0.Q(state[0]);
			state_reg0.QN(n_83);

			sel_IP_add_reg.C(clock);
			sel_IP_add_reg.D(n_101);
			sel_IP_add_reg.Q(sel_IP_add);
			sel_IP_add_reg.QN(UNCONNECTED);

			g2528.A(n_100);
			g2528.B(n_75);
			g2528.C(n_54);
			g2528.D(n_89);
			g2528.Q(n_102);

			g2529.A(n_64);
			g2529.B(n_98);
			g2529.Q(n_101);

			ce_Z_reg.D(n_99);
			ce_Z_reg.C(clock);
			ce_Z_reg.Q(ce_Z);
			ce_Z_reg.QN(UNCONNECTED0);

			sel_ALU_reg0.D(n_95);
			sel_ALU_reg0.C(clock);
			sel_ALU_reg0.Q(sel_ALU_bus[0]);
			sel_ALU_reg0.QN(n_124);

			state_reg1.D(n_93);
			state_reg1.C(clock);
			state_reg1.Q(state[1]);
			state_reg1.QN(n_17);

			state_reg2.D(n_97);
			state_reg2.C(clock);
			state_reg2.Q(state[2]);
			state_reg2.QN(n_12);
			
			end_of_job_reg.D(n_86);
			end_of_job_reg.C(clock);
			end_of_job_reg.Q(end_of_job);
			end_of_job_reg.QN(UNCONNECTED1);
			
			ce_IP_reg.D(n_92);
			ce_IP_reg.C(clock);
			ce_IP_reg.Q(ce_IP);
			ce_IP_reg.QN(UNCONNECTED2);

			ce_IR_reg.D(n_90);
			ce_IR_reg.C(clock);
			ce_IR_reg.Q(ce_IR);
			ce_IR_reg.QN(UNCONNECTED3);

			write_data_cache_reg.D(n_91);
			write_data_cache_reg.C(clock);
			write_data_cache_reg.Q(write_data_cache);
			write_data_cache_reg.QN(n_122);

			g2542.A(n_46);
			g2542.B(state[0]);
			g2542.C(n_87);
			g2542.Q(n_100);

			g2554.A(n_96);
			g2554.B(n_73);
			g2554.C(n_88);
			g2554.D(n_94);
			g2554.Q(n_99);

			g2532.A(n_27);
			g2532.B(n_84);
			g2532.C(sel_IP_add);
			g2532.Q(n_98);

			sel_IP_reg.D(n_82);
			sel_IP_reg.C(clock);
			sel_IP_reg.Q(sel_IP);
			sel_IP_reg.QN(n_123);

			g2552.A(n_96);
			g2552.B(n_39);
			g2552.C(n_80);
			g2552.Q(n_97);

			sel_ALU_reg1.D(n_79);
			sel_ALU_reg1.C(clock);
			sel_ALU_reg1.Q(sel_ALU_bus[1]);
			sel_ALU_reg1.QN(UNCONNECTED4);

			g2543.A(n_124);
			g2543.B(n_74);
			g2543.C(n_94);
			g2543.Q(n_95);

			g2544.A(n_83);
			g2544.B(n_35);
			g2544.C(n_77);
			g2544.Q(n_93);

			g2546.A(n_70);
			g2546.B(n_81);
			g2546.Q(n_92);

			g2565.A(n_122);
			g2565.B(n_68);
			g2565.C(n_51);
			g2565.Q(n_91);

			g2550.A(n_78);
			g2550.B(n_89);
			g2550.Q(n_90);

			g2569.A(n_69);
			g2569.B(ce_Z);
			g2569.Q(n_88);

			g2551.A(n_62);
			g2551.B(n_71);
			g2551.C(state[3]);
			g2551.Q(n_87);

			ce_instruction_code_reg.D(n_76);
			ce_instruction_code_reg.C(clock);
			ce_instruction_code_reg.Q(ce_instruction_code);
			ce_instruction_code_reg.QN(UNCONNECTED5);

			g2555.A(n_85);
			g2555.Q(n_86);

			g2556.A(n_60);
			g2556.B(end_of_job);
			g2556.C(n_63);
			g2556.D(n_16);
			g2556.Q(n_85);

			g2557.A(reset); //itt lesz egy hiba
			g2557.B(n_83);
			g2557.C(n_45);
			g2557.D(n_49);
			g2557.Q(n_84);

			g2549.A(n_123);
			g2549.B(n_58);
			g2549.C(n_81);
			g2549.Q(n_82);

			g2567.A(n_72);
			g2567.Q(n_80);

			state_reg3.D(n_67);
			state_reg3.C(clock);
			state_reg3.Q(state[3]);
			state_reg3.QN(n_59);

			g2553.A(n_30);
			g2553.B(n_55);
			g2553.C(n_18);
			g2553.D(n_66);
			g2553.Q(n_79);

			g2561.A(n_52);
			g2561.B(n_28);
			g2561.C(n_36);
			g2561.D(ce_IR);
			g2561.Q(n_78);

			g2547.A(n_43);
			g2547.B(state[1]);
			g2547.C(n_25);
			g2547.D(state[3]);
			g2547.Q(n_77);

			g2548.A(n_75);
			g2548.B(n_53);
			g2548.Q(n_76);

			g2564.A(n_73);
			g2564.B(n_38);
			g2564.C(n_65);
			g2564.Q(n_74);

			g2568.A(n_71);
			g2568.B(ss_instruction_code_bus[3]);
			g2568.C(n_24);
			g2568.D(n_56);
			g2568.Q(n_72);

			g2576.A(n_61);
			g2576.B(ce_IP);
			g2576.C(n_33);
			g2576.D(run);
			g2576.Q(n_70);

			g2577.A(n_68);
			g2577.Q(n_69);

			g2558.A(n_96);
			g2558.B(n_34);
			g2558.C(n_44);
			g2558.D(n_59);
			g2558.Q(n_67);

			g2559.A(n_65);
			g2559.B(n_40);
			g2559.C(sel_ALU_bus[1]);
			g2559.Q(n_66);

			g2560.A(n_42);
			g2560.B(n_14);
			g2560.C(n_63);
			g2560.Q(n_64);

			g2562.A(n_57);
			g2562.Q(n_81);

			g2575.A(n_50);
			g2575.B(ss_instruction_code_bus[3]);
			g2575.C(n_29);
			g2575.D(run);
			g2575.Q(n_62);

			g2578.A(n_47);
			g2578.B(n_61);
			g2578.Q(n_68);

			g2583.A(reset);
			g2583.B(n_59);
			g2583.C(n_58);
			g2583.Q(n_60);

			g2563.A(n_48);
			g2563.B(n_56);
			g2563.C(n_55);
			g2563.D(n_54);
			g2563.Q(n_57);

			g2566.A(n_52);
			g2566.B(n_6);
			g2566.C(n_20);
			g2566.D(ce_instruction_code);
			g2566.Q(n_53);

			g2571.A(n_50);
			g2571.B(n_41);
			g2571.C(ss_instruction_code_bus[1]);
			g2571.Q(n_51);

			g2573.A(n_48);
			g2573.B(n_12);
			g2573.C(n_47);
			g2573.Q(n_49);

			g2584.A(n_26);
			g2584.B(n_59);
			g2584.C(n_17);
			g2584.Q(n_75);

			g2589.A(n_45);
			g2589.Q(n_46);

			g2591.A(n_44);
			g2591.Q(n_61);

			g2570.A(ss_instruction_code_bus[1]);
			g2570.B(ss_instruction_code_bus[3]);
			g2570.C(n_15);
			g2570.D(n_22);
			g2570.Q(n_43);

			g2572.A(ss_instruction_code_bus[2]);
			g2572.B(n_13);
			g2572.C(n_56);
			g2572.D(n_55);
			g2572.Q(n_63);

			g2580.A(n_42);
			g2580.B(n_19);
			g2580.C(n_41);
			g2580.Q(n_73);

			g2581.A(n_32);
			g2581.Q(n_71);

			g2585.A(reset);	//itt nem lesz jo
			g2585.B(n_39);
			g2585.C(n_21);
			g2585.Q(n_40);

			g2586.A(n_5);
			g2586.B(n_38);
			g2586.C(n_37);
			g2586.Q(n_58);

			g2590.A(n_37);
			g2590.B(state[3]);
			g2590.Q(n_45);

			g2592.A(n_52);
			g2592.B(n_37);
			g2592.Q(n_44);

			g2595.A(n_55);
			g2595.B(ss_instruction_code_bus[2]);
			g2595.C(ss_instruction_code_bus[0]);
			g2595.Q(n_50);

			g2598.A(n_59);
			g2598.B(n_23);
			g2598.C(n_35);
			g2598.Q(n_36);

			g2574.A(n_31);
			g2574.B(n_10);
			g2574.C(state[0]);
			g2574.D(state[1]);
			g2574.Q(n_34);

			g2579.A(n_33);
			g2579.B(state[3]);
			g2579.Q(n_54);

			g2582.A(n_55);
			g2582.B(n_31);
			g2582.C(ss_instruction_code_bus[1]);
			g2582.Q(n_32);

			g2587.A(n_30);
			g2587.B(ss_instruction_code_bus[3]);
			g2587.Q(n_48);

			g2594.A(n_29);
			g2594.B(n_39);
			g2594.Q(n_89);

			g2596.A(n_39);
			g2596.B(n_28);
			g2596.C(state[0]);
			g2596.Q(n_94);

			g2604.A(n_35);
			g2604.Q(n_27);

			g2608.A(n_55);
			g2608.Q(n_26);

			g2588.A(n_24);
			g2588.B(n_23);
			g2588.Q(n_25);

			g2609.A(n_28);
			g2609.B(n_83);
			g2609.Q(n_55);

			g2593.A(state[1]);
			g2593.B(n_22);			
			g2593.Q(n_33);

			g2597.A(ss_instruction_code_bus[3]);
			g2597.B(n_3);
			g2597.C(n_52);
			g2597.Q(n_21);

			g2599.A(n_7);
			g2599.B(state[2]);
			g2599.C(n_96);
			g2599.D(n_59);
			g2599.Q(n_20);

			g2600.A(n_30);
			g2600.Q(n_19);

			g2610.A(n_18);
			g2610.Q(n_41);

			g2612.A(n_96);
			g2612.B(n_23);
			g2612.Q(n_37);

			g2605.A(n_47);
			g2605.B(n_17);
			g2605.Q(n_35);

			g2601.A(ss_instruction_code_bus[2]);
			g2601.B(n_16);
			g2601.Q(n_30);

			g2603.A(n_52);
			g2603.B(n_8);
			g2603.Q(n_15);

			g2606.A(n_22);
			g2606.Q(n_29);

			g2613.A(n_11);
			g2613.Q(n_14);

			g2611.A(n_39);
			g2611.B(n_13);
			g2611.Q(n_18);

			g2607.A(n_52);
			g2607.B(n_12);
			g2607.Q(n_22);

			g2614.A(n_4);
			g2614.B(ss_N);
			g2614.C(n_9);
			g2614.D(ss_Z);
			g2614.Q(n_11);

			g2615.A(n_9);
			g2615.B(ss_instruction_code_bus[1]);
			g2615.S(ss_instruction_code_bus[3]);
			g2615.Q(n_10);

			g2617.A(n_8);
			g2617.Q(n_31);

			g2624.A(n_7);
			g2624.Q(n_47);

			g2626.A(n_96);
			g2626.Q(n_28);

			g2628.A(n_39);
			g2628.Q(n_56);

			g2630.A(n_6);
			g2630.Q(n_23);

			g2602.A(n_65);
			g2602.B(state[0]);
			g2602.Q(n_24);
			
			g2625.A(n_59);
			g2625.B(n_38);
			g2625.Q(n_7);

			g2616.A(n_83);
			g2616.B(run);
			g2616.Q(n_5);

			g2619.A(n_4);
			g2619.B(n_9);
			g2619.Q(n_16);

			g2620.A(n_42);
			g2620.Q(n_3);
			
			g2627.A(state[2]);
			g2627.B(n_38);
			g2627.Q(n_96);

			g2631.A(reset);
			g2631.B(n_17);
			g2631.Q(n_6);

			g2618.A(ss_instruction_code_bus[2]);
			g2618.B(n_4);
			g2618.Q(n_8);

			g2629.A(state[3]);
			g2629.B(n_17);
			g2629.Q(n_39);

			g2623.A(reset);
			g2623.B(state[0]);
			g2623.Q(n_52);

			g2621.A(ss_instruction_code_bus[1]);
			g2621.B(ss_instruction_code_bus[0]);
			g2621.Q(n_42);
			
			g2622.A(reset);
			g2622.B(state[2]);
			g2622.Q(n_65);
			
			g2641.A(reset);
			g2641.Q(n_38);
			
			g2635.A(ss_instruction_code_bus[1]);
			g2635.Q(n_9);
			
			g2639.A(ss_instruction_code_bus[3]);
			g2639.Q(n_13);

			g2632.A(ss_instruction_code_bus[0]);
			g2632.Q(n_4);
			
			SC_METHOD(write_ss_instruction_code_bus);
			sensitive << ss_instruction_code;
			dont_initialize();
			
			SC_METHOD(write_sel_ALU);
			for(size_t it = 0; it < 2; ++it) sensitive << sel_ALU_bus[it];
			dont_initialize();
			
			SC_METHOD(write_we_RF);
			sensitive << ce_Z;
			dont_initialize();
			
			SC_METHOD(write_ce_N);
			sensitive << ce_Z;
			dont_initialize();
			
			SC_METHOD(write_ce_MDR);
			sensitive << ce_instruction_code;
			dont_initialize();
			
			SC_METHOD(write_ce_MAR);
			sensitive << ce_instruction_code;
			dont_initialize();
			
			SC_METHOD(write_ce_IMM);
			sensitive << ce_instruction_code;
			dont_initialize();
			
			SC_METHOD(write_ce_OP_A);
			sensitive << ce_instruction_code;
			dont_initialize();
			
			SC_METHOD(write_ce_OP_B);
			sensitive << ce_instruction_code;
			dont_initialize();
			
			SC_METHOD(write_ce_destination);
			sensitive << ce_instruction_code;
			dont_initialize();
						
		}
	};

}	//namespace demoproc_gates

#endif //_CONTROLLER_HPP_
