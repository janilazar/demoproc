#ifndef _REGISTER_8_HPP_
#define _REGISTER_8_HPP_

#include <logitherm>
#include "../ams035/ams035.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_ams035
{
	SC_MODULE(register_8)
	{
		sc_in<bool> clock;
		sc_in<sc_logic> reset, enable;
		sc_in<sc_lv<8>> input;
		sc_out<sc_lv<8>> output;
		
		sc_signal<sc_logic> UNCONNECTED, UNCONNECTED0, UNCONNECTED1, UNCONNECTED2, UNCONNECTED3, UNCONNECTED4, UNCONNECTED5, UNCONNECTED6;
		
		sc_signal<sc_logic> n_0, n_1, n_2, n_3, n_4, n_5, n_6, n_7, n_8, n_9, n_10, n_11, n_12, n_13, n_14, n_15;
		
		sc_vector<sc_signal<sc_logic>> in, out;
		
		layout::sunred::component_t layout_item;
		
		ams035::df3 dout_reg0;
		ams035::df3 dout_reg1;
		ams035::df3 dout_reg2;
		ams035::df3 dout_reg3;
		ams035::df3 dout_reg4;
		ams035::df3 dout_reg5;
		ams035::df3 dout_reg6;
		ams035::df3 dout_reg7;
		
		ams035::nor20 g33;
		ams035::nor20 g29;
		ams035::nor20 g34;
		ams035::nor20 g30;
		ams035::nor20 g35;
		ams035::nor20 g36;
		ams035::nor20 g31;
		ams035::nor20 g32;
		
		ams035::imux20 g40;
		ams035::imux20 g38;
		ams035::imux20 g41;
		ams035::imux20 g42;
		ams035::imux20 g43;
		ams035::imux20 g44;
		ams035::imux20 g37;
		ams035::imux20 g39;
		
		void write_in()
		{
			for(size_t it = 0; it < in.size(); ++it) in[it] = input.read()[it];
		}
		
		void write_output()
		{
			sc_lv<8> tmp;
			for(size_t it = 0; it < out.size(); ++it) tmp[it] = out[it];
			output.write(tmp);
		}
		
		register_8(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_adapter)
		:
			sc_core::sc_module(nm, systemc_adapter),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			layout_item(id, layer_name, position, layout::xy_length_t(84_um,65_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter)),
			in("in",8),
			out("out",8),
			dout_reg0("dout_reg0", layer_name, layout::xy_length_t(0_um,0_um),  &layout_item, systemc_adapter),
			dout_reg1("dout_reg1", layer_name, layout::xy_length_t(21_um,0_um),  &layout_item, systemc_adapter),
			dout_reg2("dout_reg2", layer_name, layout::xy_length_t(42_um,0_um), &layout_item, systemc_adapter),
			dout_reg3("dout_reg3", layer_name, layout::xy_length_t(63_um,0_um), &layout_item, systemc_adapter),
			dout_reg4("dout_reg4", layer_name, layout::xy_length_t(0_um,26_um),  &layout_item, systemc_adapter),
			dout_reg5("dout_reg5", layer_name, layout::xy_length_t(21_um,26_um),  &layout_item, systemc_adapter),
			dout_reg6("dout_reg6", layer_name, layout::xy_length_t(42_um,26_um), &layout_item, systemc_adapter),
			dout_reg7("dout_reg7", layer_name, layout::xy_length_t(63_um,26_um), &layout_item, systemc_adapter),
			
			g33("g33", layer_name, layout::xy_length_t(0_um,52_um), &layout_item, systemc_adapter),
			g29("g29", layer_name, layout::xy_length_t(7_um,52_um), &layout_item, systemc_adapter),
			g34("g34", layer_name, layout::xy_length_t(14_um,52_um), &layout_item, systemc_adapter),
			g30("g30", layer_name, layout::xy_length_t(21_um,52_um), &layout_item, systemc_adapter),
			g35("g35", layer_name, layout::xy_length_t(28_um,52_um), &layout_item, systemc_adapter),
			g36("g36", layer_name, layout::xy_length_t(35_um,52_um), &layout_item, systemc_adapter),
			g31("g31", layer_name, layout::xy_length_t(42_um,52_um), &layout_item, systemc_adapter),
			g32("g32", layer_name, layout::xy_length_t(49_um,52_um), &layout_item, systemc_adapter),
			
			g40("g40", layer_name, layout::xy_length_t(56_um,52_um), &layout_item, systemc_adapter),
			g38("g38", layer_name, layout::xy_length_t(59.5_um,52_um), &layout_item, systemc_adapter),
			g41("g41", layer_name, layout::xy_length_t(63_um,52_um), &layout_item, systemc_adapter),
			g42("g42", layer_name, layout::xy_length_t(66.5_um,52_um), &layout_item, systemc_adapter),
			g43("g43", layer_name, layout::xy_length_t(70_um,52_um), &layout_item, systemc_adapter),
			g44("g44", layer_name, layout::xy_length_t(73.5_um,52_um), &layout_item, systemc_adapter),
			g37("g37", layer_name, layout::xy_length_t(77_um,52_um), &layout_item, systemc_adapter),
			g39("g39", layer_name, layout::xy_length_t(80.5_um,52_um), &layout_item, systemc_adapter)
			
		{
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(dout_reg0.id);
				adapter->manager->add_dissipator_component(dout_reg1.id);
				adapter->manager->add_dissipator_component(dout_reg2.id);
				adapter->manager->add_dissipator_component(dout_reg3.id);
				adapter->manager->add_dissipator_component(dout_reg4.id);
				adapter->manager->add_dissipator_component(dout_reg5.id);
				adapter->manager->add_dissipator_component(dout_reg6.id);
				adapter->manager->add_dissipator_component(dout_reg7.id);
				
				adapter->manager->add_dissipator_component(g33.id);
				adapter->manager->add_dissipator_component(g29.id);
				adapter->manager->add_dissipator_component(g34.id);
				adapter->manager->add_dissipator_component(g30.id);
				adapter->manager->add_dissipator_component(g35.id);
				adapter->manager->add_dissipator_component(g36.id);
				adapter->manager->add_dissipator_component(g31.id);
				adapter->manager->add_dissipator_component(g32.id);
				
				adapter->manager->add_dissipator_component(g40.id);
				adapter->manager->add_dissipator_component(g38.id);
				adapter->manager->add_dissipator_component(g41.id);
				adapter->manager->add_dissipator_component(g42.id);
				adapter->manager->add_dissipator_component(g43.id);
				adapter->manager->add_dissipator_component(g44.id);
				adapter->manager->add_dissipator_component(g37.id);
				adapter->manager->add_dissipator_component(g39.id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_8);
			
			dout_reg0.C (clock);
			dout_reg0.D (n_11);
			dout_reg0.Q (out[0]);
			dout_reg0.QN(UNCONNECTED);

			dout_reg2.C (clock);
			dout_reg2.D (n_10);
			dout_reg2.Q (out[2]);
			dout_reg2.QN(UNCONNECTED0);

			dout_reg3.C (clock);
			dout_reg3.D (n_14);
			dout_reg3.Q (out[3]);
			dout_reg3.QN(UNCONNECTED1);

			dout_reg4.C (clock);
			dout_reg4.D (n_12);
			dout_reg4.Q (out[4]);
			dout_reg4.QN(UNCONNECTED2);

			dout_reg1.C (clock);
			dout_reg1.D (n_8);
			dout_reg1.Q (out[1]);
			dout_reg1.QN(UNCONNECTED3);

			dout_reg5.C (clock);
			dout_reg5.D (n_9);
			dout_reg5.Q (out[5]);
			dout_reg5.QN(UNCONNECTED4);

			dout_reg6.C (clock);
			dout_reg6.D (n_15);
			dout_reg6.Q (out[6]);
			dout_reg6.QN(UNCONNECTED5);

			dout_reg7.C (clock);
			dout_reg7.D (n_13);
			dout_reg7.Q (out[7]);
			dout_reg7.QN(UNCONNECTED6);

			g33.A (reset);
			g33.B (n_4);
			g33.Q (n_15);

			g29.A (reset);
			g29.B (n_0);
			g29.Q (n_14);

			g34.A (reset);
			g34.B (n_2);
			g34.Q (n_13);

			g30.A (reset);
			g30.B (n_7);
			g30.Q (n_12);

			g35.A (reset);
			g35.B (n_6);
			g35.Q (n_11);

			g36.A (reset);
			g36.B (n_1);
			g36.Q (n_10);

			g31.A (reset);
			g31.B (n_5);
			g31.Q (n_9);

			g32.A (reset);
			g32.B (n_3);
			g32.Q (n_8);

			g40.A (out[4]);
			g40.B (in[4]);
			g40.S (enable);
			g40.Q (n_7);

			g38.A (out[0]);
			g38.B (in[0]);
			g38.S (enable);
			g38.Q (n_6);

			g41.A (out[5]);
			g41.B (in[5]);
			g41.S (enable);
			g41.Q (n_5);

			g42.A (out[6]);
			g42.B (in[6]);
			g42.S (enable);
			g42.Q (n_4);

			g43.A (out[1]);
			g43.B (in[1]);
			g43.S (enable);
			g43.Q (n_3);

			g44.A (out[7]);
			g44.B (in[7]);
			g44.S (enable);
			g44.Q (n_2);

			g37.A (out[2]);
			g37.B (in[2]);
			g37.S (enable);
			g37.Q (n_1);

			g39.A (out[3]);
			g39.B (in[3]);
			g39.S (enable);
			g39.Q (n_0);

			SC_METHOD(write_in);
			sensitive << input;
			dont_initialize();
			
			SC_METHOD(write_output);
			for(size_t it = 0; it < 8; ++it) sensitive << out[it];
			dont_initialize();
		
		}
	};
}	//namespace demoproc_ams035
#endif //_REGISTER_8_HPP_

