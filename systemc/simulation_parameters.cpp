#include "simulation_parameters.hpp"

const simulation_parameters* parameter = nullptr; //ez mindig a fenti map egyik elemere mutat

#include <math.h>
//  Windows
#ifdef _WIN32
#include <Windows.h>
double simulation_parameters::get_wall_time() const
{
    LARGE_INTEGER time,freq;
    if (!QueryPerformanceFrequency(&freq))
    {
        //  Handle error
        return 0;
    }
    if (!QueryPerformanceCounter(&time))
    {
        //  Handle error
        return 0;
    }
    return (double)time.QuadPart / freq.QuadPart;
}

double simulation_parameters::get_cpu_time() const
{
    FILETIME a,b,c,d;
    if (GetProcessTimes(GetCurrentProcess(),&a,&b,&c,&d) != 0)
    {
        //  Returns total user time.
        //  Can be tweaked to include kernel times as well.
        return
            (double)(d.dwLowDateTime |
            ((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
    }
    else
    {
        //  Handle error
        return 0;
    }
}

//  Posix/Linux
#else

#include <sys/time.h>
    double simulation_parameters::get_wall_time() const
    {
        struct timeval time;
        if (gettimeofday(&time,NULL))
        {
            //  Handle error
            return 0;
        }
        return (double)time.tv_sec + (double)time.tv_usec * .000001;
    }

    double simulation_parameters::get_cpu_time() const
    {
        return (double)clock() / CLOCKS_PER_SEC;
    }
#endif

simulation_parameters::simulation_parameters(const std::string& id)
:
    id(id)
{
    layout_size = layout::xy_length_t(h_margin*2.0+core_width*h_core_count+h_core_space*(h_core_count-1.0), v_margin*2.0+core_height*v_core_count+v_core_space*(v_core_count-1.0));
    bulk_layer_thickness = soi_die == true ? (die_thickness - dissipating_layer_thickness - insulating_layer_thickness)/number_of_bulk_layers : (die_thickness - dissipating_layer_thickness)/number_of_bulk_layers;

    clock_period = 1.0/clock_frequency;

    std::fill(program_initializer.begin() + 43, program_initializer.end(), 45056);

    std::fill(data_initializer.begin() + 2, data_initializer.end(), 0);
}