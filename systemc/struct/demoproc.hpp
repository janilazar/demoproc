#ifndef _DEMOPROC_STRUCTURAL_
#define _DEMOPROC_STRUCTURAL_

#include <logitherm>
#include <vector>

#include "../simulation_parameters.hpp"
#include "../ams035/ams035.hpp"

namespace demoproc_struct
{
	
	/**
	 * standard register
	**/
	template <size_t size>
	SC_MODULE(reg)
	{
		public:
			sc_in<sc_lv<size>> input;
			sc_in<bool> clock;
			sc_in<sc_logic> reset;
			sc_in<sc_logic> enable;

			sc_out<sc_lv<size>> output;
			
			
			void operation()
			{
				if (clock.read() == true)
				{
					if (reset.read() == SC_LOGIC_1)
					{
						output.write(sc_lv<size>("0"));
					}
					else if (enable.read() == SC_LOGIC_1)
					{
						output.write(input.read());
					}
				}
			}
		
			SC_CTOR (reg)
			{
				
				SC_METHOD(operation);
				sensitive << clock.pos();
				dont_initialize();
			}
	};
	
	SC_MODULE(register_1)
	{
		sc_in<sc_logic> input;
		sc_in<sc_logic> reset, enable;
		sc_in<bool> clock;
		sc_out<sc_logic> output;
		
		reg<1> data_reg;
		
		sc_signal<sc_lv<1>> in, out;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::df3::leakage_current[it];
					leakage_current[it] += ams035::nor20::leakage_current[it];
					leakage_current[it] += ams035::imux20::leakage_current[it];
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input_activity;
		logic::systemc::activity_t reset_activity;
		logic::systemc::activity_t enable_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input_dissipation() 
		{
			return 8.332_fJ; //8.332e-09*1e-6;
		}

		unit::energy_t reset_dissipation() 
		{
			return -39.94_fJ; //-3.994e-08*1e-6;
		}
		
		unit::energy_t enable_dissipation() 
		{
			return 79.22_fJ; //7.922e-08*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 1.05_pJ; //1.050e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return 0.3246_pJ; //3.246e-07*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{

			// ams035::df3::leakage_current
			// ams035::nor20::leakage_current
			// ams035::imux20::leakage_current
			// itt ugyanaz a moka lesz mint ams035nel
			// minden ilyen modulnak lesz egy 9 elemu leakage_current tombje, ami statikus konstans, es egy statikus fv inicializalja majd
			// return 4_pW; //0.004e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		void write_in()
		{
			in = (input.read() == SC_LOGIC_1) ? "1" : "0";
		}
		
		void write_output()
		{
			output.write((out.read() == sc_lv<1>("1")) ? SC_LOGIC_1 : SC_LOGIC_0);
		}
		
		
		register_1(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			data_reg("register"),
			input_activity("input_activity",std::bind(&register_1::input_dissipation, this)),
			reset_activity("reset_activity",std::bind(&register_1::reset_dissipation, this)),
			enable_activity("enable_activity",std::bind(&register_1::enable_dissipation, this)),
			output_activity("output_activity",std::bind(&register_1::output_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&register_1::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(31.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();

			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_1);
			
			data_reg.input(in);
			data_reg.clock(clock);
			data_reg.reset(reset);
			data_reg.enable(enable);
			data_reg.output(out);
			
			SC_METHOD(write_in);
			sensitive << input;
			dont_initialize();
			
			SC_METHOD(write_output);
			sensitive << out;
			dont_initialize();
			
			SC_ACTIVITY(input_activity);
			sensitive << input;
			dont_initialize();
			
			SC_ACTIVITY(reset_activity);
			sensitive << reset;
			dont_initialize();
			
			SC_ACTIVITY(enable_activity);
			sensitive << enable;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive <<  clock.pos(); //input << reset << enable << output;
			dont_initialize();
		}
	};
	
	SC_MODULE(register_4)
	{
		sc_in<sc_lv<4>> input;
		sc_in<sc_logic> reset, enable;
		sc_in<bool> clock;
		sc_out<sc_lv<4>> output;
		
		reg<4> data_reg;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::df3::leakage_current[it] * 4.0;
					leakage_current[it] += ams035::nor20::leakage_current[it] * 4.0;
					leakage_current[it] += ams035::imux20::leakage_current[it] * 4.0;
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input_activity;
		logic::systemc::activity_t reset_activity;
		logic::systemc::activity_t enable_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input_dissipation() 
		{
			return 5.944_fJ;//5.944e-09*1e-6;
		}

		unit::energy_t reset_dissipation() 
		{
			return -0.2770_pJ;//-2.770e-07*1e-6;
		}
		
		unit::energy_t enable_dissipation() 
		{
			return 0.3846_pJ;//3.846e-07*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 2.312_pJ; //2.312e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return 1.298_pJ; //1.298e-06*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 15_pW; //0.015e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		register_4(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			data_reg("register"),
			input_activity("input_activity",std::bind(&register_4::input_dissipation, this)),
			reset_activity("reset_activity",std::bind(&register_4::reset_dissipation, this)),
			enable_activity("enable_activity",std::bind(&register_4::enable_dissipation, this)),
			output_activity("output_activity",std::bind(&register_4::output_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&register_4::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(42_um,65_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();

			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_4);
			
			data_reg.input(input);
			data_reg.clock(clock);
			data_reg.reset(reset);
			data_reg.enable(enable);
			data_reg.output(output);
			
			SC_ACTIVITY(input_activity);
			sensitive << input;
			dont_initialize();
			
			SC_ACTIVITY(reset_activity);
			sensitive << reset;
			dont_initialize();
			
			SC_ACTIVITY(enable_activity);
			sensitive << enable;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << clock.pos(); //input << reset << enable << output;
			dont_initialize();
			
		}
	};
	
	SC_MODULE(register_8)
	{
		sc_in<sc_lv<8>> input;
		sc_in<sc_logic> reset, enable;
		sc_in<bool> clock;
		sc_out<sc_lv<8>> output;
		
		reg<8> data_reg;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::df3::leakage_current[it] * 8.0;
					leakage_current[it] += ams035::nor20::leakage_current[it] * 8.0;
					leakage_current[it] += ams035::imux20::leakage_current[it] * 8.0;
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input_activity;
		logic::systemc::activity_t reset_activity;
		logic::systemc::activity_t enable_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input_dissipation() 
		{
			return 10.55_fJ; //1.055e-08*1e-6;
		}

		unit::energy_t reset_dissipation() 
		{
			return -0.3445_pJ; //-3.445e-07*1e-6;
		}
		
		unit::energy_t enable_dissipation() 
		{
			return 0.614_pJ; //6.140e-07*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 4.341_pJ; //4.341e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return 2.597_pJ; //2.597e-06*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 29_pW;//0.029e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		register_8(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			data_reg("register"),
			input_activity("input_activity",std::bind(&register_8::input_dissipation, this)),
			reset_activity("reset_activity",std::bind(&register_8::reset_dissipation, this)),
			enable_activity("enable_activity",std::bind(&register_8::enable_dissipation, this)),
			output_activity("output_activity",std::bind(&register_8::output_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&register_8::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(84_um,65_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();

			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_8);
			
			data_reg.input(input);
			data_reg.clock(clock);
			data_reg.reset(reset);
			data_reg.enable(enable);
			data_reg.output(output);
	
			SC_ACTIVITY(input_activity);
			sensitive << input;
			dont_initialize();
			
			SC_ACTIVITY(reset_activity);
			sensitive << reset;
			dont_initialize();
			
			SC_ACTIVITY(enable_activity);
			sensitive << enable;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << clock.pos(); //input << reset << enable << output;
			dont_initialize();
			
		}
	};
	
	SC_MODULE(register_16)
	{
		sc_in<sc_lv<16>> input;
		sc_in<sc_logic> reset, enable;
		sc_in<bool> clock;
		sc_out<sc_lv<16>> output;
		
		reg<16> data_reg;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::df3::leakage_current[it] * 16.0;
					leakage_current[it] += ams035::nor20::leakage_current[it] * 16.0;
					leakage_current[it] += ams035::imux20::leakage_current[it] * 16.0;
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input_activity;
		logic::systemc::activity_t reset_activity;
		logic::systemc::activity_t enable_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input_dissipation() 
		{
			return 21.15_fJ; //2.115e-08*1e-6;
		}

		unit::energy_t reset_dissipation() 
		{
			return -0.6934_pJ; //-6.934e-07*1e-6;
		}
		
		unit::energy_t enable_dissipation() 
		{
			return 1.201_pJ; //1.201e-06*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 8.589_pJ; //8.589e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return 5.196_pJ; //5.196e-06*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 59_pW; //0.059e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		register_16(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input("input"),
			reset("reset"),
			enable("enable"),
			clock("clock"),
			output("output"),
			data_reg("register"),
			input_activity("input_activity",std::bind(&register_16::input_dissipation, this)),
			reset_activity("reset_activity",std::bind(&register_16::reset_dissipation, this)),
			enable_activity("enable_activity",std::bind(&register_16::enable_dissipation, this)),
			output_activity("output_activity",std::bind(&register_16::output_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&register_16::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(84_um,143_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();
			
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input, parameter->id);
				add_activity_trace(parameter->trace_path, &reset, parameter->id);
				add_activity_trace(parameter->trace_path, &enable, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(register_16);
			
			data_reg.input(input);
			data_reg.clock(clock);
			data_reg.reset(reset);
			data_reg.enable(enable);
			data_reg.output(output);
			
			SC_ACTIVITY(input_activity);
			sensitive << input;
			dont_initialize();
			
			SC_ACTIVITY(reset_activity);
			sensitive << reset;
			dont_initialize();
			
			SC_ACTIVITY(enable_activity);
			sensitive << enable;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << clock.pos(); //input << reset << enable << output;
			dont_initialize();
			
		}
	};
	
	/**
	 * adder
	**/ 
	SC_MODULE(adder_8)
	{
		sc_in<sc_lv<8>> input0;
		sc_in<sc_lv<8>> input1;
		sc_out<sc_lv<8>> result;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::xor30::leakage_current[it];
					leakage_current[it] += ams035::add31::leakage_current[it];
					leakage_current[it] += ams035::add31::leakage_current[it];
					leakage_current[it] += ams035::add31::leakage_current[it];
					leakage_current[it] += ams035::add31::leakage_current[it];
					leakage_current[it] += ams035::add31::leakage_current[it];
					leakage_current[it] += ams035::add31::leakage_current[it];
					leakage_current[it] += ams035::add21::leakage_current[it];
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input0_activity;
		logic::systemc::activity_t input1_activity;
		logic::systemc::activity_t result_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input0_dissipation() 
		{
			return 1.216_pJ; //1.216e-6*1e-6;
		}
		
		unit::energy_t input1_dissipation() 
		{
			return 1.033_pJ; //1.033e-6*1e-6;
		}
		
		unit::energy_t result_dissipation() 
		{
			return 2.493_pJ; //2.493e-6*1e-6;
		}
		
		unit::energy_t intercept_dissipation()
		{
			return 0.4955_fJ; //4.955e-10*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 0.0_W;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		void operation() {
			result.write(sc_lv<8>(sc_int<8>(input0.read()) + sc_int<8>(input1.read()))); //ez igy nem lesz jo, sc_bv<> tipusra nincs osszeadas
		}

		adder_8(const sc_module_name& nm,  const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input0("input0"),
			input1("input1"),
			result("result"),
			input0_activity("input0_activity",std::bind(&adder_8::input0_dissipation, this)),
			input1_activity("input1_activity",std::bind(&adder_8::input1_dissipation, this)),
			result_activity("result_activity",std::bind(&adder_8::result_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&adder_8::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(56_um,65_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();
						
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &input0, parameter->id);
				add_activity_trace(parameter->trace_path, &input1, parameter->id);
				add_activity_trace(parameter->trace_path, &result, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(adder_8);				
			
			SC_METHOD(operation);
			sensitive << input0 << input1;
			dont_initialize();
			
			SC_ACTIVITY(input0_activity);
			sensitive << input0;
			dont_initialize();
			
			SC_ACTIVITY(input1_activity);
			sensitive << input1;
			dont_initialize();
			
			SC_ACTIVITY(result_activity);
			sensitive << result;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << result;
			dont_initialize();
		}
	};
	
	/**
	 * range selector
	**/ 
	template <size_t size, size_t msb, size_t lsb>
	SC_MODULE(range_selector)
	{
		public:
			static constexpr size_t output_size = msb - lsb + 1;

			sc_in<sc_lv<size>> in;
			sc_out<sc_lv<output_size>> out;

			void operation()
			{
				sc_lv<output_size> result;

				for (size_t i = 0; i < output_size; ++i)
				{
					result[i] = in.read()[i + lsb];
				}
				out.write(result);
			}

			SC_CTOR(range_selector)
			{
				static_assert(lsb <= msb, "The value of lsb has to be smaller than or equal to that of msb in range_selector.");
				
				SC_METHOD(operation);
				sensitive << in;
				dont_initialize();
			}
	};

	/**
	 * multiplexer
	**/ 
	template <size_t address_width, size_t data_width>
	SC_MODULE(mux)
	{
		public:
			sc_vector<sc_in<sc_lv<data_width>>> input;
			sc_in<sc_lv<address_width>> select;
			sc_out<sc_lv<data_width>> output;

			void operation()
			{
				try
				{
					output.write(input[select.read().to_uint()].read()); //nyil helyett talan . kene?
				}
				catch (std::domain_error const &)
				{
					output.write(sc_lv<data_width>()); //ez a sor mit csinal? valamilyen default erteket ir ki?
				}
			}

		public:
			SC_CTOR(mux)
			:
				input("input",size_t(1 << address_width))
			{

				SC_METHOD(operation)
				sensitive << select;
				for( unsigned i = 0; i<input.size(); ++i )
					sensitive << input[i];
				dont_initialize();
				
			}
	};
	
	SC_MODULE(mux2_8)
	{
		sc_in<sc_lv<8>> input0, input1;
		sc_in<sc_logic> select;
		sc_out<sc_lv<8>> output;
		
		mux<1,8> multiplexer;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::mux21::leakage_current[it] * 8.0;
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input0_activity;
		logic::systemc::activity_t input1_activity;
		logic::systemc::activity_t select_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input0_dissipation() 
		{
			return -74.01_fJ; //-7.401e-08*1e-6;
		}
		
		unit::energy_t input1_dissipation() 
		{
			return -52.71_fJ; //-5.271e-08*1e-6;
		}
		
		unit::energy_t select_dissipation() 
		{
			return 0.57_pJ; //5.700e-07*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 1.032_pJ; //1.032e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return 8.164_fJ; //8.164e-09*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 7_pW; //0.007e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		sc_signal<sc_lv<1>> sel;		

		void write_sel()
		{
			sel = (select == SC_LOGIC_1) ? "1" : "0";
		}
		
		mux2_8(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input0("input0"),
			input1("input1"),
			select("select"),
			output("output"),
			multiplexer("multiplexer"),
			input0_activity("input0_activity",std::bind(&mux2_8::input0_dissipation, this)),
			input1_activity("input1_activity",std::bind(&mux2_8::input1_dissipation, this)),
			select_activity("select_activity",std::bind(&mux2_8::select_dissipation, this)),
			output_activity("output_activity",std::bind(&mux2_8::output_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&mux2_8::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(28_um,39_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();
						
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path,&input0, parameter->id);
				add_activity_trace(parameter->trace_path,&input1, parameter->id);
				add_activity_trace(parameter->trace_path,&select, parameter->id);
				add_activity_trace(parameter->trace_path,&output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(mux2_8);
			
			multiplexer.input[0](input0);
			multiplexer.input[1](input1);
			multiplexer.select(sel);
			multiplexer.output(output);
			
			SC_METHOD(write_sel);
			sensitive << select;
			dont_initialize();
			
			SC_ACTIVITY(input0_activity);
			sensitive << input0;
			dont_initialize();
			
			SC_ACTIVITY(input1_activity);
			sensitive << input1;
			dont_initialize();
			
			SC_ACTIVITY(select_activity);
			sensitive << select;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << input0 << input1 << select << output;
			dont_initialize();
		}
	};
	
	
	SC_MODULE(mux4_8)
	{
		sc_in<sc_lv<8>> input0, input1, input2, input3;
		sc_in<sc_lv<2>> select;
		sc_out<sc_lv<8>> output;
		
		mux<2,8> multiplexer;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
					leakage_current[it] += ams035::mux41::leakage_current[it];
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t input0_activity;
		logic::systemc::activity_t input1_activity;
		logic::systemc::activity_t input2_activity;
		logic::systemc::activity_t input3_activity;
		logic::systemc::activity_t select_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t input0_dissipation() 
		{
			return -14.56_fJ; //-1.456e-08*1e-6;
		}
		
		unit::energy_t input1_dissipation() 
		{
			return 3.934_fJ; //3.934e-09*1e-6;
		}
		
		unit::energy_t input2_dissipation() 
		{
			return 0.742_fJ; //7.420e-10*1e-6;
		}
		
		unit::energy_t input3_dissipation() 
		{
			return 10.11_fJ; //1.011e-08*1e-6;
		}
		
		unit::energy_t select_dissipation() 
		{
			return 1.199_pJ; //1.199e-06*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 1.523_pJ; //1.523e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return -4.038_fJ; //-4.038e-09*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 12_pW; //0.012e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		mux4_8(const sc_module_name& nm,  const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			input0("input0"),
			input1("input1"),
			input2("input2"),
			input3("input3"),
			select("select"),
			output("output"),
			multiplexer("multiplexer"),
			input0_activity("input0_activity",std::bind(&mux4_8::input0_dissipation, this)),
			input1_activity("input1_activity",std::bind(&mux4_8::input1_dissipation, this)),
			input2_activity("input2_activity",std::bind(&mux4_8::input2_dissipation, this)),
			input3_activity("input3_activity",std::bind(&mux4_8::input3_dissipation, this)),
			select_activity("select_activity",std::bind(&mux4_8::select_dissipation, this)),
			output_activity("output_activity",std::bind(&mux4_8::output_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&mux4_8::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(52.5_um,65_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();
			
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path,&input0, parameter->id);
				add_activity_trace(parameter->trace_path,&input1, parameter->id);
				add_activity_trace(parameter->trace_path,&input2, parameter->id);
				add_activity_trace(parameter->trace_path,&input3, parameter->id);
				add_activity_trace(parameter->trace_path,&select, parameter->id);
				add_activity_trace(parameter->trace_path,&output, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(mux4_8);
			
			multiplexer.input[0](input0);
			multiplexer.input[1](input1);
			multiplexer.input[2](input2);
			multiplexer.input[3](input3);
			multiplexer.select(select);
			multiplexer.output(output);
			
			SC_ACTIVITY(input0_activity);
			sensitive << input0;
			dont_initialize();
			
			SC_ACTIVITY(input1_activity);
			sensitive << input1;
			dont_initialize();
			
			SC_ACTIVITY(input2_activity);
			sensitive << input1;
			dont_initialize();
			
			SC_ACTIVITY(input3_activity);
			sensitive << input1;
			dont_initialize();

			SC_ACTIVITY(select_activity);
			sensitive << select;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << input0 << input1 << input2 << input3 << select << output;
			dont_initialize();

		}
	};
	
	
	
	/**
	 * register file
	**/ 
	SC_MODULE(regfile)
	{

		private:
			std::array<sc_lv<8>, 16> data_;

		public:
			sc_in<bool> clock;
			sc_in<sc_logic> write_enable;
			sc_in<sc_lv<4>> write_address;
			sc_in<sc_lv<8>> data_in;
			sc_in<sc_lv<4>> read_address_a;
			sc_in<sc_lv<4>> read_address_b;
			sc_in<sc_lv<4>> read_address_c;
			sc_out<sc_lv<8>> data_out_a;
			sc_out<sc_lv<8>> data_out_b;
			sc_out<sc_lv<8>> data_out_c;

			static bool leakage_initialized;
			static unit::current_t leakage_current[9];

			static void initialize_leakage_current()
			{
				if(leakage_initialized == false)
				{
					for(size_t it = 0; it < 9; ++it)
					{
						leakage_current[it] = 0_pA;
						leakage_current[it] += ams035::dfe1::leakage_current[it] * 128.0;
						leakage_current[it] += ams035::df3::leakage_current[it] * 24.0;
						
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi210::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::aoi2110::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::nor30::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::nor40::leakage_current[it];
						leakage_current[it] += ams035::nor40::leakage_current[it];	
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor30::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
					}
					leakage_initialized = true;
				}
			}
			
			logic::systemc::activity_t write_enable_activity;
			logic::systemc::activity_t write_address_activity;
			logic::systemc::activity_t data_in_activity;
			logic::systemc::activity_t read_address_a_activity;
			logic::systemc::activity_t read_address_b_activity;
			logic::systemc::activity_t read_address_c_activity;
			logic::systemc::activity_t data_out_a_activity;
			logic::systemc::activity_t data_out_b_activity;
			logic::systemc::activity_t data_out_c_activity;
			logic::systemc::activity_t intercept_activity;
			
			layout::sunred::component_t layout_item;
			
			unit::energy_t write_enable_dissipation() 
			{
				return 3.004_pJ; //3.004e-06*1e-6;
			}
			
			unit::energy_t write_address_dissipation() 
			{
				return -0.3272_pJ; //-3.272e-07*1e-6;
			}
			
			unit::energy_t data_in_dissipation() 
			{
				return 0.3935_pJ; //3.935e-07*1e-6;
			}
			
			unit::energy_t read_address_a_dissipation() 
			{
				return 9.571_pJ; //9.571e-06*1e-6;
			}
			
			unit::energy_t read_address_b_dissipation() 
			{
				return 8.993_pJ; //8.993e-06*1e-6;
			}
			
			unit::energy_t read_address_c_dissipation() 
			{
				return 9.188_pJ; //9.188e-06*1e-6;
			}
			
			unit::energy_t data_out_a_dissipation() 
			{
				return 0.624_pJ; //6.240e-07*1e-6;
			}
			
			unit::energy_t data_out_b_dissipation() 
			{
				return 0.4661_pJ; //4.661e-07*1e-6;
			}
			
			unit::energy_t data_out_c_dissipation() 
			{
				return 0.4808_pJ; //4.808e-07*1e-6;
			}
			
			unit::energy_t intercept_dissipation()
			{
				return 40.11_pJ; //4.011e-05*1e-6;
			}
			
			unit::power_t component_static_dissipation() const override 
			{
				// return 582_pW; //0.582e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
			}

			void write()
			{
				if (clock.read() == true && write_enable.read() == SC_LOGIC_1)
				{ 
					data_[write_address.read().to_uint()] = data_in.read(); //This operation will throw std::domain_error if write_address is undefined

				}
			}

			void read()
			{
				try {
					if (clock.read() == true)
					{
						data_out_a.write(data_[read_address_a.read().to_uint()]);	//itt majd meg kell csinalni a castolast
						data_out_b.write(data_[read_address_b.read().to_uint()]);	//itt majd meg kell csinalni a castolast
						data_out_c.write(data_[read_address_c.read().to_uint()]);	//itt majd meg kell csinalni a castolast
					}
				}
				catch (std::domain_error const &)
				{
					data_out_a.write(sc_bv<8>());
				}
			}

			regfile(const sc_module_name& name_,  const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
			:
				sc_core::sc_module(name_, systemc_engine),
				data_(),
				clock("clock"),
				write_enable("write_enable"),
				write_address("write_address"),
				data_in("data_in"),
				read_address_a("read_address_a"),
				read_address_b("read_address_b"),
				read_address_c("read_address_c"),
				data_out_a("data_out_a"),
				data_out_b("data_out_b"),
				data_out_c("data_out_c"),
				write_enable_activity("write_enable_activity",std::bind(&regfile::write_enable_dissipation, this)),
				write_address_activity("write_address_activity",std::bind(&regfile::write_address_dissipation, this)),
				data_in_activity("data_in_activity",std::bind(&regfile::data_in_dissipation, this)),
				read_address_a_activity("read_address_a_activity",std::bind(&regfile::read_address_a_dissipation, this)),
				read_address_b_activity("read_address_b_activity",std::bind(&regfile::read_address_b_dissipation, this)),
				read_address_c_activity("read_address_c_activity",std::bind(&regfile::read_address_c_dissipation, this)),
				data_out_a_activity("data_out_a_activity",std::bind(&regfile::data_out_a_dissipation, this)),
				data_out_b_activity("data_out_b_activity",std::bind(&regfile::data_out_b_dissipation, this)),
				data_out_c_activity("data_out_c_activity",std::bind(&regfile::data_out_c_dissipation, this)),
				intercept_activity("intercept_activity",std::bind(&regfile::intercept_dissipation, this)),
				layout_item(id, layer_name, position, layout::xy_length_t(385_um,403_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
			{
				initialize_leakage_current();
							
				if(parameter->add_dissipators)
				{
					adapter->manager->add_dissipator_component(id);
					adapter->manager->add_display_component(id);
				}
				
				if(parameter->trace_activity)
				{
					add_activity_trace(parameter->trace_path, &write_enable, parameter->id);
					add_activity_trace(parameter->trace_path, &write_address, parameter->id);
					add_activity_trace(parameter->trace_path, &data_in, parameter->id);
					add_activity_trace(parameter->trace_path, &read_address_a, parameter->id);
					add_activity_trace(parameter->trace_path, &read_address_b, parameter->id);
					add_activity_trace(parameter->trace_path, &read_address_c, parameter->id);
					add_activity_trace(parameter->trace_path, &data_out_a, parameter->id);
					add_activity_trace(parameter->trace_path, &data_out_b, parameter->id);
					add_activity_trace(parameter->trace_path, &data_out_c, parameter->id);
				}
				
				if(parameter->trace_dissipation)
				{
					add_static_dissipation_trace(parameter->trace_path, parameter->id);
					add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
				}
				
				if(parameter->trace_temperature)
				{
					layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
					layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
					layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
				}
				
				SC_HAS_PROCESS(regfile);
				
				SC_METHOD(write);
				sensitive << clock.pos() << write_enable;
				dont_initialize();
				
				SC_METHOD(read);
				sensitive << clock.pos();	//ez minek?
				dont_initialize();
				
				
				SC_ACTIVITY(write_enable_activity);
				sensitive << write_enable;
				dont_initialize();
				
				SC_ACTIVITY(write_address_activity);
				sensitive << write_address;
				dont_initialize();
				
				SC_ACTIVITY(data_in_activity);
				sensitive << data_in;
				dont_initialize();
				
				SC_ACTIVITY(read_address_a_activity);
				sensitive << read_address_a;
				dont_initialize();
				
				SC_ACTIVITY(read_address_b_activity);
				sensitive << read_address_b;
				dont_initialize();
				
				SC_ACTIVITY(read_address_c_activity);
				sensitive << read_address_c;
				dont_initialize();
				
				SC_ACTIVITY(data_out_a_activity);
				sensitive << data_out_a;
				dont_initialize();
				
				SC_ACTIVITY(data_out_b_activity);
				sensitive << data_out_b;
				dont_initialize();
				
				SC_ACTIVITY(data_out_c_activity);
				sensitive << data_out_c;
				dont_initialize();
				
				SC_ACTIVITY(intercept_activity);
				sensitive << clock.pos();//write_enable << write_address << data_in << read_address_a << read_address_b << read_address_c << data_out_a << data_out_b << data_out_c;
				dont_initialize();
			}
			
			regfile(const sc_module_name& name_, std::array<sc_lv<8>,16> init, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
			:
				sc_core::sc_module(name_, systemc_engine),
				data_(init),
				clock("clock"),
				write_enable("write_enable"),
				write_address("write_address"),
				data_in("data_in"),
				read_address_a("read_address_a"),
				read_address_b("read_address_b"),
				read_address_c("read_address_c"),
				data_out_a("data_out_a"),
				data_out_b("data_out_b"),
				data_out_c("data_out_c"),
				write_enable_activity("write_enable_activity",std::bind(&regfile::write_enable_dissipation, this)),
				write_address_activity("write_address_activity",std::bind(&regfile::write_address_dissipation, this)),
				data_in_activity("data_in_activity",std::bind(&regfile::data_in_dissipation, this)),
				read_address_a_activity("read_address_a_activity",std::bind(&regfile::read_address_a_dissipation, this)),
				read_address_b_activity("read_address_b_activity",std::bind(&regfile::read_address_b_dissipation, this)),
				read_address_c_activity("read_address_c_activity",std::bind(&regfile::read_address_c_dissipation, this)),
				data_out_a_activity("data_out_a_activity",std::bind(&regfile::data_out_a_dissipation, this)),
				data_out_b_activity("data_out_b_activity",std::bind(&regfile::data_out_b_dissipation, this)),
				data_out_c_activity("data_out_c_activity",std::bind(&regfile::data_out_c_dissipation, this)),
				intercept_activity("intercept_activity",std::bind(&regfile::intercept_dissipation, this)),
				layout_item(id, layer_name, position, layout::xy_length_t(385_um,403_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
			{
				initialize_leakage_current();
				
				if(parameter->add_dissipators)
				{
					adapter->manager->add_dissipator_component(id);
					adapter->manager->add_display_component(id);
				}
				
				if(parameter->trace_activity)
				{
					add_activity_trace(parameter->trace_path, &write_enable, parameter->id);
					add_activity_trace(parameter->trace_path, &write_address, parameter->id);
					add_activity_trace(parameter->trace_path, &data_in, parameter->id);
					add_activity_trace(parameter->trace_path, &read_address_a, parameter->id);
					add_activity_trace(parameter->trace_path, &read_address_b, parameter->id);
					add_activity_trace(parameter->trace_path, &read_address_c, parameter->id);
					add_activity_trace(parameter->trace_path, &data_out_a, parameter->id);
					add_activity_trace(parameter->trace_path, &data_out_b, parameter->id);
					add_activity_trace(parameter->trace_path, &data_out_c, parameter->id);
				}
				
				if(parameter->trace_dissipation)
				{
					add_static_dissipation_trace(parameter->trace_path, parameter->id);
					add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
				}
				
				if(parameter->trace_temperature)
				{
					layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
					layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
					layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
				}

				SC_HAS_PROCESS(regfile);
				
				SC_METHOD(write);
				sensitive << clock.pos() << write_enable;
				dont_initialize();
				
				SC_METHOD(read);
				sensitive << clock.pos();	//ez minek?
				dont_initialize();
				
				SC_ACTIVITY(write_enable_activity);
				sensitive << write_enable;
				dont_initialize();
				
				SC_ACTIVITY(write_address_activity);
				sensitive << write_address;
				dont_initialize();
				
				SC_ACTIVITY(data_in_activity);
				sensitive << data_in;
				dont_initialize();
				
				SC_ACTIVITY(read_address_a_activity);
				sensitive << read_address_a;
				dont_initialize();
				
				SC_ACTIVITY(read_address_b_activity);
				sensitive << read_address_b;
				dont_initialize();
				
				SC_ACTIVITY(read_address_c_activity);
				sensitive << read_address_c;
				dont_initialize();
				
				SC_ACTIVITY(data_out_a_activity);
				sensitive << data_out_a;
				dont_initialize();
				
				SC_ACTIVITY(data_out_b_activity);
				sensitive << data_out_b;
				dont_initialize();
				
				SC_ACTIVITY(data_out_c_activity);
				sensitive << data_out_c;
				dont_initialize();
				
				SC_ACTIVITY(intercept_activity);
				sensitive << clock.pos(); //write_enable << write_address << data_in << read_address_a << read_address_b << read_address_c << data_out_a << data_out_b << data_out_c;
				dont_initialize();
			}
	};
	
	/**
	 * memory
	**/ 
	template <size_t address_width, size_t data_width>
	SC_MODULE(memory)
	{
		public:
			static constexpr size_t memory_size = 1 << address_width;

		private:
			std::array<sc_lv<data_width>, memory_size> data_;

		public:
			sc_in<bool> clock;
			sc_in<sc_logic> write_enable;
			sc_in<sc_lv<address_width>> write_address;
			sc_in<sc_lv<data_width>> data_in;
			sc_in<sc_lv<address_width>> read_address_a;
			sc_in<sc_lv<address_width>> read_address_b;
			sc_in<sc_lv<address_width>> read_address_c;
			sc_out<sc_lv<data_width>> data_out_a;
			sc_out<sc_lv<data_width>> data_out_b;
			sc_out<sc_lv<data_width>> data_out_c;

			void write()
			{
				if (clock.read() == true && write_enable.read() == SC_LOGIC_1)
				{ 
					data_[write_address.read().to_uint()] = data_in.read(); //This operation will throw std::domain_error if write_address is undefined

				}
			}

			void read()
			{
				try
				{
					if (clock.read() == true)
					{
						data_out_a.write(data_[read_address_a.read().to_uint()]);	//itt majd meg kell csinalni a castolast
						data_out_b.write(data_[read_address_b.read().to_uint()]);	//itt majd meg kell csinalni a castolast
						data_out_c.write(data_[read_address_c.read().to_uint()]);	//itt majd meg kell csinalni a castolast
					}
				}
				catch (std::domain_error const &)
				{
					data_out_a.write(sc_bv<data_width>());
				}
			}

			memory(const sc_module_name& name_)
			:
				sc_core::sc_module(name_),
				data_()
			{
				SC_HAS_PROCESS(memory);
				
				SC_METHOD(write);
				sensitive << clock.pos() << write_enable;
				dont_initialize();
				
				SC_METHOD(read);
				//sensitive << read_address_a << read_address_b << read_address_c;
				sensitive << clock.pos();	//ez minek?
				dont_initialize();
			}
			
			memory(const sc_module_name& name_, std::array<sc_lv<data_width>,memory_size> init)
			:
				sc_core::sc_module(name_),
				data_(init)
			{
				
				SC_HAS_PROCESS(memory);
				
				SC_METHOD(write);
				sensitive << clock.pos() << write_enable;
				dont_initialize();
				
				SC_METHOD(read);
				//sensitive << read_address_a << read_address_b << read_address_c;
				sensitive << clock.pos();	//ez minek?
				dont_initialize();
			}
	};
	
	
	/**
	 * alu
	**/ 
	SC_MODULE(alu_8)
	{

		enum class function {alu_add, alu_sub, alu_and, alu_or, alu_xor, alu_xxx = 7};

		sc_in<sc_lv<3>> alu_function;
		sc_in<sc_lv<8>> inputa;
		sc_in<sc_lv<8>> inputb;
		sc_out<sc_lv<8>> output;
		sc_out<sc_logic> z_out;
		sc_out<sc_logic> n_out;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					// 8 alu slices
					leakage_current[it] += ams035::xor20::leakage_current[it] * 24.0;
					leakage_current[it] += ams035::nor20::leakage_current[it] * 16.0;
					leakage_current[it] += ams035::inv0::leakage_current[it] * 32.0;
					leakage_current[it] += ams035::nand20::leakage_current[it] * 16.0;

					leakage_current[it] += ams035::nand30::leakage_current[it];
					leakage_current[it] += ams035::nand20::leakage_current[it];
					leakage_current[it] += ams035::inv0::leakage_current[it] * 4.0;
					leakage_current[it] += ams035::nor40::leakage_current[it] * 2.0;
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t alu_func_activity;
		logic::systemc::activity_t inputa_activity;
		logic::systemc::activity_t inputb_activity;
		logic::systemc::activity_t output_activity;
		logic::systemc::activity_t z_out_activity;
		logic::systemc::activity_t n_out_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t alu_func_dissipation() 
		{
			return 7.073_pJ; //7.073e-6*1e-6;
		}
		
		unit::energy_t inputa_dissipation() 
		{
			return 4.760_pJ; //4.760e-06*1e-6;
		}
		
		unit::energy_t inputb_dissipation() 
		{
			return 5.142_pJ; //5.142e-06*1e-6;
		}
		
		unit::energy_t output_dissipation() 
		{
			return 3.404_pJ; //3.404e-06*1e-6;
		}
		
		unit::energy_t z_out_dissipation() 
		{
			return 1.117_pJ; //1.117e-06*1e-6;
		}
		
		unit::energy_t n_out_dissipation() 
		{
			return 3.147_pJ; //3.147e-06*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return -1.730_pJ; //-1.730e-06*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 44_pW; //0.044e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		void operation()
		{
			
			function current_function;
			
			try
			{
				current_function = function(alu_function.read().to_uint());
			}
			catch (std::domain_error const &de)
			{
				current_function = function::alu_xxx;
			}

			unsigned long long value;
			sc_lv<8> bit_array_value;

			switch (current_function)
			{
				case function::alu_add:
					try
					{
						value = inputa.read().to_uint() + inputb.read().to_uint();
						sc_lv<8> the_result(value & ((1 << 8) - 1));
						output.write(the_result);
						z_out.write(the_result == sc_lv<8>(0) ? SC_LOGIC_1 : SC_LOGIC_0);
						n_out.write(the_result[7] == SC_LOGIC_1 ? SC_LOGIC_1 : SC_LOGIC_0);
					}
					catch (std::domain_error const &de)
					{
						output.write(sc_lv<8>('x'));
						z_out.write(SC_LOGIC_X);
						n_out.write(SC_LOGIC_X);
					}
					break;

				case function::alu_sub:
					try
					{
						value = inputa.read().to_uint() - inputb.read().to_uint();
						sc_lv<8> the_result(value & ((1 << 8) - 1));
						output.write(the_result);
						z_out.write(the_result == sc_lv<8>(0) ? SC_LOGIC_1 : SC_LOGIC_0);
						n_out.write(the_result[7] == SC_LOGIC_1 ? SC_LOGIC_1 : SC_LOGIC_0);
					}
					catch (std::domain_error const &de)
					{
						output.write(sc_lv<8>('x'));
						z_out.write(SC_LOGIC_X);
						n_out.write(SC_LOGIC_X);
					}
					break;

				case function::alu_and:
					output.write(bit_array_value = inputa.read() & inputb.read());
					z_out.write(bit_array_value == sc_lv<8>(0) ? SC_LOGIC_1 : SC_LOGIC_0);
					n_out.write(bit_array_value[7] == SC_LOGIC_1 ? SC_LOGIC_1 : SC_LOGIC_0);
					break;

				case function::alu_or:
					output.write(bit_array_value = inputa.read() | inputb.read());
					z_out.write(bit_array_value  == sc_lv<8>(0) ?  SC_LOGIC_1 : SC_LOGIC_0);
					n_out.write(bit_array_value[7] ==  SC_LOGIC_1 ? SC_LOGIC_1 : SC_LOGIC_0);
					break;

				case function::alu_xor:
					output.write(bit_array_value = inputa.read() ^ inputb.read());
					z_out.write(bit_array_value  == sc_lv<8>(0) ? SC_LOGIC_1 : SC_LOGIC_0);
					n_out.write(bit_array_value[7] ==  SC_LOGIC_1 ? SC_LOGIC_1 : SC_LOGIC_0);
					break;

				default:
					output.write(sc_lv<8>(inputb.read()));
					z_out.write(inputb.read() == sc_lv<8>(0) ? SC_LOGIC_1 : SC_LOGIC_0);
					n_out.write(inputb.read()[7] ==  SC_LOGIC_1 ? SC_LOGIC_1 : SC_LOGIC_0);

			}
		}
		
		alu_8(const sc_module_name& nm,  const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			alu_function("alu_function"),
			inputa("inputa"),
			inputb("inputb"),
			output("output"),
			z_out("z_out"),
			n_out("n_out"),
			alu_func_activity("alu_func_activity",std::bind(&alu_8::alu_func_dissipation, this)),
			inputa_activity("inputa_activity",std::bind(&alu_8::inputa_dissipation, this)),
			inputb_activity("inputb_activity",std::bind(&alu_8::inputb_dissipation, this)),
			output_activity("output_activity",std::bind(&alu_8::output_dissipation, this)),
			z_out_activity("z_out_activity",std::bind(&alu_8::z_out_dissipation, this)),
			n_out_activity("n_out_activity",std::bind(&alu_8::n_out_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&alu_8::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(126_um,169_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();
						
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &alu_function, parameter->id);
				add_activity_trace(parameter->trace_path, &inputa, parameter->id);
				add_activity_trace(parameter->trace_path, &inputb, parameter->id);
				add_activity_trace(parameter->trace_path, &output, parameter->id);
				add_activity_trace(parameter->trace_path, &z_out, parameter->id);
				add_activity_trace(parameter->trace_path, &n_out, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			
			SC_HAS_PROCESS(alu_8);

			
			SC_METHOD(operation)
			sensitive << alu_function << inputa << inputb;
			dont_initialize();
			
			SC_ACTIVITY(alu_func_activity);
			sensitive << alu_function;
			dont_initialize();
			
			SC_ACTIVITY(inputa_activity);
			sensitive << inputa;
			dont_initialize();
			
			SC_ACTIVITY(inputb_activity);
			sensitive << inputb;
			dont_initialize();
			
			SC_ACTIVITY(output_activity);
			sensitive << output;
			dont_initialize();
			
			SC_ACTIVITY(z_out_activity);
			sensitive << z_out;
			dont_initialize();
			
			SC_ACTIVITY(n_out_activity);
			sensitive << n_out;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << alu_function << inputa << inputb << output << z_out << n_out;
			dont_initialize();
		}
				
	};
	
	
	/**
	 * instruction decoder
	**/ 
	SC_MODULE(instruction_decoder)
	{
			
		enum class instruction {i_i2rf, i_m2rf, i_rf2m, i_add, i_sub, i_and, i_or, i_xor, i_jmp, i_bz, i_bn, i_eoj};

		sc_in<sc_lv<4>> opcode;
		sc_out<sc_lv<3>> alu_func;

		static bool leakage_initialized;
		static unit::current_t leakage_current[9];

		static void initialize_leakage_current()
		{
			if(leakage_initialized == false)
			{
				for(size_t it = 0; it < 9; ++it)
				{
					leakage_current[it] = 0_pA;
					leakage_current[it] += ams035::oai2110::leakage_current[it];
					leakage_current[it] += ams035::nand20::leakage_current[it];
					leakage_current[it] += ams035::nand20::leakage_current[it];
					leakage_current[it] += ams035::nor20::leakage_current[it];
					leakage_current[it] += ams035::xnr20::leakage_current[it];
					leakage_current[it] += ams035::nand30::leakage_current[it];
					leakage_current[it] += ams035::nor20::leakage_current[it];
				}
				leakage_initialized = true;
			}
		}
		
		logic::systemc::activity_t opcode_activity;
		logic::systemc::activity_t alu_func_activity;
		logic::systemc::activity_t intercept_activity;
		
		layout::sunred::component_t layout_item;
		
		unit::energy_t opcode_dissipation() 
		{
			return 0.1947_pJ; //1.947e-07*1e-6;
		}
		
		unit::energy_t alu_func_dissipation() 
		{
			return 0.393_pJ; //3.930e-07*1e-6;
		}
		
		unit::energy_t intercept_dissipation() 
		{
			return 0.0878_fJ; //8.781e-11*1e-6;
		}
		
		unit::power_t component_static_dissipation() const override 
		{
			// return 2_pW; //0.002e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
		}
		
		void operation()
		{
			try
			{
				switch(instruction(opcode.read().to_uint()))
				{
					case instruction::i_add:
						alu_func.write(sc_lv<3>(static_cast<int>(alu_8::function::alu_add)));
						break;

					case instruction::i_sub:
						alu_func.write(sc_lv<3>(static_cast<int>(alu_8::function::alu_sub)));
						break;

					case instruction::i_and:
						alu_func.write(sc_lv<3>(static_cast<int>(alu_8::function::alu_and)));
						break;

					case instruction::i_or:
						alu_func.write(sc_lv<3>(static_cast<int>(alu_8::function::alu_or)));
						break;

					case instruction::i_xor:
						alu_func.write(sc_lv<3>(static_cast<int>(alu_8::function::alu_xor)));
						break;

					default:
						alu_func.write(sc_lv<3>(static_cast<int>(alu_8::function::alu_xxx)));
				}
			}
			catch (std::domain_error const &)
			{
				alu_func.write(sc_lv<3>());
			}
		}
		
		instruction_decoder(const sc_module_name& nm,  const std::string& layer_name, const layout::xy_length_t position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
		:
			sc_core::sc_module(nm, systemc_engine),
			opcode("opcode"),
			alu_func("alu_func"),
			opcode_activity("opcode_activity",std::bind(&instruction_decoder::opcode_dissipation, this)),
			alu_func_activity("alu_func_activity",std::bind(&instruction_decoder::alu_func_dissipation, this)),
			intercept_activity("intercept_activity",std::bind(&instruction_decoder::intercept_dissipation, this)),
			layout_item(id, layer_name, position, layout::xy_length_t(35_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
		{
			initialize_leakage_current();
						
			if(parameter->add_dissipators)
			{
				adapter->manager->add_dissipator_component(id);
				adapter->manager->add_display_component(id);
			}
			
			if(parameter->trace_activity)
			{
				add_activity_trace(parameter->trace_path, &opcode, parameter->id);
				add_activity_trace(parameter->trace_path, &alu_func, parameter->id);
			}
			
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			SC_HAS_PROCESS(instruction_decoder);
			
			SC_METHOD(operation);
			sensitive << opcode;
			dont_initialize();
			
			SC_ACTIVITY(opcode_activity);
			sensitive << opcode;
			dont_initialize();
			
			SC_ACTIVITY(alu_func_activity);
			sensitive << alu_func;
			dont_initialize();
			
			SC_ACTIVITY(intercept_activity);
			sensitive << opcode << alu_func;
			dont_initialize();
		}
	};




	/**
	 * controller
	**/ 
	SC_MODULE(controller)
	{
		public:
			typedef enum state {wait_for_run, fetch_1, fetch_2, decode_1, decode_2, execute_1, execute_2, data_cache_read_access, inc_IP, deassert_all} state;
			//korabban enum class volt, de annak nincs operator << (ostream) 
			
			
			sc_in<bool> clock;
			sc_in<sc_logic> reset;
			sc_in<sc_logic> run;
			sc_in<sc_logic> ss_Z;
			sc_in<sc_logic> ss_N;
			sc_in<sc_lv<4>> ss_instruction_code;
			sc_out<sc_logic> end_of_job;
			sc_out<sc_logic> write_data_cache;
			sc_out<sc_logic> ce_IR;
			sc_out<sc_logic> ce_IP;
			sc_out<sc_logic> ce_instruction_code;
			sc_out<sc_logic> ce_destination;
			sc_out<sc_logic> ce_OP_A;
			sc_out<sc_logic> ce_OP_B;
			sc_out<sc_logic> ce_IMM;
			sc_out<sc_logic> ce_MAR;
			sc_out<sc_logic> ce_MDR;
			sc_out<sc_logic> ce_Z;
			sc_out<sc_logic> ce_N;
			sc_out<sc_logic> we_RF;
			sc_out<sc_logic> sel_IP;
			sc_out<sc_logic> sel_IP_add;
			sc_out<sc_lv<2>> sel_ALU;
			
		private:
			sc_signal<state> state_;

		public:

			static bool leakage_initialized;
			static unit::current_t leakage_current[9];

			static void initialize_leakage_current()
			{
				if(leakage_initialized == false)
				{
					for(size_t it = 0; it < 9; ++it)
					{
						leakage_current[it] = 0_pA;
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::nand40::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::aoi210::leakage_current[it];
						leakage_current[it] += ams035::oai2110::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::aoi210::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::oai2110::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::df3::leakage_current[it];
						leakage_current[it] += ams035::oai310::leakage_current[it];
						leakage_current[it] += ams035::oai310::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::aoi210::leakage_current[it];
						leakage_current[it] += ams035::oai220::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::oai220::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::oai310::leakage_current[it];
						leakage_current[it] += ams035::oai310::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::oai310::leakage_current[it];
						leakage_current[it] += ams035::nor40::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::aoi210::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor30::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::oai310::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nor30::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand30::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::oai210::leakage_current[it];
						leakage_current[it] += ams035::oai220::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::aoi220::leakage_current[it];
						leakage_current[it] += ams035::imux20::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::nand20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::nor20::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
						leakage_current[it] += ams035::inv0::leakage_current[it];
					}
					leakage_initialized = true;
				}
			}

			logic::systemc::activity_t sel_ALU_activity;
			logic::systemc::activity_t we_RF_activity;
			logic::systemc::activity_t ce_MDR_activity;
			logic::systemc::activity_t sel_IP_activity;
			logic::systemc::activity_t ss_N_activity;
			logic::systemc::activity_t run_activity;
			logic::systemc::activity_t ss_Z_activity;
			logic::systemc::activity_t reset_activity;
			logic::systemc::activity_t ss_instruction_code_activity;
			logic::systemc::activity_t write_data_cache_activity;
			logic::systemc::activity_t ce_IR_activity;
			logic::systemc::activity_t end_of_job_activity;
			logic::systemc::activity_t ce_IP_activity;
			logic::systemc::activity_t intercept_activity;
		
			layout::sunred::component_t layout_item;
			
			unit::energy_t sel_ALU_dissipation() 
			{
				return 6.292_pJ; //6.292e-06*1e-6;
			}
			
			unit::energy_t we_RF_dissipation() 
			{
				return 4.826_pJ; //4.826e-06*1e-6;
			}
			
			unit::energy_t ce_MDR_dissipation() 
			{
				return 5.217_pJ; //5.217e-06*1e-6;
			}
			
			unit::energy_t sel_IP_dissipation() 
			{
				return 4.731_pJ; //4.731e-06*1e-6;
			}
			
			unit::energy_t ss_N_dissipation() 
			{
				return 81.25_fJ; //8.125e-08*1e-6;
			}
			
			unit::energy_t run_dissipation() 
			{
				return 0.3184_pJ; //3.184e-07*1e-6;
			}
			
			unit::energy_t ss_Z_dissipation() 
			{
				return 0.1043_pJ; //1.043e-07*1e-6;
			}
			
			unit::energy_t reset_dissipation() 
			{
				return 0.9712_pJ; //9.712e-07*1e-6;
			}
			
			unit::energy_t ss_instruction_code_dissipation() 
			{
				return 0.9134_pJ; //9.134e-07*1e-6;
			}
			
			unit::energy_t write_data_cache_dissipation() 
			{
				return 5.174_pJ; //5.174e-06*1e-6;
			}
			
			unit::energy_t ce_IR_dissipation() 
			{
				return 5.999_pJ; //5.999e-06*1e-6;
			}
			
			unit::energy_t end_of_job_dissipation() 
			{
				return 2.999_pJ; //2.999e-06*1e-6;
			}
			
			unit::energy_t ce_IP_dissipation() 
			{
				return 6.575_pJ; //6.575e-06*1e-6;
			}
			
			unit::energy_t intercept_dissipation() 
			{
				return 4.630_pJ; //4.630e-06*1e-6;
			}
			
			unit::power_t component_static_dissipation() const override 
			{
				// return 68_pW; //0.068e-9;
			return ams035::vdd * ams035::interpolate(temperature, leakage_current);
			}
		
			void operation()
			{
				if (clock.read() == true)
				{
					if (reset.read() == SC_LOGIC_1)
					{
						state_ = state::wait_for_run;
						end_of_job.write(SC_LOGIC_0);
						write_data_cache.write(SC_LOGIC_0);
						ce_IR.write(SC_LOGIC_0);
						ce_IP.write(SC_LOGIC_0);
						ce_instruction_code.write(SC_LOGIC_0);
						ce_destination.write(SC_LOGIC_0);
						ce_OP_A.write(SC_LOGIC_0);
						ce_OP_B.write(SC_LOGIC_0);
						ce_IMM.write(SC_LOGIC_0);
						ce_MAR.write(SC_LOGIC_0);
						ce_MDR.write(SC_LOGIC_0);
						ce_Z.write(SC_LOGIC_0);
						ce_N.write(SC_LOGIC_0);
						we_RF.write(SC_LOGIC_0);
						sel_IP.write(SC_LOGIC_0);
						sel_IP_add.write(SC_LOGIC_0);
						sel_ALU.write(sc_lv<2>("00"));
					}
					else
					{
						switch(state_)
						{
							case state::wait_for_run:
								if (run.read() == SC_LOGIC_1)
								{
									end_of_job.write(SC_LOGIC_0);
									sel_IP.write(SC_LOGIC_0);
									ce_IP.write(SC_LOGIC_1);
									state_ = state::fetch_1;
								}
								else state_ = state::wait_for_run;
								break;

							case state::fetch_1:
								ce_IP.write(SC_LOGIC_0);
								state_ = state::fetch_2;
								break;

							case state::fetch_2:
								ce_IR.write(SC_LOGIC_1);
								state_ = state::decode_1;
								break;

							case state::decode_1:
								ce_IR.write(SC_LOGIC_0);
								state_ = state::decode_2;
								break;

							case state::decode_2:
								ce_instruction_code.write(SC_LOGIC_1);
								ce_destination.write(SC_LOGIC_1);
								ce_OP_A.write(SC_LOGIC_1);
								ce_OP_B.write(SC_LOGIC_1);
								ce_IMM.write(SC_LOGIC_1);
								ce_MAR.write(SC_LOGIC_1);
								ce_MDR.write(SC_LOGIC_1);

								state_ = state::execute_1;
								break;

							case state::execute_1:
								ce_instruction_code.write(SC_LOGIC_0);
								ce_destination.write(SC_LOGIC_0);
								ce_OP_A.write(SC_LOGIC_0);
								ce_OP_B.write(SC_LOGIC_0);
								ce_IMM.write(SC_LOGIC_0);
								ce_MAR.write(SC_LOGIC_0);
								ce_MDR.write(SC_LOGIC_0);

								state_ = state::execute_2;
								break;

							case state::execute_2:
								state_ = state::inc_IP;

								if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_i2rf)))
								{
									sel_ALU.write(sc_lv<2>("00"));
									we_RF.write(SC_LOGIC_1);
									ce_Z.write(SC_LOGIC_1);
									ce_N.write(SC_LOGIC_1);
								} 
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_m2rf)))
								{
									state_ = state::data_cache_read_access;
								}
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_rf2m)))
								{
									write_data_cache.write(SC_LOGIC_1);
								}
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_add)) ||
										ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_sub)) ||
										ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_and)) ||
										ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_or)) ||
										ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_xor))
										)
								{
									sel_ALU.write(sc_lv<2>("10"));
									we_RF.write(SC_LOGIC_1);
									ce_Z.write(SC_LOGIC_1);
									ce_N.write(SC_LOGIC_1);
								}
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_jmp)))
								{
									sel_IP.write(SC_LOGIC_1);
									sel_IP_add.write(SC_LOGIC_1);
									ce_IP.write(SC_LOGIC_1);
									state_ = state::deassert_all;
								}
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_bz)))
								{
									if (ss_Z.read() == SC_LOGIC_1)
									{
										sel_IP_add.write(SC_LOGIC_1);
									}
									else
									{
										sel_IP_add.write(SC_LOGIC_0);
									}
									sel_IP.write(SC_LOGIC_1);
									ce_IP.write(SC_LOGIC_1);
									state_ = state::deassert_all;
								}
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_bn)))
								{
									if (ss_N.read() == SC_LOGIC_1)
									{
										sel_IP_add.write(SC_LOGIC_1);
									}
									else
									{
										sel_IP_add.write(SC_LOGIC_0);
									}
									sel_IP.write(SC_LOGIC_1);
									ce_IP.write(SC_LOGIC_1);
									state_ = state::deassert_all;
								}
								else if (ss_instruction_code.read() == sc_lv<4>(static_cast<int>(instruction_decoder::instruction::i_eoj)))
								{
									end_of_job.write(SC_LOGIC_1);
									state_ = state::wait_for_run;
								}
								break;

							case state::data_cache_read_access:
								sel_ALU.write(sc_lv<2>("01"));
								we_RF.write(SC_LOGIC_1);
								ce_Z.write(SC_LOGIC_1);
								ce_N.write(SC_LOGIC_1);
								state_ = state::inc_IP;
								break;

							case state::inc_IP:
								sel_IP.write(SC_LOGIC_1);
								sel_IP_add.write(SC_LOGIC_0);
								ce_IP.write(SC_LOGIC_1);
								state_ = state::deassert_all;
								break;

							case state::deassert_all:
								ce_IR.write(SC_LOGIC_0);
								ce_IP.write(SC_LOGIC_0);
								ce_instruction_code.write(SC_LOGIC_0);
								ce_destination.write(SC_LOGIC_0);
								ce_OP_A.write(SC_LOGIC_0);
								ce_OP_B.write(SC_LOGIC_0);
								ce_IMM.write(SC_LOGIC_0);
								ce_MAR.write(SC_LOGIC_0);
								ce_MDR.write(SC_LOGIC_0);
								ce_Z.write(SC_LOGIC_0);
								ce_N.write(SC_LOGIC_0);
								we_RF.write(SC_LOGIC_0);
								write_data_cache.write(SC_LOGIC_0);
								state_ = state::fetch_2;
							default:
								break;
						}
					}
				}
			}
			
			controller(const sc_module_name& nm,  const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* systemc_engine)
			:
				sc_core::sc_module(nm, systemc_engine),
				clock("clock"),
				reset("reset"),
				run("run"),
				ss_Z("ss_Z"),
				ss_N("ss_N"),
				ss_instruction_code("ss_instruction_code"),
				end_of_job("end_of_job"),
				write_data_cache("write_data_cache"),
				ce_IR("ce_IR"),
				ce_IP("ce_IP"),
				ce_instruction_code("ce_instruction_code"),
				ce_destination("ce_destination"),
				ce_OP_A("ce_OP_A"),
				ce_OP_B("ce_OP_B"), 
				ce_IMM("ce_IMM"),
				ce_MAR("ce_MAR"),
				ce_MDR("ce_MDR"),
				ce_Z("ce_Z"),
				ce_N("ce_N"),
				we_RF("we_RF"),
				sel_IP("sel_IP"),
				sel_IP_add("sel_IP_add"),
				sel_ALU("sel_ALU"),
				sel_ALU_activity("sel_ALU_activity",std::bind(&controller::sel_ALU_dissipation, this)),
				we_RF_activity("we_RF_activity",std::bind(&controller::we_RF_dissipation, this)),
				ce_MDR_activity("ce_MDR_activity",std::bind(&controller::ce_MDR_dissipation, this)),
				sel_IP_activity("sel_IP_activity",std::bind(&controller::sel_IP_dissipation, this)),
				ss_N_activity("ss_N_activity",std::bind(&controller::ss_N_dissipation, this)),
				run_activity("run_activity",std::bind(&controller::run_dissipation, this)),
				ss_Z_activity("ss_Z_activity",std::bind(&controller::ss_Z_dissipation, this)),
				reset_activity("reset_activity",std::bind(&controller::reset_dissipation, this)),
				ss_instruction_code_activity("ss_instruction_code_activity",std::bind(&controller::ss_instruction_code_dissipation, this)),
				write_data_cache_activity("write_data_cache_activity",std::bind(&controller::write_data_cache_dissipation, this)),
				ce_IR_activity("ce_IR_activity",std::bind(&controller::ce_IR_dissipation, this)),
				end_of_job_activity("end_of_job_activity",std::bind(&controller::end_of_job_dissipation, this)),
				ce_IP_activity("ce_IP_activity",std::bind(&controller::ce_IP_dissipation, this)),
				intercept_activity("intercept_activity",std::bind(&controller::intercept_dissipation, this)),
				layout_item(id, layer_name, position, layout::xy_length_t(140_um,143_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
			{
				initialize_leakage_current();

				if(parameter->add_dissipators)
				{
					adapter->manager->add_dissipator_component(id);
					adapter->manager->add_display_component(id);
				}
								
				if(parameter->trace_activity)
				{
					add_activity_trace(parameter->trace_path, &reset, parameter->id);
					add_activity_trace(parameter->trace_path, &run, parameter->id);
					add_activity_trace(parameter->trace_path, &ss_Z, parameter->id);
					add_activity_trace(parameter->trace_path, &ss_N, parameter->id);
					add_activity_trace(parameter->trace_path, &ss_instruction_code, parameter->id);
					add_activity_trace(parameter->trace_path, &end_of_job, parameter->id);
					add_activity_trace(parameter->trace_path, &write_data_cache, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_IR, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_IP, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_instruction_code, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_destination, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_OP_A, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_OP_B, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_IMM, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_MAR, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_MDR, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_Z, parameter->id);
					add_activity_trace(parameter->trace_path, &ce_N, parameter->id);
					add_activity_trace(parameter->trace_path, &we_RF, parameter->id);
					add_activity_trace(parameter->trace_path, &sel_IP_add, parameter->id);
					add_activity_trace(parameter->trace_path, &sel_ALU, parameter->id);
				}
				
				if(parameter->trace_dissipation)
				{
					add_static_dissipation_trace(parameter->trace_path, parameter->id);
					add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
				}
				
				if(parameter->trace_temperature)
				{
					layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
					layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
					layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
				}
								
				SC_HAS_PROCESS(controller);

				SC_METHOD(operation);
				sensitive << clock.pos();
				dont_initialize();
				

				SC_ACTIVITY(sel_ALU_activity);
				sensitive << sel_ALU;
				dont_initialize();
				
				SC_ACTIVITY(we_RF_activity);
				sensitive << we_RF;
				dont_initialize();
				
				SC_ACTIVITY(ce_MDR_activity);
				sensitive << ce_MDR;
				dont_initialize();
				
				SC_ACTIVITY(sel_IP_activity);
				sensitive << sel_IP;
				dont_initialize();
				
				SC_ACTIVITY(ss_N_activity);
				sensitive << ss_N;
				dont_initialize();
				
				SC_ACTIVITY(run_activity);
				sensitive << run;
				dont_initialize();
				
				SC_ACTIVITY(ss_Z_activity);
				sensitive << ss_Z;
				dont_initialize();
				
				SC_ACTIVITY(reset_activity);
				sensitive << reset;
				dont_initialize();
				
				SC_ACTIVITY(ss_instruction_code_activity);
				sensitive << ss_instruction_code;
				dont_initialize();
				
				SC_ACTIVITY(write_data_cache_activity);
				sensitive << write_data_cache;
				dont_initialize();
				
				SC_ACTIVITY(ce_IR_activity);
				sensitive << ce_IR;
				dont_initialize();
				
				SC_ACTIVITY(end_of_job_activity);
				sensitive << end_of_job;
				dont_initialize();
				
				SC_ACTIVITY(ce_IP_activity);
				sensitive << ce_IP;
				dont_initialize();
				
				SC_ACTIVITY(intercept_activity);
				sensitive << clock.pos(); //<< sel_ALU << we_RF << ce_MDR << sel_IP << ss_N << run << ss_Z << reset << ss_instruction_code << write_data_cache << ce_IR << end_of_job << ce_IP;
				dont_initialize();
				
				state_ = state::wait_for_run;
			}
	};
	
	SC_MODULE(demo_core)
	{
		sc_in<bool> clk;
		sc_in<sc_logic> rst;
		sc_in<sc_logic> run;
		sc_out<sc_logic> end_of_job;
		
		
		
		//CTRL-bol kiindulo jelek
		sc_signal<sc_logic> ce_IR_from_CTRL;
		sc_signal<sc_logic> ce_IP_from_CTRL;
		sc_signal<sc_logic> ce_destination_from_CTRL;
		sc_signal<sc_logic> ce_instruction_code_from_CTRL;
		sc_signal<sc_logic> ce_IMM_from_CTRL;
		sc_signal<sc_logic> ce_OP_A_from_CTRL;
		sc_signal<sc_logic> ce_OP_B_from_CTRL;
		sc_signal<sc_logic> ce_MAR_from_CTRL;
		sc_signal<sc_logic> ce_MDR_from_CTRL;
		sc_signal<sc_logic> ce_Z_from_CTRL;
		sc_signal<sc_logic> ce_N_from_CTRL;
		sc_signal<sc_lv<2>> sel_ALU_from_CTRL;
		sc_signal<sc_logic> sel_IP_from_CTRL;
		sc_signal<sc_logic> sel_IP_ADD_from_CTRL;
		sc_signal<sc_logic> write_data_cache_from_CTRL;
		sc_signal<sc_logic> we_RF_from_CTRL;
		sc_signal<sc_logic> eoj_core0;

		//IR-bol kiindulo jel
		sc_signal<sc_lv<16>> dout_from_IR;
		
		//RS_IR_7_4-bol kiindulo jel
		sc_signal<sc_lv<4>> out_from_RS_IR_7_4;
		
		//RS_IR_7_0-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_RS_IR_7_0;
		
		//RS_IR_3_0-bol kiindulo jel
		sc_signal<sc_lv<4>> out_from_RS_IR_3_0;
		
		//RS_IR_11_8-bol kiindulo jel
		sc_signal<sc_lv<4>>	out_from_RS_IR_11_8;
		
		//RS_IR_15_12-bol kiindulo jel
		sc_signal<sc_lv<4>> out_from_RS_IR_15_12;
		
		//RF-bol kiindulo jelek
		sc_signal<sc_lv<8>> data_out_a_from_RF;
		sc_signal<sc_lv<8>> data_out_b_from_RF;
		sc_signal<sc_lv<8>> data_out_c_from_RF;
		
		//ADDR_ADD-bol kiindulo jel
		sc_signal<sc_lv<8>> result_from_ADDR_ADD;
		
		//DATA_CACHE-bol kiindulo jel
		sc_signal<sc_lv<8>> data_out_a_from_DATA_CACHE;
		//DATA_CACHE dummy jelek
		sc_signal<sc_lv<8>> data_out_b_from_DATA_CACHE;
		sc_signal<sc_lv<8>> data_out_c_from_DATA_CACHE;
		sc_signal<sc_lv<8>>	address_b_to_DATA_CACHE;
		sc_signal<sc_lv<8>>	address_c_to_DATA_CACHE;
		
		//OP_B-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_OP_B;
		
		//MUX_ALU-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_MUX_ALU;
		
		//MUX_ALU dummy jel
		sc_signal<sc_lv<8>> input4_to_MUX_ALU;
		
		//OP_A-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_OP_A;
		
		
		//DESTINATION-bol kiindulo jel
		sc_signal<sc_lv<4>> dout_from_DESTINATION;
		
		//ALU-bol kiindulo jelek
		sc_signal<sc_lv<8>> result_from_ALU;
		sc_signal<sc_logic> z_out_from_ALU;
		sc_signal<sc_logic> n_out_from_ALU;
		
		//Z-bol kiindulo jel
		sc_signal<sc_logic> dout_from_Z;
		
		//N-bol kiindulo jel
		sc_signal<sc_logic> dout_from_N;
		
		//INSTRUCTION_CODE-bol kiindulo jel
		sc_signal<sc_lv<4>> dout_from_INSTRUCTION_CODE;
		
		//INSTRUCTION_DECODER-bol kiindulo jel
		sc_signal<sc_lv<3>> alu_func_from_INSTRUCTION_DECODER;
		
		//MAR-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_MAR;
		
		//MDR-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_MDR;

		//INSTRUCTION_CACHE-bol kiindulo jel
		sc_signal<sc_lv<16>> data_out_a_from_INSTRUCTION_CACHE;
		//INSTRUCTION_CACHE dummy jelek
		sc_signal<sc_lv<16>>	data_out_b_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<16>>	data_out_c_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<8>>		address_b_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<8>>		address_c_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<16>>	data_in_to_INSTRUCTION_CACHE;
		sc_signal<sc_logic>		write_enable_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<8>>		write_address_to_INSTRUCTION_CACHE;
		
		//IMM-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_IMM;
		//MUX_IP_ADD-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_MUX_IP_ADD;
		
		//MUX_IP_ADD dummy jel
		sc_signal<sc_lv<8>> input0_to_MUX_IP_ADD;
		
		
		
		//IP-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_IP;
		
		//IP_ADD-bol kiindulo jel
		sc_signal<sc_lv<8>> result_from_IP_ADD;
		
		//MUX_IP-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_MUX_IP;
		//MUX_IP dummy jel
		sc_signal<sc_lv<8>> input0_to_MUX_IP;
		
		layout::sunred::component_t layout_item;
		
		controller 					cntrl;
		register_16 				ir;
		mux2_8 						mux_ip;
		register_8 					ip;
		register_4 					inst_code;
		register_4 					dest;
		register_8 					op_a;
		register_8 					op_b;
		register_8 					imm;
		register_8 					mar;
		register_8 					mdr;
		register_1 					z;
		register_1 					n;
		regfile 					rf;
		mux2_8 						mux_ip_add;
		adder_8 					ip_add;
		adder_8 					addr_add;
		instruction_decoder 		inst_dec;
		mux4_8 						mux_alu;
		alu_8 						alu;
		memory<8,16>				inst_cache;	
		memory<8,8>					data_cache;	
		range_selector<16,3,0> 		rs_ir_3_0;	
		range_selector<16,7,4> 		rs_ir_7_4;	
		range_selector<16,11,8> 	rs_ir_11_8;	
		range_selector<16,7,0> 		rs_ir_7_0;	
		range_selector<16,15,12> 	rs_ir_15_12;
		
		demo_core(	const sc_module_name& nm,
					const std::array<sc_lv<16>,256>& program_initializer,
					const std::array<sc_lv<8>,256>& data_initializer,
					const std::string& layer_name,
					const layout::xy_length_t& position,
					logic::systemc::adapter_t* systemc_engine,
					layout::sunred::adapter_t* layout_adapter)
		:
			sc_core::sc_module(nm, systemc_engine),
			layout_item(id, layer_name, position, layout::xy_length_t(525_um,559_um), nullptr, layout_adapter),
			cntrl("cntrl", layer_name, layout::xy_length_t(385_um,0_um), &layout_item, systemc_engine),
			ir("ir", layer_name, layout::xy_length_t(385_um,156_um), &layout_item, systemc_engine),
			ip("ip", layer_name, layout::xy_length_t(0_um,416_um), &layout_item, systemc_engine),
			mux_ip("mux_ip", layer_name, layout::xy_length_t(84_um,416_um), &layout_item, systemc_engine),
			ip_add("ip_add", layer_name, layout::xy_length_t(112_um,416_um), &layout_item, systemc_engine),
			mux_ip_add("mux_ip_add", layer_name, layout::xy_length_t(168_um,416_um), &layout_item, systemc_engine),
			inst_code("inst_code", layer_name, layout::xy_length_t(290.5_um,416_um), &layout_item, systemc_engine),
			dest("dest", layer_name, layout::xy_length_t(248.5_um,416_um), &layout_item, systemc_engine),
			op_a("op_a", layer_name, layout::xy_length_t(0_um,494_um), &layout_item, systemc_engine),
			op_b("op_b", layer_name, layout::xy_length_t(84_um,494_um), &layout_item, systemc_engine),
			imm("imm", layer_name, layout::xy_length_t(168_um,494_um), &layout_item, systemc_engine),
			mar("mar", layer_name, layout::xy_length_t(252_um,494_um), &layout_item, systemc_engine),
			mdr("mdr", layer_name, layout::xy_length_t(336_um,494_um), &layout_item, systemc_engine),
			rf("rf", layer_name, layout::xy_length_t(0_um,0_um), &layout_item, systemc_engine),
			addr_add("addr_add", layer_name, layout::xy_length_t(420_um,494_um), &layout_item, systemc_engine),
			inst_dec("inst_dec", layer_name, layout::xy_length_t(203_um,416_um), &layout_item, systemc_engine),
			mux_alu("mux_alu", layer_name, layout::xy_length_t(332.5_um,416_um), &layout_item, systemc_engine),
			alu("alu", layer_name, layout::xy_length_t(385_um,312_um), &layout_item, systemc_engine),
			z("z", layer_name, layout::xy_length_t(462_um,468_um), &layout_item, systemc_engine),
			n("n", layer_name, layout::xy_length_t(493.5_um,468_um), &layout_item, systemc_engine),
			inst_cache("inst_cache", program_initializer),
			data_cache("data_cache", data_initializer),
			rs_ir_3_0("rs_ir_3_0"),
			rs_ir_7_4("rs_ir_7_4"),
			rs_ir_11_8("rs_ir_11_8"),
			rs_ir_7_0("rs_ir_7_0"),
			rs_ir_15_12("rs_ir_15_12")
			
		{
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			//CTRL
			cntrl.ce_IR(ce_IR_from_CTRL);
			cntrl.ce_IP(ce_IP_from_CTRL);
			cntrl.ce_destination(ce_destination_from_CTRL);
			cntrl.ce_instruction_code(ce_instruction_code_from_CTRL);
			cntrl.ce_IMM(ce_IMM_from_CTRL);
			cntrl.ce_OP_A(ce_OP_A_from_CTRL);
			cntrl.ce_OP_B(ce_OP_B_from_CTRL);
			cntrl.ce_MAR(ce_MAR_from_CTRL);
			cntrl.ce_MDR(ce_MDR_from_CTRL);
			cntrl.ce_Z(ce_Z_from_CTRL);
			cntrl.ce_N(ce_N_from_CTRL);
			cntrl.sel_ALU(sel_ALU_from_CTRL);
			cntrl.sel_IP(sel_IP_from_CTRL);
			cntrl.sel_IP_add(sel_IP_ADD_from_CTRL);
			cntrl.write_data_cache(write_data_cache_from_CTRL);
			cntrl.we_RF(we_RF_from_CTRL);
			cntrl.ss_Z(dout_from_Z);
			cntrl.ss_N(dout_from_N);
			cntrl.ss_instruction_code(dout_from_INSTRUCTION_CODE);
			cntrl.clock(clk);
			cntrl.reset(rst);
			cntrl.run(run);
			cntrl.end_of_job(end_of_job);
			
			//std::cout << "bekotes utan" << std::endl;
			//cntrl.add_trace();
			
			
			//IR
			ir.input(data_out_a_from_INSTRUCTION_CACHE);
			ir.enable(ce_IR_from_CTRL);
			ir.output(dout_from_IR);
			ir.clock(clk);
			ir.reset(rst);
			
			//RS_IR_3_0
			rs_ir_3_0.in(dout_from_IR);
			rs_ir_3_0.out(out_from_RS_IR_3_0);
			
			//RS_IR_7_4
			rs_ir_7_4.in(dout_from_IR);
			rs_ir_7_4.out(out_from_RS_IR_7_4);
			
			//RS_IR_7_0
			rs_ir_7_0.in(dout_from_IR);
			rs_ir_7_0.out(out_from_RS_IR_7_0);
			
			//RS_IR_11_8
			rs_ir_11_8.in(dout_from_IR);
			rs_ir_11_8.out(out_from_RS_IR_11_8);
			
			//RS_IR_15_12
			rs_ir_15_12.in(dout_from_IR);
			rs_ir_15_12.out(out_from_RS_IR_15_12);
			
			//RF
			rf.read_address_a(out_from_RS_IR_7_4);
			rf.read_address_b(out_from_RS_IR_3_0);
			rf.read_address_c(out_from_RS_IR_11_8);
			rf.write_enable(we_RF_from_CTRL);
			rf.write_address(dout_from_DESTINATION);
			rf.data_in(result_from_ALU);
			rf.data_out_a(data_out_a_from_RF);
			rf.data_out_b(data_out_b_from_RF);
			rf.data_out_c(data_out_c_from_RF);
			rf.clock(clk);
			
			//ADDR_ADD
			addr_add.input0(data_out_a_from_RF);
			addr_add.input1(data_out_b_from_RF);
			addr_add.result(result_from_ADDR_ADD);
			
			
			//DATA_CACHE
			data_cache.data_in(dout_from_MDR);
			data_cache.write_address(dout_from_MAR);
			data_cache.write_enable(write_data_cache_from_CTRL);
			data_cache.read_address_a(dout_from_MAR);
			data_cache.data_out_a(data_out_a_from_DATA_CACHE);
			data_cache.clock(clk);
			
			//DATA_CACHE dummy jelek
			data_cache.read_address_b(address_b_to_DATA_CACHE);
			data_cache.read_address_c(address_c_to_DATA_CACHE);
			data_cache.data_out_b(data_out_b_from_DATA_CACHE);
			data_cache.data_out_c(data_out_c_from_DATA_CACHE);
			
			//OP_B
			op_b.input(data_out_b_from_RF);
			op_b.enable(ce_OP_B_from_CTRL);
			op_b.output(dout_from_OP_B);
			op_b.clock(clk);
			op_b.reset(rst);
			
			//MUX_ALU
			mux_alu.input0(dout_from_IMM);
			mux_alu.input1(data_out_a_from_DATA_CACHE);
			mux_alu.input2(dout_from_OP_B);
			mux_alu.input3(input4_to_MUX_ALU);
			mux_alu.select(sel_ALU_from_CTRL);
			mux_alu.output(out_from_MUX_ALU);
			
			//OP_A
			op_a.input(data_out_a_from_RF);
			op_a.enable(ce_OP_A_from_CTRL);
			op_a.output(dout_from_OP_A);
			op_a.clock(clk);
			op_a.reset(rst);
			
			//DESTINATION
			dest.input(out_from_RS_IR_11_8);
			dest.enable(ce_destination_from_CTRL);
			dest.output(dout_from_DESTINATION);
			dest.clock(clk);
			dest.reset(rst);
			
			//ALU
			alu.alu_function(alu_func_from_INSTRUCTION_DECODER);
			alu.inputa(dout_from_OP_A);
			alu.inputb(out_from_MUX_ALU);
			alu.output(result_from_ALU);
			alu.z_out(z_out_from_ALU);
			alu.n_out(n_out_from_ALU);
			
			//Z
			z.input(z_out_from_ALU);
			z.enable(ce_Z_from_CTRL);
			z.output(dout_from_Z);
			z.clock(clk);
			z.reset(rst);
			
			//N
			n.input(n_out_from_ALU);
			n.enable(ce_N_from_CTRL);
			n.output(dout_from_N);
			n.clock(clk);
			n.reset(rst);
			
			//INSTRUCTION_CODE
			inst_code.input(out_from_RS_IR_15_12);
			inst_code.enable(ce_instruction_code_from_CTRL);
			inst_code.output(dout_from_INSTRUCTION_CODE);
			inst_code.clock(clk);
			inst_code.reset(rst);
			
			//INSTRUCTION_DECODER
			inst_dec.opcode(dout_from_INSTRUCTION_CODE);
			inst_dec.alu_func(alu_func_from_INSTRUCTION_DECODER);
			
			//MAR
			mar.input(result_from_ADDR_ADD);
			mar.enable(ce_MAR_from_CTRL);
			mar.output(dout_from_MAR);
			mar.clock(clk);
			mar.reset(rst);
			
			//MDR
			mdr.input(data_out_c_from_RF);
			mdr.enable(ce_MDR_from_CTRL);
			mdr.output(dout_from_MDR);
			mdr.clock(clk);
			mdr.reset(rst);
			
			//INSTRUCTION_CACHE
			inst_cache.read_address_a(dout_from_IP);
			inst_cache.data_out_a(data_out_a_from_INSTRUCTION_CACHE);
			inst_cache.clock(clk);
			
			//INSTRUCTION_CACHE dummy jelek
			inst_cache.read_address_b(address_b_to_INSTRUCTION_CACHE);
			inst_cache.read_address_c(address_c_to_INSTRUCTION_CACHE);
			inst_cache.data_out_b(data_out_b_to_INSTRUCTION_CACHE);
			inst_cache.data_out_c(data_out_c_to_INSTRUCTION_CACHE);
			inst_cache.data_in(data_in_to_INSTRUCTION_CACHE);
			inst_cache.write_address(write_address_to_INSTRUCTION_CACHE);
			inst_cache.write_enable(write_enable_to_INSTRUCTION_CACHE);
			
			//IMM
			imm.input(out_from_RS_IR_7_0);
			imm.enable(ce_IMM_from_CTRL);
			imm.output(dout_from_IMM);
			imm.clock(clk);
			imm.reset(rst);
			
			//MUX_IP_ADD
			mux_ip_add.input0(input0_to_MUX_IP_ADD);
			mux_ip_add.input1(dout_from_IMM);
			mux_ip_add.select(sel_IP_ADD_from_CTRL);
			mux_ip_add.output(out_from_MUX_IP_ADD);
			
			//IP
			ip.input(out_from_MUX_IP);
			ip.enable(ce_IP_from_CTRL);
			ip.output(dout_from_IP);
			ip.clock(clk);
			ip.reset(rst);
			
			//IP_ADD
			ip_add.input0(out_from_MUX_IP_ADD);
			ip_add.input1(dout_from_IP);
			ip_add.result(result_from_IP_ADD);
			
			//MUX_IP
			mux_ip.input0(input0_to_MUX_IP);
			mux_ip.input1(result_from_IP_ADD);
			mux_ip.select(sel_IP_from_CTRL);
			mux_ip.output(out_from_MUX_IP);
			
			
			
			input0_to_MUX_IP_ADD = "00000001",
			input0_to_MUX_IP = "00000000";
		}
	};

}	//namespace demoproc_struct

#endif //_DEMOPROC_STRUCTURAL_
