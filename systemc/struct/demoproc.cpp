//#include <systemc.h>
//Hahh!!
#include <logitherm>

#include <algorithm>
#include <fstream>

#include "demoproc.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_struct
{
	bool register_1::leakage_initialized = false;
	unit::current_t register_1::leakage_current[9];

	bool register_4::leakage_initialized = false;
	unit::current_t register_4::leakage_current[9];

	bool register_8::leakage_initialized = false;
	unit::current_t register_8::leakage_current[9];

	bool register_16::leakage_initialized = false;
	unit::current_t register_16::leakage_current[9];

	bool adder_8::leakage_initialized = false;
	unit::current_t adder_8::leakage_current[9];

	bool mux2_8::leakage_initialized = false;
	unit::current_t mux2_8::leakage_current[9];

	bool mux4_8::leakage_initialized = false;
	unit::current_t mux4_8::leakage_current[9];

	bool regfile::leakage_initialized = false;
	unit::current_t regfile::leakage_current[9];

	bool alu_8::leakage_initialized = false;
	unit::current_t alu_8::leakage_current[9];

	bool instruction_decoder::leakage_initialized = false;
	unit::current_t instruction_decoder::leakage_current[9];

	bool controller::leakage_initialized = false;
	unit::current_t controller::leakage_current[9];
}

int sc_main(int argc, char *argv[])
{
	try
	{
		sc_set_time_resolution(1, SC_FS); //ezt miert ide kell tenni?
		sc_report_handler::set_actions (SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
		sc_report_handler::set_actions (SC_ID_LOGIC_X_TO_BOOL_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
		
		std::unordered_map<std::string, simulation_parameters> parameters;
		// simulation_parameters default_parameters("default");

		simulation_parameters locked_100MHz_parameters("locked_100MHz");
		locked_100MHz_parameters.clock_frequency = 100_MHz;
		locked_100MHz_parameters.clock_period = 1.0/locked_100MHz_parameters.clock_frequency;
		locked_100MHz_parameters.timestep = locked_100MHz_parameters.clock_period;
		locked_100MHz_parameters.simulation_time = 2_ms;
		locked_100MHz_parameters.trace_temperature = false;
		locked_100MHz_parameters.trace_critical_path = false;
		locked_100MHz_parameters.trace_layer_temperature = false;
		locked_100MHz_parameters.zero_delay = true;
		locked_100MHz_parameters.add_dissipators = false;

		simulation_parameters locked_1MHz_parameters("locked_1MHz");
		locked_1MHz_parameters.clock_frequency = 1_MHz;
		locked_1MHz_parameters.clock_period = 1.0/locked_1MHz_parameters.clock_frequency;
		locked_1MHz_parameters.timestep = locked_1MHz_parameters.clock_period;
		locked_1MHz_parameters.simulation_time = 2_ms;
		locked_1MHz_parameters.trace_temperature = false;
		locked_1MHz_parameters.trace_critical_path = false;
		locked_1MHz_parameters.trace_layer_temperature = false;
		locked_1MHz_parameters.zero_delay = true;
		locked_1MHz_parameters.add_dissipators = false;
		
		// functional only
		simulation_parameters functional_only_parameters("functional_only");
		functional_only_parameters.add_dissipators = false;
		functional_only_parameters.trace_dissipation = false;
		functional_only_parameters.trace_temperature = false;
		functional_only_parameters.trace_critical_path = false;
		functional_only_parameters.trace_layer_temperature = false;
		functional_only_parameters.zero_delay = true;

		// bulk die
		simulation_parameters bulk_die_parameters("bulk_die");
		bulk_die_parameters.add_dissipators = true;
		bulk_die_parameters.trace_dissipation = false;
		bulk_die_parameters.trace_temperature = false;
		bulk_die_parameters.trace_critical_path = false;
		bulk_die_parameters.trace_layer_temperature = false;
		bulk_die_parameters.zero_delay = false;
		bulk_die_parameters.number_of_bulk_layers = 10;

		// soi die
		simulation_parameters soi_die_parameters("soi_die");
		soi_die_parameters.add_dissipators = true;
		soi_die_parameters.soi_die = true;
		soi_die_parameters.trace_dissipation = false;
		soi_die_parameters.trace_temperature = false;
		soi_die_parameters.trace_critical_path = false;
		soi_die_parameters.trace_layer_temperature = false;
		soi_die_parameters.zero_delay = false;
		soi_die_parameters.number_of_bulk_layers = 10;

		parameters.insert(std::make_pair(locked_100MHz_parameters.id, locked_100MHz_parameters));
		parameters.insert(std::make_pair(locked_1MHz_parameters.id, locked_1MHz_parameters));
		parameters.insert(std::make_pair(functional_only_parameters.id, functional_only_parameters));
		parameters.insert(std::make_pair(bulk_die_parameters.id, bulk_die_parameters));
		parameters.insert(std::make_pair(soi_die_parameters.id, soi_die_parameters));

		for(auto &it: parameters)
		{
			parameter = &it.second;

			std::ofstream info_log("./output/info_" + parameter->id + ".log");
			info_log << "nodes, " << "walltime, " << "cputime" << std::endl;

			auto* manager = logitherm::manager_t::get_manager();
			manager->set_log_file_path("./output");
			
			auto* systemc_engine = dynamic_cast<logic::systemc::adapter_t*>(manager->set_logic_engine(util::logic_engine_t::systemc));
			if(nullptr == systemc_engine) throw("systemc_engine is nullptr");

			auto* sunred_engine = dynamic_cast<thermal::sunred::adapter_t*>(manager->set_thermal_engine(util::thermal_engine_t::sunred));
			if(nullptr == sunred_engine) throw("sunred_engine is nullptr");
			manager->set_timestep(parameter->timestep);
			
			sunred_engine->create_layout(parameter->layout_size, parameter->layout_resolution);
			auto* sunred_layout = dynamic_cast<layout::sunred::adapter_t*>(sunred_engine->get_layout());
			if(nullptr == sunred_layout) throw("sunred_layout is nullptr");

			sunred_engine->add_material(std::get<0>(parameter->die_material), std::get<1>(parameter->die_material), std::get<2>(parameter->die_material));
			sunred_engine->add_layer(parameter->dissipating_layer, std::get<0>(parameter->die_material), parameter->dissipating_layer_thickness);

			if(parameter->soi_die == true)
			{
				sunred_engine->add_material(std::get<0>(parameter->insulator_material), std::get<1>(parameter->insulator_material), std::get<2>(parameter->insulator_material));
				sunred_engine->add_layer(parameter->insulating_layer, std::get<0>(parameter->insulator_material), parameter->insulating_layer_thickness);
			}

			for(size_t it = 0; it < parameter->number_of_bulk_layers; ++it)
			{
				sunred_engine->add_layer(parameter->bulk_layer + "_" + std::to_string(it), std::get<0>(parameter->die_material), parameter->bulk_layer_thickness/parameter->number_of_bulk_layers);
			}
			
			auto* dissipating_layer_observer = new layout::sunred::component_t("dissipating_layer_observer", parameter->dissipating_layer, layout::xy_length_t(0_um,0_um), parameter->layout_size, nullptr, sunred_layout);
			if(parameter->trace_layer_temperature)
			{
				dissipating_layer_observer->add_layer_temperature_trace(parameter->trace_path, parameter->id); //at kellene nevezni???
			}
			
			sunred_engine->add_boundary_condition(thermal::sunred::side_t::bottom, thermal::sunred::boundary_t::conductive, parameter->boundary_condition_value);

			//elso parameter a periodus ido
			sc_clock clk("clk", static_cast<double>(parameter->clock_period), SC_SEC, 0.5, 5, SC_NS, false);
			sc_signal<bool> not_clk;
			sc_signal<sc_logic> rst;
			sc_signal<sc_logic> run;
			sc_signal<sc_logic> dont_run;
			sc_signal<sc_logic> end_of_job0;
			sc_signal<sc_logic> end_of_job1;
			sc_signal<sc_logic> end_of_job2;
			sc_signal<sc_logic> end_of_job3;
			
			/**
			* core instantiating
			**/ 
			auto* core0 = new demoproc_struct::demo_core("core0", parameter->program_initializer, parameter->data_initializer, parameter->dissipating_layer,
													layout::xy_length_t(parameter->h_margin,parameter->v_margin), systemc_engine, sunred_layout);
			core0->clk(not_clk);
			core0->rst(rst);
			core0->run(dont_run);
			core0->end_of_job(end_of_job0);
			//core0->layout_item.reflect_vertical();

			auto* core1 = new demoproc_struct::demo_core("core1", parameter->program_initializer, parameter->data_initializer, parameter->dissipating_layer,
													layout::xy_length_t(parameter->h_margin+parameter->core_width+parameter->h_core_space,parameter->v_margin), systemc_engine, sunred_layout);
			core1->clk(not_clk);
			core1->rst(rst);
			core1->run(dont_run);
			core1->end_of_job(end_of_job1);
			
			auto* core2 = new demoproc_struct::demo_core("core2", parameter->program_initializer, parameter->data_initializer, parameter->dissipating_layer,
													layout::xy_length_t(parameter->h_margin,parameter->v_margin+parameter->core_height+parameter->v_core_space), systemc_engine, sunred_layout);
			core2->clk(clk);
			core2->rst(rst);
			core2->run(run);
			core2->end_of_job(end_of_job2);
			//core2->layout_item.reflect_vertical();
			
			auto* core3 = new demoproc_struct::demo_core("core3", parameter->program_initializer, parameter->data_initializer, parameter->dissipating_layer,
													layout::xy_length_t(parameter->h_margin+parameter->core_width+parameter->h_core_space,parameter->v_margin+parameter->core_height+parameter->v_core_space), systemc_engine, sunred_layout);
			core3->clk(clk);
			core3->rst(rst);
			core3->run(run);
			core3->end_of_job(end_of_job3);	

			rst = SC_LOGIC_0;
			run = SC_LOGIC_0;
			not_clk = false;
			dont_run = SC_LOGIC_0;
			sc_start(12, SC_US);
			rst = SC_LOGIC_1;
			sc_start(11, SC_US);
			rst = SC_LOGIC_0;
			sc_start(9, SC_US);
			run = SC_LOGIC_1;
			sc_start(61, SC_US);

			//  Start Timers
			manager->start_timer();
			
			sc_start(static_cast<double>(parameter->simulation_time), SC_SEC);
			
			//  Stop timers
			std::pair<unit::time_t,unit::time_t> time = manager->get_elapsed_time();

			layout::xyz_pitch_t xyz_pitch = sunred_layout->get_pitch();
			info_log << xyz_pitch[0]*xyz_pitch[1]*xyz_pitch[2] << ", " << time.first << ", " << time.second  << std::endl;
			
			std::fstream temp_image("./output/temps_" + parameter->id + ".map", std::fstream::out);
			if (temp_image) sunred_engine->print_temperature_map(temp_image);

			std::fstream temp_svg_image("./output/temps_" + parameter->id + ".svg", std::fstream::out);
			if (temp_svg_image) manager->print_temperature_map_in_svg(temp_svg_image);

			std::fstream dis_image("./output/dis_" + parameter->id + ".map", std::fstream::out);
			if (dis_image) sunred_engine->print_dissipation_map(dis_image);
			
			delete core0;
			delete core1;
			delete core2;
			delete core3;
			delete dissipating_layer_observer;

			delete manager;
			// delete sc_core::sc_curr_simcontext; // MEM_LEAK!!
			sc_core::sc_curr_simcontext = new sc_core::sc_simcontext();
			sc_core::sc_default_global_context = sc_core::sc_curr_simcontext;
		}
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
	}
	catch(...)
	{
		std::cerr << "You fucked up." << std::endl;
	}
	
	
	return 0;
}
