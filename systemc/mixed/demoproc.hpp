#ifndef _DEMOPROC_MIXED_
#define _DEMOPROC_MIXED_

#include <logitherm>


#include "../struct/demoproc.hpp"
#include "../gate/demoproc.hpp"
#include "../simulation_parameters.hpp"

namespace demoproc_mixed
{	
	SC_MODULE(demo_core)
	{
		sc_in<bool> clk;
		sc_in<sc_logic> rst;
		sc_in<sc_logic> run;
		sc_out<sc_logic> end_of_job;
		
		
		
		//CTRL-bol kiindulo jelek
		sc_signal<sc_logic> ce_IR_from_CTRL;
		sc_signal<sc_logic> ce_IP_from_CTRL;
		sc_signal<sc_logic> ce_destination_from_CTRL;
		sc_signal<sc_logic> ce_instruction_code_from_CTRL;
		sc_signal<sc_logic> ce_IMM_from_CTRL;
		sc_signal<sc_logic> ce_OP_A_from_CTRL;
		sc_signal<sc_logic> ce_OP_B_from_CTRL;
		sc_signal<sc_logic> ce_MAR_from_CTRL;
		sc_signal<sc_logic> ce_MDR_from_CTRL;
		sc_signal<sc_logic> ce_Z_from_CTRL;
		sc_signal<sc_logic> ce_N_from_CTRL;
		sc_signal<sc_lv<2>> sel_ALU_from_CTRL;
		sc_signal<sc_logic> sel_IP_from_CTRL;
		sc_signal<sc_logic> sel_IP_ADD_from_CTRL;
		sc_signal<sc_logic> write_data_cache_from_CTRL;
		sc_signal<sc_logic> we_RF_from_CTRL;
		sc_signal<sc_logic> eoj_core0;

		//IR-bol kiindulo jel
		sc_signal<sc_lv<16>> dout_from_IR;
		
		//RS_IR_7_4-bol kiindulo jel
		sc_signal<sc_lv<4>> out_from_RS_IR_7_4;
		
		//RS_IR_7_0-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_RS_IR_7_0;
		
		//RS_IR_3_0-bol kiindulo jel
		sc_signal<sc_lv<4>> out_from_RS_IR_3_0;
		
		//RS_IR_11_8-bol kiindulo jel
		sc_signal<sc_lv<4>>	out_from_RS_IR_11_8;
		
		//RS_IR_15_12-bol kiindulo jel
		sc_signal<sc_lv<4>> out_from_RS_IR_15_12;
		
		//RF-bol kiindulo jelek
		sc_signal<sc_lv<8>> data_out_a_from_RF;
		sc_signal<sc_lv<8>> data_out_b_from_RF;
		sc_signal<sc_lv<8>> data_out_c_from_RF;
		
		//ADDR_ADD-bol kiindulo jel
		sc_signal<sc_lv<8>> result_from_ADDR_ADD;
		
		//DATA_CACHE-bol kiindulo jel
		sc_signal<sc_lv<8>> data_out_a_from_DATA_CACHE;
		//DATA_CACHE dummy jelek
		sc_signal<sc_lv<8>> data_out_b_from_DATA_CACHE;
		sc_signal<sc_lv<8>> data_out_c_from_DATA_CACHE;
		sc_signal<sc_lv<8>>	address_b_to_DATA_CACHE;
		sc_signal<sc_lv<8>>	address_c_to_DATA_CACHE;
		
		//OP_B-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_OP_B;
		
		//MUX_ALU-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_MUX_ALU;
		
		//MUX_ALU dummy jel
		sc_signal<sc_lv<8>> input4_to_MUX_ALU;
		
		//OP_A-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_OP_A;
		
		
		//DESTINATION-bol kiindulo jel
		sc_signal<sc_lv<4>> dout_from_DESTINATION;
		
		//ALU-bol kiindulo jelek
		sc_signal<sc_lv<8>> result_from_ALU;
		sc_signal<sc_logic> z_out_from_ALU;
		sc_signal<sc_logic> n_out_from_ALU;
		
		//Z-bol kiindulo jel
		sc_signal<sc_logic> dout_from_Z;
		
		//N-bol kiindulo jel
		sc_signal<sc_logic> dout_from_N;
		
		//INSTRUCTION_CODE-bol kiindulo jel
		sc_signal<sc_lv<4>> dout_from_INSTRUCTION_CODE;
		
		//INSTRUCTION_DECODER-bol kiindulo jel
		sc_signal<sc_lv<3>> alu_func_from_INSTRUCTION_DECODER;
		
		//MAR-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_MAR;
		
		//MDR-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_MDR;

		//INSTRUCTION_CACHE-bol kiindulo jel
		sc_signal<sc_lv<16>> data_out_a_from_INSTRUCTION_CACHE;
		//INSTRUCTION_CACHE dummy jelek
		sc_signal<sc_lv<16>>	data_out_b_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<16>>	data_out_c_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<8>>		address_b_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<8>>		address_c_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<16>>	data_in_to_INSTRUCTION_CACHE;
		sc_signal<sc_logic>		write_enable_to_INSTRUCTION_CACHE;
		sc_signal<sc_lv<8>>		write_address_to_INSTRUCTION_CACHE;
		
		//IMM-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_IMM;
		//MUX_IP_ADD-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_MUX_IP_ADD;
		
		//MUX_IP_ADD dummy jel
		sc_signal<sc_lv<8>> input0_to_MUX_IP_ADD;
		
		
		
		//IP-bol kiindulo jel
		sc_signal<sc_lv<8>> dout_from_IP;
		
		//IP_ADD-bol kiindulo jel
		sc_signal<sc_lv<8>> result_from_IP_ADD;
		
		//MUX_IP-bol kiindulo jel
		sc_signal<sc_lv<8>> out_from_MUX_IP;
		//MUX_IP dummy jel
		sc_signal<sc_lv<8>> input0_to_MUX_IP;
		
		layout::sunred::component_t layout_item;
		
		demoproc_struct::controller 				cntrl;
		demoproc_struct::register_16 				ir;
		demoproc_struct::mux2_8 					mux_ip;
		demoproc_struct::register_8 				ip;
		demoproc_ams035::register_4 				inst_code;
		demoproc_struct::register_4 				dest;
		demoproc_struct::register_8 				op_a;
		demoproc_struct::register_8 				op_b;
		demoproc_struct::register_8 				imm;
		demoproc_struct::register_8 				mar;
		demoproc_struct::register_8 				mdr;
		demoproc_ams035::register_1 				z;
		demoproc_struct::register_1 				n;
		demoproc_struct::regfile 					rf;
		demoproc_struct::mux2_8 					mux_ip_add;
		demoproc_struct::adder_8 					ip_add;
		demoproc_struct::adder_8 					addr_add;
		demoproc_ams035::instruction_decoder 		inst_dec;
		demoproc_struct::mux4_8 					mux_alu;
		demoproc_ams035::alu_8 						alu;
		demoproc_struct::memory<8,16>				inst_cache;	
		demoproc_struct::memory<8,8>				data_cache;	
		demoproc_struct::range_selector<16,3,0> 	rs_ir_3_0;	
		demoproc_struct::range_selector<16,7,4> 	rs_ir_7_4;	
		demoproc_struct::range_selector<16,11,8> 	rs_ir_11_8;	
		demoproc_struct::range_selector<16,7,0> 	rs_ir_7_0;	
		demoproc_struct::range_selector<16,15,12> 	rs_ir_15_12;
		
		demo_core(	const sc_module_name& nm,
					const std::array<sc_lv<16>,256>& program_initializer,
					const std::array<sc_lv<8>,256>& data_initializer,
					const std::string& layer_name,
					const layout::xy_length_t& position,
					logic::systemc::adapter_t* logic_adapter,
					layout::sunred::adapter_t* layout_adapter)
		:
			sc_module(nm, logic_adapter),
			layout_item(id, layer_name, position, layout::xy_length_t(525_um,559_um), nullptr, layout_adapter),
			cntrl("cntrl", layer_name, layout::xy_length_t(385_um,0_um), &layout_item, logic_adapter),
			ir("ir", layer_name, layout::xy_length_t(385_um,156_um), &layout_item, logic_adapter),
			ip("ip", layer_name, layout::xy_length_t(0_um,416_um), &layout_item, logic_adapter),
			mux_ip("mux_ip", layer_name, layout::xy_length_t(84_um,416_um), &layout_item, logic_adapter),
			ip_add("ip_add", layer_name, layout::xy_length_t(112_um,416_um), &layout_item, logic_adapter),
			mux_ip_add("mux_ip_add", layer_name, layout::xy_length_t(168_um,416_um), &layout_item, logic_adapter),
			inst_code("inst_code", layer_name, layout::xy_length_t(290.5_um,416_um), &layout_item, logic_adapter),
			dest("dest", layer_name, layout::xy_length_t(248.5_um,416_um), &layout_item, logic_adapter),
			op_a("op_a", layer_name, layout::xy_length_t(0_um,494_um), &layout_item, logic_adapter),
			op_b("op_b", layer_name, layout::xy_length_t(84_um,494_um), &layout_item, logic_adapter),
			imm("imm", layer_name, layout::xy_length_t(168_um,494_um), &layout_item, logic_adapter),
			mar("mar", layer_name, layout::xy_length_t(252_um,494_um), &layout_item, logic_adapter),
			mdr("mdr", layer_name, layout::xy_length_t(336_um,494_um), &layout_item, logic_adapter),
			rf("rf", layer_name, layout::xy_length_t(0_um,0_um), &layout_item, logic_adapter),
			addr_add("addr_add", layer_name, layout::xy_length_t(420_um,494_um), &layout_item, logic_adapter),
			inst_dec("inst_dec", layer_name, layout::xy_length_t(203_um,416_um), &layout_item, logic_adapter),
			mux_alu("mux_alu", layer_name, layout::xy_length_t(332.5_um,416_um), &layout_item, logic_adapter),
			alu("alu", layer_name, layout::xy_length_t(385_um,312_um), &layout_item, logic_adapter),
			z("z", layer_name, layout::xy_length_t(462_um,468_um), &layout_item, logic_adapter),
			n("n", layer_name, layout::xy_length_t(493.5_um,468_um), &layout_item, logic_adapter),
			inst_cache("inst_cache", program_initializer),
			data_cache("data_cache", data_initializer),
			rs_ir_3_0("rs_ir_3_0"),
			rs_ir_7_4("rs_ir_7_4"),
			rs_ir_11_8("rs_ir_11_8"),
			rs_ir_7_0("rs_ir_7_0"),
			rs_ir_15_12("rs_ir_15_12")
			
		{
			if(parameter->trace_dissipation)
			{
				add_static_dissipation_trace(parameter->trace_path, parameter->id);
				add_dynamic_dissipation_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_temperature)
			{
				layout_item.add_max_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_min_temperature_trace(parameter->trace_path, parameter->id);
				layout_item.add_avg_temperature_trace(parameter->trace_path, parameter->id);
			}
			
			if(parameter->trace_critical_path)
			{
				logic::systemc::path_delay_trace_t* critical_path = add_path_delay_trace(parameter->trace_path, id + "_critical_path", parameter->id);
				
				/** critical path inst_code 1-es regiszterebol indul **/
				add_delay_trace_to_path_trace(critical_path, &inst_code.dout_reg1.q_delay);
				
				/** critical path inst_dec-ben **/
				add_delay_trace_to_path_trace(critical_path, &inst_dec.g79.q_delay);
				add_delay_trace_to_path_trace(critical_path, &inst_dec.g76.q_delay);
				add_delay_trace_to_path_trace(critical_path, &inst_dec.g75.q_delay);
				
				/** critical path az aluban **/
				add_delay_trace_to_path_trace(critical_path, &alu.inv_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.nand3_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.inv_1.q_delay);
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice0.nand_1.q_delay); //slice0.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice0.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice0.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice0.inv_4.q_delay); //slice0.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice1.nand_1.q_delay); //slice1.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice1.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice1.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice1.inv_4.q_delay); //slice1.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice2.nand_1.q_delay); //slice2.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice2.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice2.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice2.inv_4.q_delay); //slice2.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice3.nand_1.q_delay); //slice3.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice3.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice3.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice3.inv_4.q_delay); //slice3.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice4.nand_1.q_delay); //slice4.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice4.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice4.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice4.inv_4.q_delay); //slice4.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice5.nand_1.q_delay); //slice5.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice5.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice5.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice5.inv_4.q_delay); //slice5.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice6.nand_1.q_delay); //slice6.cin
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice6.inv_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice6.nor_2.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice6.inv_4.q_delay); //slice6.cout
				
				add_delay_trace_to_path_trace(critical_path, &alu.alu_slice7.xor_3.q_delay); //slice7.cin
				
				add_delay_trace_to_path_trace(critical_path, &alu.mux4_1.g137.q_delay); //mux4_1 7. bitje
				
				add_delay_trace_to_path_trace(critical_path, &alu.nor4_2.q_delay); //slice7.cin
				
				add_delay_trace_to_path_trace(critical_path, &alu.nand2_1.q_delay);
				add_delay_trace_to_path_trace(critical_path, &alu.inv_4.q_delay); //ez az alu z_out-ja
				
				/** critical path a z regiszterben **/
				add_delay_trace_to_path_trace(critical_path, &z.g9.q_delay);
				add_delay_trace_to_path_trace(critical_path, &z.g8.q_delay);	
			}

			//CTRL
			cntrl.ce_IR(ce_IR_from_CTRL);
			cntrl.ce_IP(ce_IP_from_CTRL);
			cntrl.ce_destination(ce_destination_from_CTRL);
			cntrl.ce_instruction_code(ce_instruction_code_from_CTRL);
			cntrl.ce_IMM(ce_IMM_from_CTRL);
			cntrl.ce_OP_A(ce_OP_A_from_CTRL);
			cntrl.ce_OP_B(ce_OP_B_from_CTRL);
			cntrl.ce_MAR(ce_MAR_from_CTRL);
			cntrl.ce_MDR(ce_MDR_from_CTRL);
			cntrl.ce_Z(ce_Z_from_CTRL);
			cntrl.ce_N(ce_N_from_CTRL);
			cntrl.sel_ALU(sel_ALU_from_CTRL);
			cntrl.sel_IP(sel_IP_from_CTRL);
			cntrl.sel_IP_add(sel_IP_ADD_from_CTRL);
			cntrl.write_data_cache(write_data_cache_from_CTRL);
			cntrl.we_RF(we_RF_from_CTRL);
			cntrl.ss_Z(dout_from_Z);
			cntrl.ss_N(dout_from_N);
			cntrl.ss_instruction_code(dout_from_INSTRUCTION_CODE);
			cntrl.clock(clk);
			cntrl.reset(rst);
			cntrl.run(run);
			cntrl.end_of_job(end_of_job);
			
			//IR
			ir.input(data_out_a_from_INSTRUCTION_CACHE);
			ir.enable(ce_IR_from_CTRL);
			ir.output(dout_from_IR);
			ir.clock(clk);
			ir.reset(rst);
			
			//RS_IR_3_0
			rs_ir_3_0.in(dout_from_IR);
			rs_ir_3_0.out(out_from_RS_IR_3_0);
			
			//RS_IR_7_4
			rs_ir_7_4.in(dout_from_IR);
			rs_ir_7_4.out(out_from_RS_IR_7_4);
			
			//RS_IR_7_0
			rs_ir_7_0.in(dout_from_IR);
			rs_ir_7_0.out(out_from_RS_IR_7_0);
			
			//RS_IR_11_8
			rs_ir_11_8.in(dout_from_IR);
			rs_ir_11_8.out(out_from_RS_IR_11_8);
			
			//RS_IR_15_12
			rs_ir_15_12.in(dout_from_IR);
			rs_ir_15_12.out(out_from_RS_IR_15_12);
			
			//RF
			rf.read_address_a(out_from_RS_IR_7_4);
			rf.read_address_b(out_from_RS_IR_3_0);
			rf.read_address_c(out_from_RS_IR_11_8);
			rf.write_enable(we_RF_from_CTRL);
			rf.write_address(dout_from_DESTINATION);
			rf.data_in(result_from_ALU);
			rf.data_out_a(data_out_a_from_RF);
			rf.data_out_b(data_out_b_from_RF);
			rf.data_out_c(data_out_c_from_RF);
			rf.clock(clk);
			
			//ADDR_ADD
			addr_add.input0(data_out_a_from_RF);
			addr_add.input1(data_out_b_from_RF);
			addr_add.result(result_from_ADDR_ADD);
			
			
			//DATA_CACHE
			data_cache.data_in(dout_from_MDR);
			data_cache.write_address(dout_from_MAR);
			data_cache.write_enable(write_data_cache_from_CTRL);
			data_cache.read_address_a(dout_from_MAR);
			data_cache.data_out_a(data_out_a_from_DATA_CACHE);
			data_cache.clock(clk);
			
			//DATA_CACHE dummy jelek
			data_cache.read_address_b(address_b_to_DATA_CACHE);
			data_cache.read_address_c(address_c_to_DATA_CACHE);
			data_cache.data_out_b(data_out_b_from_DATA_CACHE);
			data_cache.data_out_c(data_out_c_from_DATA_CACHE);
			
			//OP_B
			op_b.input(data_out_b_from_RF);
			op_b.enable(ce_OP_B_from_CTRL);
			op_b.output(dout_from_OP_B);
			op_b.clock(clk);
			op_b.reset(rst);
			
			//MUX_ALU
			mux_alu.input0(dout_from_IMM);
			mux_alu.input1(data_out_a_from_DATA_CACHE);
			mux_alu.input2(dout_from_OP_B);
			mux_alu.input3(input4_to_MUX_ALU);
			mux_alu.select(sel_ALU_from_CTRL);
			mux_alu.output(out_from_MUX_ALU);
			
			//OP_A
			op_a.input(data_out_a_from_RF);
			op_a.enable(ce_OP_A_from_CTRL);
			op_a.output(dout_from_OP_A);
			op_a.clock(clk);
			op_a.reset(rst);
			
			//DESTINATION
			dest.input(out_from_RS_IR_11_8);
			dest.enable(ce_destination_from_CTRL);
			dest.output(dout_from_DESTINATION);
			dest.clock(clk);
			dest.reset(rst);
			
			//ALU
			alu.alu_function(alu_func_from_INSTRUCTION_DECODER);
			alu.inputa(dout_from_OP_A);
			alu.inputb(out_from_MUX_ALU);
			alu.output(result_from_ALU);
			alu.z_out(z_out_from_ALU);
			alu.n_out(n_out_from_ALU);
			
			//Z
			z.input(z_out_from_ALU);
			z.enable(ce_Z_from_CTRL);
			z.output(dout_from_Z);
			z.clock(clk);
			z.reset(rst);
			
			//N
			n.input(n_out_from_ALU);
			n.enable(ce_N_from_CTRL);
			n.output(dout_from_N);
			n.clock(clk);
			n.reset(rst);
			
			//INSTRUCTION_CODE
			inst_code.input(out_from_RS_IR_15_12);
			inst_code.enable(ce_instruction_code_from_CTRL);
			inst_code.output(dout_from_INSTRUCTION_CODE);
			inst_code.clock(clk);
			inst_code.reset(rst);
			
			//INSTRUCTION_DECODER
			inst_dec.opcode(dout_from_INSTRUCTION_CODE);
			inst_dec.alu_func(alu_func_from_INSTRUCTION_DECODER);
			
			//MAR
			mar.input(result_from_ADDR_ADD);
			mar.enable(ce_MAR_from_CTRL);
			mar.output(dout_from_MAR);
			mar.clock(clk);
			mar.reset(rst);
			
			//MDR
			mdr.input(data_out_c_from_RF);
			mdr.enable(ce_MDR_from_CTRL);
			mdr.output(dout_from_MDR);
			mdr.clock(clk);
			mdr.reset(rst);
			
			//INSTRUCTION_CACHE
			inst_cache.read_address_a(dout_from_IP);
			inst_cache.data_out_a(data_out_a_from_INSTRUCTION_CACHE);
			inst_cache.clock(clk);
			
			//INSTRUCTION_CACHE dummy jelek
			inst_cache.read_address_b(address_b_to_INSTRUCTION_CACHE);
			inst_cache.read_address_c(address_c_to_INSTRUCTION_CACHE);
			inst_cache.data_out_b(data_out_b_to_INSTRUCTION_CACHE);
			inst_cache.data_out_c(data_out_c_to_INSTRUCTION_CACHE);
			inst_cache.data_in(data_in_to_INSTRUCTION_CACHE);
			inst_cache.write_address(write_address_to_INSTRUCTION_CACHE);
			inst_cache.write_enable(write_enable_to_INSTRUCTION_CACHE);
			
			//IMM
			imm.input(out_from_RS_IR_7_0);
			imm.enable(ce_IMM_from_CTRL);
			imm.output(dout_from_IMM);
			imm.clock(clk);
			imm.reset(rst);
			
			//MUX_IP_ADD
			mux_ip_add.input0(input0_to_MUX_IP_ADD);
			mux_ip_add.input1(dout_from_IMM);
			mux_ip_add.select(sel_IP_ADD_from_CTRL);
			mux_ip_add.output(out_from_MUX_IP_ADD);
			
			//IP
			ip.input(out_from_MUX_IP);
			ip.enable(ce_IP_from_CTRL);
			ip.output(dout_from_IP);
			ip.clock(clk);
			ip.reset(rst);
			
			//IP_ADD
			ip_add.input0(out_from_MUX_IP_ADD);
			ip_add.input1(dout_from_IP);
			ip_add.result(result_from_IP_ADD);
			
			//MUX_IP
			mux_ip.input0(input0_to_MUX_IP);
			mux_ip.input1(result_from_IP_ADD);
			mux_ip.select(sel_IP_from_CTRL);
			mux_ip.output(out_from_MUX_IP);
			
			
			
			input0_to_MUX_IP_ADD = "00000001",
			input0_to_MUX_IP = "00000000";
		}
	};

}	//namespace demoproc_struct

#endif //_DEMOPROC_MIXED_
