#include "../../gate/instruction_decoder.hpp"
#include "../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	
    std::random_device rd;
    std::mt19937 gen(rd());
	std::uniform_int_distribution<size_t> dis(0,255);

	
	::layout::sunred::xy_coordinates_t layout_size(23,3);
	std::string layer_name = "layer";
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
	
	::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
	
	demoproc_ams_035::instruction_decoder inst_dec_gate("inst_dec_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
	demoproc_struct::instruction_decoder inst_dec_struct("inst_dec_struct", layer_name, ::layout::sunred::xy_coordinates_t(12,1), nullptr);
	
	//::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(inst_dec_gate.name());
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(inst_dec_struct.name());
	
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(inst_dec_gate.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(inst_dec_gate.name());
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(inst_dec_struct.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(inst_dec_struct.name());
	
	sc_lv<4> instruction_array[12] = {
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_i2rf)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_m2rf)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_rf2m)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_sub)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_and)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_or)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_xor)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_jmp)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_bz)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_bz)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_bn)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_eoj))		
		};
	
	sc_clock clk("clock", 1, SC_US, 0.5, 5, SC_NS, false);
	sc_signal<sc_lv<4>> opcode;
	sc_signal<sc_lv<3>> alu_func_gate;
	sc_signal<sc_lv<3>> alu_func_struct;
	
	inst_dec_gate.opcode(opcode);
	inst_dec_gate.alu_func(alu_func_gate);
	
	inst_dec_struct.opcode(opcode);
	inst_dec_struct.alu_func(alu_func_struct);	
	
	sc_start(200, SC_NS);
	for(size_t it = 0; it < 1024; ++it)
	{		
		opcode = instruction_array[dis(gen)%12];
		sc_start(1, SC_US);

	}
	sc_start(1, SC_US);
	
	//sc_close_vcd_trace_file(tf);
	return 0;
}
