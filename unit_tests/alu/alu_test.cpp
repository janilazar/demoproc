#include "../../gate/alu_8.hpp"
#include "../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	sc_report_handler::set_actions (SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
	//sc_report_handler::set_actions (SC_ID_LOGIC_X_TO_BOOL_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
	
    std::random_device rd;
    std::mt19937 gen(rd());
	std::uniform_int_distribution<size_t> dis(0,255);

	
	::layout::sunred::xy_coordinates_t layout_size(75,15);
	std::string layer_name = "layer";
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
	
	::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
	
	demoproc_ams_035::alu_8 alu_8_gate("alu_8_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
	demoproc_struct::alu_8 alu_8_struct("alu_8_struct", layer_name, ::layout::sunred::xy_coordinates_t(38,1), nullptr);
	
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(alu_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(alu_8_struct.name());
	
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(alu_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(alu_8_gate.name());
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(alu_8_struct.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(alu_8_struct.name());
	
	sc_signal<sc_lv<8>> inputa, inputb, output_gate, output_struct;
	sc_signal<sc_lv<3>> alu_function; 
	sc_signal<sc_logic> z_out_gate, z_out_struct, n_out_gate, n_out_struct; 
	
	
	
	alu_8_gate.inputa(inputa);
	alu_8_gate.inputb(inputb);
	alu_8_gate.alu_function(alu_function);
	alu_8_gate.output(output_gate);
	alu_8_gate.z_out(z_out_gate);
	alu_8_gate.n_out(n_out_gate);
	
	alu_8_struct.inputa(inputa);
	alu_8_struct.inputb(inputb);
	alu_8_struct.alu_function(alu_function);
	alu_8_struct.output(output_struct);
	alu_8_struct.z_out(z_out_struct);
	alu_8_struct.n_out(n_out_struct);
	
	
	sc_clock clk("clk", 1, SC_US, 0.5, 5, SC_NS, false);
	
	sc_trace_file *tf = sc_create_vcd_trace_file("wave");
	sc_trace(tf, inputa, "inputa");
	sc_trace(tf, inputb, "inputb");
	sc_trace(tf, alu_function, "alu_function");
	
	sc_trace(tf, output_gate, "output_gate");
	sc_trace(tf, z_out_gate, "z_out_gate");
	sc_trace(tf, n_out_gate, "n_out_gate");
	
	sc_trace(tf, output_struct, "output_struct");
	sc_trace(tf, z_out_struct, "z_out_struct");
	sc_trace(tf, n_out_struct, "n_out_struct");
	
	
	sc_start(500, SC_NS);
	
	
	inputa = dis(gen);
	inputb = dis(gen);
	
	
	/** alu_func **/
	for(size_t it = 0; it < 128; ++it)
	{
		sc_start(1, SC_US);
		alu_function = dis(gen) % 5;
	}
	
	
	alu_function = size_t(demoproc_struct::alu_8::function::alu_add);
	for(size_t it = 0; it < 128; ++it)
	{
		
		sc_start(1, SC_US);
		switch(dis(gen)%3)
		{
			case 0:
				inputa = dis(gen);
				break;
			case 1:
				inputb = dis(gen);
				break;
			case 2:
				inputa = dis(gen);
				inputb = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	
	alu_function = size_t(demoproc_struct::alu_8::function::alu_sub);
	for(size_t it = 0; it < 128; ++it)
	{
		
		sc_start(1, SC_US);
		switch(dis(gen)%3)
		{
			case 0:
				inputa = dis(gen);
				break;
			case 1:
				inputb = dis(gen);
				break;
			case 2:
				inputa = dis(gen);
				inputb = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	
	alu_function = size_t(demoproc_struct::alu_8::function::alu_and);
	for(size_t it = 0; it < 128; ++it)
	{
		
		sc_start(1, SC_US);
		switch(dis(gen)%3)
		{
			case 0:
				inputa = dis(gen);
				break;
			case 1:
				inputb = dis(gen);
				break;
			case 2:
				inputa = dis(gen);
				inputb = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	
	alu_function = size_t(demoproc_struct::alu_8::function::alu_or);
	for(size_t it = 0; it < 128; ++it)
	{
		
		sc_start(1, SC_US);
		switch(dis(gen)%3)
		{
			case 0:
				inputa = dis(gen);
				break;
			case 1:
				inputb = dis(gen);
				break;
			case 2:
				inputa = dis(gen);
				inputb = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	
	alu_function = size_t(demoproc_struct::alu_8::function::alu_xor);
	for(size_t it = 0; it < 128; ++it)
	{
		
		sc_start(1, SC_US);
		switch(dis(gen)%3)
		{
			case 0:
				inputa = dis(gen);
				break;
			case 1:
				inputb = dis(gen);
				break;
			case 2:
				inputa = dis(gen);
				inputb = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	sc_start(1, SC_US);
	
	sc_close_vcd_trace_file(tf);
	return 0;
}
