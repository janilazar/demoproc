#include "../../gate/regfile.hpp"
#include "../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	try
	{
		sc_report_handler::set_actions (SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
		//sc_report_handler::set_actions (SC_ID_LOGIC_X_TO_BOOL_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
			
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<size_t> dis(0,255);

		
		::layout::sunred::xy_coordinates_t layout_size(223,33);
		std::string layer_name = "layer";
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
		
		::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
		
		demoproc_ams_035::regfile regfile_gate("regfile_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
		demoproc_struct::regfile regfile_struct("regfile_struct", layer_name, ::layout::sunred::xy_coordinates_t(112,1), nullptr);
		
		//::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(regfile_gate.name());
		//::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(regfile_struct.name());
		
		
		::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(regfile_gate.name());
		::logitherm::logitherm_manager::get_logitherm().trace_component_activity(regfile_gate.name());
		
		::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(regfile_struct.name());
		::logitherm::logitherm_manager::get_logitherm().trace_component_activity(regfile_struct.name());


		
		sc_clock clk("clock", 1, SC_US, 0.5, 5, SC_NS, false);
		sc_signal<sc_lv<8>> data_in;
		sc_signal<sc_lv<4>> write_address, read_address[3];
		sc_signal<sc_logic> write_enable;
		sc_signal<sc_lv<8>> data_out_a_gate, data_out_b_gate, data_out_c_gate;
		sc_signal<sc_lv<8>> data_out_a_struct, data_out_b_struct, data_out_c_struct;

		
		regfile_gate.read_address_a(read_address[0]);
		regfile_gate.read_address_b(read_address[1]);
		regfile_gate.read_address_c(read_address[2]);
		regfile_gate.write_enable(write_enable);
		regfile_gate.write_address(write_address);
		regfile_gate.data_in(data_in);
		regfile_gate.data_out_a(data_out_a_gate);
		regfile_gate.data_out_b(data_out_b_gate);
		regfile_gate.data_out_c(data_out_c_gate);
		regfile_gate.clock(clk);
		
		regfile_struct.read_address_a(read_address[0]);
		regfile_struct.read_address_b(read_address[1]);
		regfile_struct.read_address_c(read_address[2]);
		regfile_struct.write_enable(write_enable);
		regfile_struct.write_address(write_address);
		regfile_struct.data_in(data_in);
		regfile_struct.data_out_a(data_out_a_struct);
		regfile_struct.data_out_b(data_out_b_struct);
		regfile_struct.data_out_c(data_out_c_struct);
		regfile_struct.clock(clk);	
		
		sc_start(200, SC_NS);
		for(size_t it = 0; it < 16; ++it)
		{
			write_address = it;
			data_in = dis(gen);
			sc_start(1, SC_US);
			write_enable = SC_LOGIC_1;
			sc_start(1, SC_US);
			write_enable = SC_LOGIC_0;
			sc_start(1, SC_US);
		}
		
		for(size_t it = 0; it < 4096; ++it)
		{
			switch(dis(gen)%3)
			{
				case 0: //2 olvasas 1 iras
				{
					
					size_t not_sel = dis(gen)%3;
					
					for(size_t jt = 0; jt < 3; ++jt)
						if(jt != not_sel)
							read_address[jt] = dis(gen)%16;
					
					write_address = it;
					data_in = dis(gen);
					sc_start(1, SC_US);
					write_enable = SC_LOGIC_1;
					sc_start(1, SC_US);
					write_enable = SC_LOGIC_0;
					sc_start(1, SC_US);
					break;
				}
				case 1: // csak iras
					write_address = it;
					data_in = dis(gen);
					sc_start(1, SC_US);
					write_enable = SC_LOGIC_1;
					sc_start(1, SC_US);
					write_enable = SC_LOGIC_0;
					sc_start(1, SC_US);
					break;
				case 2: //3 olvasas
					read_address[0] = dis(gen)%16;
					read_address[1] = dis(gen)%16;
					read_address[2] = dis(gen)%16;
					sc_start(1, SC_US);
					break;
				default:
					throw("modulo error");
			}
		}
		sc_start(1, SC_US);
		
		//sc_close_vcd_trace_file(tf);
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
	}
	return 0;
}
