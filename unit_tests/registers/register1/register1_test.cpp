#include "../../../gate/register_1.hpp"
#include "../../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	try
	{
	
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<size_t> dis(0,255);

		
		::layout::sunred::xy_coordinates_t layout_size(36,7);
		std::string layer_name = "layer";
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
		
		::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
		::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
		
		demoproc_ams_035::register_1 register_1_gate("register_1_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
		demoproc_struct::register_1 register_1_struct("register_1_struct", layer_name, ::layout::sunred::xy_coordinates_t(18,1), nullptr);
		
		//::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(register_1_gate.name());
		//::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(register_1_struct.name());
		
		
		::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(register_1_gate.name());
		::logitherm::logitherm_manager::get_logitherm().trace_component_activity(register_1_gate.name());
		
		::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(register_1_struct.name());
		::logitherm::logitherm_manager::get_logitherm().trace_component_activity(register_1_struct.name());

		sc_signal<sc_logic> input, reset, enable, output_gate, output_struct;
		sc_clock clk("clk", 1, SC_US, 0.5, 5, SC_NS, false);
		
		register_1_gate.input(input);
		register_1_gate.reset(reset);
		register_1_gate.enable(enable);
		register_1_gate.clock(clk);
		register_1_gate.output(output_gate);
		
		register_1_struct.input(input);
		register_1_struct.reset(reset);
		register_1_struct.enable(enable);
		register_1_struct.clock(clk);
		register_1_struct.output(output_struct);
		
		
		sc_start(200, SC_NS);
		
		for(size_t it = 0; it < 64; ++it)
		{
			enable = sc_logic(static_cast<bool>(dis(gen)%2));
			sc_start(1, SC_US);
			
			for(size_t jt = 0; jt < 8; ++jt)
			{
				input = sc_logic(static_cast<bool>(dis(gen)%2));
				sc_start(1, SC_US);
			}
			if((dis(gen)%4) == 0)
			{
				reset = SC_LOGIC_1;
				sc_start(1, SC_US);
				reset = SC_LOGIC_0;
				sc_start(1, SC_US);
			}
		}
		sc_start(1, SC_US);
	
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
	}	
	return 0;
}
