#include "../../../gate/mux4_8.hpp"
#include "../../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	sc_report_handler::set_actions (SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
	//sc_report_handler::set_actions (SC_ID_LOGIC_X_TO_BOOL_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
	
    std::random_device rd;
    std::mt19937 gen(rd());
	std::uniform_int_distribution<size_t> dis(0,255);

	
	::layout::sunred::xy_coordinates_t layout_size(33,7);
	std::string layer_name = "layer";
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
	
	::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
	
	demoproc_ams_035::mux4_8 mux4_8_gate("mux4_8_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
	demoproc_struct::mux4_8 mux4_8_struct("mux4_8_struct", layer_name, ::layout::sunred::xy_coordinates_t(16,1), nullptr);
	
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(mux4_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(mux4_8_struct.name());
	
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(mux4_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(mux4_8_gate.name());
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(mux4_8_struct.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(mux4_8_struct.name());
	
	sc_signal<sc_lv<8>> input[4], output_gate, output_struct;
	sc_signal<sc_lv<2>> select; 
	
	
	
	mux4_8_gate.input0(input[0]);
	mux4_8_gate.input1(input[1]);
	mux4_8_gate.input2(input[2]);
	mux4_8_gate.input3(input[3]);
	mux4_8_gate.select(select);
	mux4_8_gate.output(output_gate);
	
	mux4_8_struct.input0(input[0]);
	mux4_8_struct.input1(input[1]);
	mux4_8_struct.input2(input[2]);
	mux4_8_struct.input3(input[3]);
	mux4_8_struct.select(select);
	mux4_8_struct.output(output_struct);
	
	
	sc_clock clk("clk", 1, SC_US, 0.5, 5, SC_NS, false);
	
	sc_trace_file *tf = sc_create_vcd_trace_file("wave");
	sc_trace(tf, input[0], "input0");
	sc_trace(tf, input[1], "input1");
	sc_trace(tf, input[2], "input2");
	sc_trace(tf, input[3], "input3");
	sc_trace(tf, select, "select");
	
	sc_trace(tf, output_gate, "output_gate");
	
	sc_trace(tf, output_struct, "output_struct");
	
	
	sc_start(500, SC_NS);
	
	//input0 = "00000000";
	//input1 = "00000000";
	//input2 = "00000000";
	//input3 = "00000000";
	/** select **/
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	sc_start(1, SC_US);
	//	select = dis(gen) % 4;
	//}
	
	//for(size_t it = 0; it < 8; ++it)
	//{
	//	input0 = dis(gen);
	//	input1 = dis(gen);
	//	
	//	
	//	/** alu_func **/
	//	for(size_t it = 0; it < 16; ++it)
	//	{
	//		sc_start(1, SC_US);
	//		select = sc_logic(int(dis(gen) % 2));
	//	}
	//}
	
	/** ugy nez ki, ennek nincs sok hatasa az eredmenyre **/
	//select = "00";
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	input0 = dis(gen);
	//}
	//
	//select = "01";
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	input1 = dis(gen);
	//}
	//
	//select = "10";
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	input2 = dis(gen);
	//}
	//
	//select = "11";
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	input3 = dis(gen);
	//}
	
	
	for(size_t it = 0; it < 4096; ++it)
	{
		
		sc_start(1, SC_US);
		select = dis(gen) % 4;
		
		switch(dis(gen)%5)
		{
			case 0:
				break;
			case 1:
				input[dis(gen)%4] = dis(gen);
				break;
			case 2:
			{ //ez miert kell ide??
				size_t first = dis(gen)%4;
				size_t second = dis(gen)%4;
				while(first == second) second = dis(gen)%4;
				input[first] = dis(gen);
				input[second] = dis(gen);
				break;
			}
			case 3:
			{
				size_t not_sel = dis(gen)%4;
				for(size_t it = 0; it < 4; ++it) if(it != not_sel) input[it] = dis(gen);
				break;
			}
			case 4:
				input[0] = dis(gen);
				input[1] = dis(gen);
				input[2] = dis(gen);
				input[3] = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	
	
	sc_start(1, SC_US);
	
	sc_close_vcd_trace_file(tf);
	return 0;
}
