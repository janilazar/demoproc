#include "../../../gate/mux2_8.hpp"
#include "../../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	sc_report_handler::set_actions (SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
	//sc_report_handler::set_actions (SC_ID_LOGIC_X_TO_BOOL_, SC_DO_NOTHING); // ne sirjon a szaja folyamatosan
	
    std::random_device rd;
    std::mt19937 gen(rd());
	std::uniform_int_distribution<size_t> dis(0,255);

	
	::layout::sunred::xy_coordinates_t layout_size(19,5);
	std::string layer_name = "layer";
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
	
	::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
	
	demoproc_ams_035::mux2_8 mux2_8_gate("mux2_8_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
	demoproc_struct::mux2_8 mux2_8_struct("mux2_8_struct", layer_name, ::layout::sunred::xy_coordinates_t(10,1), nullptr);
	
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(mux2_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(mux2_8_struct.name());
	
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(mux2_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(mux2_8_gate.name());
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(mux2_8_struct.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(mux2_8_struct.name());
	
	sc_signal<sc_lv<8>> input[2], output_gate, output_struct;
	sc_signal<sc_logic> select; 
	
	
	
	mux2_8_gate.input0(input[0]);
	mux2_8_gate.input1(input[1]);
	mux2_8_gate.select(select);
	mux2_8_gate.output(output_gate);
	
	mux2_8_struct.input0(input[0]);
	mux2_8_struct.input1(input[1]);
	mux2_8_struct.select(select);
	mux2_8_struct.output(output_struct);
	
	
	sc_clock clk("clk", 1, SC_US, 0.5, 5, SC_NS, false);
	
	sc_trace_file *tf = sc_create_vcd_trace_file("wave");
	sc_trace(tf, input[0], "input0");
	sc_trace(tf, input[1], "input1");
	sc_trace(tf, select, "select");
	
	sc_trace(tf, output_gate, "output_gate");
	
	sc_trace(tf, output_struct, "output_struct");
	
	
	sc_start(500, SC_NS);
	
	//input0 = "00000000";
	//input1 = "00000000";
	///** select **/
	//for(size_t it = 0; it < 64; ++it)
	//{
	//	sc_start(1, SC_US);
	//	select = sc_logic(int(dis(gen) % 2));
	//}
	
	//for(size_t it = 0; it < 8; ++it)
	//{
	//	input0 = dis(gen);
	//	input1 = dis(gen);
	//	
	//	
	//	/** alu_func **/
	//	for(size_t it = 0; it < 16; ++it)
	//	{
	//		sc_start(1, SC_US);
	//		select = sc_logic(int(dis(gen) % 2));
	//	}
	//}
	
	/** ugy nez ki, ennek nincs osk hatasa az eredmenyre **/
	//select = SC_LOGIC_0;
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	input0 = dis(gen);
	//}
	//
	//select = SC_LOGIC_1;
	//for(size_t it = 0; it < 128; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	input1 = dis(gen);
	//}
	
	for(size_t it = 0; it < 1024; ++it)
	{
		
		sc_start(1, SC_US);
		select = sc_logic(int(dis(gen) % 2));
		
		switch(dis(gen)%3)
		{
			case 0:
				break;
			case 1:
				input[dis(gen)%2] = dis(gen);
				break;
			case 2:
				input[0] = dis(gen);
				input[1] = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	
	
	//for(size_t it = 0; it < 512; ++it)
	//{
	//	
	//	sc_start(1, SC_US);
	//	switch(dis(gen)%4)
	//	{
	//		case 0:
	//			select = sc_logic(int(dis(gen) % 2));
	//			break;
	//		case 1:
	//			select = SC_LOGIC_0;
	//			input0 = dis(gen);
	//			break;
	//		case 2:
	//			select = SC_LOGIC_1;
	//			input1 = dis(gen);
	//			break;
	//		case 3:
	//			select = sc_logic(int(dis(gen) % 2));
	//			input0 = dis(gen);
	//			input1 = dis(gen);
	//			break;
	//		default:
	//			throw("modulo error");
	//	}
	//}
	
	
	sc_start(1, SC_US);
	
	sc_close_vcd_trace_file(tf);
	return 0;
}
