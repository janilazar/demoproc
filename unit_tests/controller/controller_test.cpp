#include "../../gate/controller.hpp"
#include "../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	
    std::random_device rd;
    std::mt19937 gen(rd());
	std::uniform_int_distribution<size_t> dis(0,255);

	
	::layout::sunred::xy_coordinates_t layout_size(83,13);
	std::string layer_name = "layer";
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
	
	::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
	
	demoproc_ams_035::controller controller_gate("controller_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
	demoproc_struct::controller controller_struct("controller_struct", layer_name, ::layout::sunred::xy_coordinates_t(42,1), nullptr);
	
	//::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(controller_gate.name());
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(controller_struct.name());
	
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(controller_gate.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(controller_gate.name());
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(controller_struct.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(controller_struct.name());
	
	sc_lv<4> instruction_array[12] = {
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_i2rf)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_m2rf)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_rf2m)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_sub)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_and)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_or)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_xor)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_jmp)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_bz)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_bz)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_bn)),
			sc_lv<4>(static_cast<int>(demoproc_struct::instruction_decoder::instruction::i_eoj))		
		};
	
	sc_clock clk("clock", 1, SC_US, 0.5, 5, SC_NS, false);
	sc_signal<sc_logic> rst;
	sc_signal<sc_logic> run;
	sc_signal<sc_logic> ss_Z;
	sc_signal<sc_logic> ss_N;
	sc_signal<sc_lv<4>> ss_instruction_code;
	
	
	sc_signal<sc_logic> end_of_job_gate;
	sc_signal<sc_logic> write_data_cache_gate;
	sc_signal<sc_logic> ce_IR_gate;
	sc_signal<sc_logic> ce_IP_gate;
	sc_signal<sc_logic> ce_instruction_code_gate;
	sc_signal<sc_logic> ce_destination_gate;
	sc_signal<sc_logic> ce_OP_A_gate;
	sc_signal<sc_logic> ce_OP_B_gate;
	sc_signal<sc_logic> ce_IMM_gate;
	sc_signal<sc_logic> ce_MAR_gate;
	sc_signal<sc_logic> ce_MDR_gate;
	sc_signal<sc_logic> ce_Z_gate;
	sc_signal<sc_logic> ce_N_gate;
	sc_signal<sc_logic> we_RF_gate;
	sc_signal<sc_logic> sel_IP_gate;
	sc_signal<sc_logic> sel_IP_add_gate;
	sc_signal<sc_lv<2>> sel_ALU_gate;
	
	sc_signal<sc_logic> end_of_job_struct;
	sc_signal<sc_logic> write_data_cache_struct;
	sc_signal<sc_logic> ce_IR_struct;
	sc_signal<sc_logic> ce_IP_struct;
	sc_signal<sc_logic> ce_instruction_code_struct;
	sc_signal<sc_logic> ce_destination_struct;
	sc_signal<sc_logic> ce_OP_A_struct;
	sc_signal<sc_logic> ce_OP_B_struct;
	sc_signal<sc_logic> ce_IMM_struct;
	sc_signal<sc_logic> ce_MAR_struct;
	sc_signal<sc_logic> ce_MDR_struct;
	sc_signal<sc_logic> ce_Z_struct;
	sc_signal<sc_logic> ce_N_struct;
	sc_signal<sc_logic> we_RF_struct;
	sc_signal<sc_logic> sel_IP_struct;
	sc_signal<sc_logic> sel_IP_add_struct;
	sc_signal<sc_lv<2>> sel_ALU_struct;
	
	controller_gate.ce_IR(ce_IR_gate);
	controller_gate.ce_IP(ce_IP_gate);
	controller_gate.ce_destination(ce_destination_gate);
	controller_gate.ce_instruction_code(ce_instruction_code_gate);
	controller_gate.ce_IMM(ce_IMM_gate);
	controller_gate.ce_OP_A(ce_OP_A_gate);
	controller_gate.ce_OP_B(ce_OP_B_gate);
	controller_gate.ce_MAR(ce_MAR_gate);
	controller_gate.ce_MDR(ce_MDR_gate);
	controller_gate.ce_Z(ce_Z_gate);
	controller_gate.ce_N(ce_N_gate);
	controller_gate.sel_ALU(sel_ALU_gate);
	controller_gate.sel_IP(sel_IP_gate);
	controller_gate.sel_IP_add(sel_IP_add_gate);
	controller_gate.write_data_cache(write_data_cache_gate);
	controller_gate.we_RF(we_RF_gate);
	controller_gate.ss_Z(ss_Z);
	controller_gate.ss_N(ss_N);
	controller_gate.ss_instruction_code(ss_instruction_code);
	controller_gate.clock(clk);
	controller_gate.reset(rst);
	controller_gate.run(run);
	controller_gate.end_of_job(end_of_job_gate);
	
	
	controller_struct.ce_IR(ce_IR_struct);
	controller_struct.ce_IP(ce_IP_struct);
	controller_struct.ce_destination(ce_destination_struct);
	controller_struct.ce_instruction_code(ce_instruction_code_struct);
	controller_struct.ce_IMM(ce_IMM_struct);
	controller_struct.ce_OP_A(ce_OP_A_struct);
	controller_struct.ce_OP_B(ce_OP_B_struct);
	controller_struct.ce_MAR(ce_MAR_struct);
	controller_struct.ce_MDR(ce_MDR_struct);
	controller_struct.ce_Z(ce_Z_struct);
	controller_struct.ce_N(ce_N_struct);
	controller_struct.sel_ALU(sel_ALU_struct);
	controller_struct.sel_IP(sel_IP_struct);
	controller_struct.sel_IP_add(sel_IP_add_struct);
	controller_struct.write_data_cache(write_data_cache_struct);
	controller_struct.we_RF(we_RF_struct);
	controller_struct.ss_Z(ss_Z);
	controller_struct.ss_N(ss_N);
	controller_struct.ss_instruction_code(ss_instruction_code);
	controller_struct.clock(clk);
	controller_struct.reset(rst);
	controller_struct.run(run);
	controller_struct.end_of_job(end_of_job_struct);
	
	
	
	//sc_trace_file *tf = sc_create_vcd_trace_file("wave");
	//sc_trace(tf, input0, "input0");
	//sc_trace(tf, input1, "input1");
	//sc_trace(tf, result_gate, "result_gate");
	//sc_trace(tf, result_struct, "result_struct");
	
	//sc_start(40, SC_US);
	
	
	
	for(size_t it = 0; it < 8; ++it)
	{
		//RESET -ET IS VALOSZINUSEG SZERINT MUKODTESSUK
		sc_start(200, SC_NS);
		rst = SC_LOGIC_1;
		
		sc_start(2, SC_US);
		rst = SC_LOGIC_0;
		
		sc_start(200, SC_NS);
		run = SC_LOGIC_1;
		
		sc_start(3, SC_US);
		run = SC_LOGIC_0;
		for(size_t jt = 0; jt < 256; ++jt)
		{
			ss_instruction_code = instruction_array[dis(gen)%12];
			if(dis(gen)%2 == 0)
			{
				ss_Z = sc_logic(int(dis(gen)%2));
				ss_N = sc_logic(int(dis(gen)%2));
			}
			sc_start(12, SC_US);
			
			if(SC_LOGIC_1 == end_of_job_gate)
			{
				sc_start(200, SC_NS);
				run = SC_LOGIC_1;
				
				sc_start(3, SC_US);
				run = SC_LOGIC_0;
			}
		}
		
		//if(dis(gen)%4 == 0)
		//{
		//	sc_start(200, SC_NS);
		//	rst = SC_LOGIC_1;
		//	
		//	sc_start(2, SC_US);
		//	rst = SC_LOGIC_0;
		//}
	}
	sc_start(1, SC_US);
	
	//sc_close_vcd_trace_file(tf);
	return 0;
}
