#include "../../gate/adder_8.hpp"
#include "../../struct/demoproc.hpp"

#include <logitherm>
#include <random>

int sc_main(int argc, char* argv[])
{
	
    std::random_device rd;
    std::mt19937 gen(rd());
	std::uniform_int_distribution<size_t> dis(0,255);

	
	::layout::sunred::xy_coordinates_t layout_size(36,7);
	std::string layer_name = "layer";
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().create_layout(layout_size);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_layer(layer_name);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().add_material("silicon", 156.3, 1.596e6);
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().define_layout_layer_material(layer_name, "silicon");
	
	::logitherm::logitherm_manager::get_logitherm().set_timestep(1.0);	
	::logitherm::logitherm_manager::get_logitherm().get_thermal_engine().set_cell_size(std::make_tuple(3.5e-6,13e-6,1e-6));
	
	demoproc_ams_035::adder_8 adder_8_gate("adder_8_gate", layer_name, ::layout::sunred::xy_coordinates_t(1,1), nullptr);
	demoproc_struct::adder_8 adder_8_struct("adder_8_struct", layer_name, ::layout::sunred::xy_coordinates_t(18,1), nullptr);
	
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(adder_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().add_dissipator_component(adder_8_struct.name());
	
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(adder_8_gate.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(adder_8_gate.name());
	
	::logitherm::logitherm_manager::get_logitherm().trace_component_dissipation(adder_8_struct.name());
	::logitherm::logitherm_manager::get_logitherm().trace_component_activity(adder_8_struct.name());
	
	sc_signal<sc_lv<8>> input0, input1, result_gate, result_struct;
	adder_8_gate.input0(input0);
	adder_8_gate.input1(input1);
	adder_8_gate.result(result_gate);
	
	adder_8_struct.input0(input0);
	adder_8_struct.input1(input1);
	adder_8_struct.result(result_struct);
	
	
	sc_clock clk("clk", 1, SC_US, 0.5, 5, SC_NS, false);
	
	sc_trace_file *tf = sc_create_vcd_trace_file("wave");
	sc_trace(tf, input0, "input0");
	sc_trace(tf, input1, "input1");
	sc_trace(tf, result_gate, "result_gate");
	sc_trace(tf, result_struct, "result_struct");
	
	sc_start(500, SC_NS);
	
	
	input0 = "00000000";
	input1 = "00000000";
	
	for(size_t it = 0; it < 1024; ++it)
	{
		
		sc_start(1, SC_US);
		switch(dis(gen)%4)
		{
			case 0:
				break;
			case 1:
				input0 = dis(gen);
				break;
			case 2:
				input1 = dis(gen);
				break;
			case 3:
				input0 = dis(gen);
				input1 = dis(gen);
				break;
			default:
				throw("modulo error");
		}
	}
	sc_start(1, SC_US);
	
	sc_close_vcd_trace_file(tf);
	return 0;
}
