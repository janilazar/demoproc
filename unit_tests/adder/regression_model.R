setwd("/mnt/storage/egyetem/phd/tema/projects/demoproc/unit_tests/adder")
library(MASS)
# read in CSV tables
activity  <- read.csv("./activity_traces/adder_8_gate_activity_trace.csv")

dissipation <- read.csv("./dissipation_traces/adder_8_gate_dissipation_trace.csv")
uresmatrix <- matrix(0, nrow = 129, ncol = 24)

minden <- data.frame(dissipation,activity)

proba <- lm(formula=minden)
