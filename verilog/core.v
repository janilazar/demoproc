module core
(
	instruction_memory_address,
	instruction_memory_data,
	
	data_memory_data_in,
	data_memory_write_address,
	data_memory_write_enable,
	data_memory_read_address,
	data_memory_data_out,
	
	clk,
	rst,
	run,
	end_of_job
);

	output [7:0] instruction_memory_address;
	input [15:0] instruction_memory_data;
	
	output [7:0] data_memory_data_in;
	output [7:0] data_memory_write_address;
	output data_memory_write_enable;
	output [7:0] data_memory_read_address;
	input [7:0] data_memory_data_out;
	
	input clk;
	input rst;
	input run;
	output end_of_job;
	
	wire [7:0] instruction_memory_address;
	wire [15:0] instruction_memory_data;
	
	wire [7:0] data_memory_data_in;
	wire [7:0] data_memory_write_address;
	wire data_memory_write_enable;
	wire [7:0] data_memory_read_address;
	wire [7:0] data_memory_data_out;
	
	wire clk;
	wire rst;
	wire run;
	wire end_of_job;	

	//CTRL-bol kiindulo jelek
	wire ce_IR_from_CTRL;
	wire ce_IP_from_CTRL;
	wire ce_destination_from_CTRL;
	wire ce_instruction_code_from_CTRL;
	wire ce_IMM_from_CTRL;
	wire ce_OP_A_from_CTRL;
	wire ce_OP_B_from_CTRL;
	wire ce_MAR_from_CTRL;
	wire ce_MDR_from_CTRL;
	wire ce_Z_from_CTRL;
	wire ce_N_from_CTRL;
	wire [1:0] sel_ALU_from_CTRL;
	wire sel_IP_from_CTRL;
	wire sel_IP_ADD_from_CTRL;
	wire write_data_cache_from_CTRL;
	wire we_RF_from_CTRL;
	wire eoj_core0;

	//IR-bol kiindulo jel
	wire [15:0] dout_from_IR;


	//RF-bol kiindulo jelek
	wire [7:0] data_out_a_from_RF;
	wire [7:0] data_out_b_from_RF;
	wire [7:0] data_out_c_from_RF;

	//ADDR_ADD-bol kiindulo jel
	wire [7:0] result_from_ADDR_ADD;

	//OP_B-bol kiindulo jel
	wire [7:0] dout_from_OP_B;

	//MUX_ALU-bol kiindulo jel
	wire [7:0] out_from_MUX_ALU;

	//MUX_ALU dummy jel
	wire [7:0] input3_to_MUX_ALU;

	//OP_A-bol kiindulo jel
	wire [7:0] dout_from_OP_A;


	//DESTINATION-bol kiindulo jel
	wire [3:0] dout_from_DESTINATION;

	//ALU-bol kiindulo jelek
	wire [7:0] result_from_ALU;
	wire z_out_from_ALU;
	wire n_out_from_ALU;

	//Z-bol kiindulo jel
	wire dout_from_Z;

	//N-bol kiindulo jel
	wire dout_from_N;

	//INSTRUCTION_CODE-bol kiindulo jel
	wire [3:0] dout_from_INSTRUCTION_CODE;

	//INSTRUCTION_DECODER-bol kiindulo jel
	wire[2:0] alu_func_from_INSTRUCTION_DECODER;

	//MAR-bol kiindulo jel
	wire[7:0] dout_from_MAR;

	//MDR-bol kiindulo jel
	wire [7:0] dout_from_MDR;

	//IMM-bol kiindulo jel
	wire [7:0] dout_from_IMM;
	//MUX_IP_ADD-bol kiindulo jel
	wire [7:0] out_from_MUX_IP_ADD;

	//MUX_IP_ADD dummy jel
	wire[7:0] input0_to_MUX_IP_ADD;


	//IP-bol kiindulo jel
	wire [7:0] dout_from_IP;

	//IP_ADD-bol kiindulo jel
	wire [7:0] result_from_IP_ADD;

	//MUX_IP-bol kiindulo jel
	wire [7:0] out_from_MUX_IP;
	//MUX_IP dummy jel
	wire [7:0] input0_to_MUX_IP;



	controller cntrl
	(
		.ce_IR(ce_IR_from_CTRL),
		.ce_IP(ce_IP_from_CTRL),
		.ce_destination(ce_destination_from_CTRL),
		.ce_instruction_code(ce_instruction_code_from_CTRL),
		.ce_IMM(ce_IMM_from_CTRL),
		.ce_OP_A(ce_OP_A_from_CTRL),
		.ce_OP_B(ce_OP_B_from_CTRL),
		.ce_MAR(ce_MAR_from_CTRL),
		.ce_MDR(ce_MDR_from_CTRL),
		.ce_Z(ce_Z_from_CTRL),
		.ce_N(ce_N_from_CTRL),
		.sel_ALU(sel_ALU_from_CTRL),
		.sel_IP(sel_IP_from_CTRL),
		.sel_IP_add(sel_IP_ADD_from_CTRL),
		.write_data_cache(write_data_cache_from_CTRL),
		.we_RF(we_RF_from_CTRL),
		.ss_Z(dout_from_Z),
		.ss_N(dout_from_N),
		.ss_instruction_code(dout_from_INSTRUCTION_CODE),
		.clock(clk),
		.reset(rst),
		.run(run),
		.end_of_job(end_of_job)
	);

	//IR
	register_16 ir
	(
		.\input (instruction_memory_data), //data_out_a_from_INSTRUCTION_CACHE
		.enable(ce_IR_from_CTRL),
		.\output (dout_from_IR),
		.clock(clk),
		.reset(rst)
	);

	//RF
	regfile rf
	(
		.read_address_a(dout_from_IR[7:4]),
		.read_address_b(dout_from_IR[3:0]),
		.read_address_c(dout_from_IR[11:8]),
		.write_enable(we_RF_from_CTRL),
		.write_address(dout_from_DESTINATION),
		.data_in(result_from_ALU),
		.data_out_a(data_out_a_from_RF),
		.data_out_b(data_out_b_from_RF),
		.data_out_c(data_out_c_from_RF),
		.clock(clk)
	);

	//ADDR_ADD
	adder_8 addr_add
	(
		.input0(data_out_a_from_RF),
		.input1(data_out_b_from_RF),
		.result(result_from_ADDR_ADD)
	);

	//OP_B
	register_8 op_b
	(
		.\input (data_out_b_from_RF),
		.enable(ce_OP_B_from_CTRL),
		.\output (dout_from_OP_B),
		.clock(clk),
		.reset(rst)
	);
	//MUX_ALU
	mux4_8 mux_alu
	(
		.input0(dout_from_IMM),
		.input1(data_memory_data_out), //data_out_a_from_DATA_CACHE
		.input2(dout_from_OP_B),
		.input3(input3_to_MUX_ALU),
		.select(sel_ALU_from_CTRL),
		.\output (out_from_MUX_ALU)
	);

	//OP_A
	register_8 op_a
	(
		.\input (data_out_a_from_RF),
		.enable(ce_OP_A_from_CTRL),
		.\output (dout_from_OP_A),
		.clock(clk),
		.reset(rst)
	);

	//DESTINATION
	register_4 dest
	(
		.\input (dout_from_IR[11:8]),
		.enable(ce_destination_from_CTRL),
		.\output (dout_from_DESTINATION),
		.clock(clk),
		.reset(rst)
	);

	//ALU
	alu_8 alu
	(
		.alu_function(alu_func_from_INSTRUCTION_DECODER),
		.inputa(dout_from_OP_A),
		.inputb(out_from_MUX_ALU),
		.\output (result_from_ALU),
		.z_out(z_out_from_ALU),
		.n_out(n_out_from_ALU)
	);

	//Z
	register_1 z
	(
		.\input (z_out_from_ALU),
		.enable(ce_Z_from_CTRL),
		.\output (dout_from_Z),
		.clock(clk),
		.reset(rst)
	);

	//N
	register_1 n
	(
		.\input (n_out_from_ALU),
		.enable(ce_N_from_CTRL),
		.\output (dout_from_N),
		.clock(clk),
		.reset(rst)
	);

	//INSTRUCTION_CODE
	register_4 inst_code
	(
		.\input (dout_from_IR[15:12]),
		.enable(ce_instruction_code_from_CTRL),
		.\output (dout_from_INSTRUCTION_CODE),
		.clock(clk),
		.reset(rst)
	);

	//INSTRUCTION_DECODER
	instruction_decoder inst_dec
	(
		.opcode(dout_from_INSTRUCTION_CODE),
		.alu_func(alu_func_from_INSTRUCTION_DECODER)
	);

	//MAR
	register_8 mar
	(
		.\input (result_from_ADDR_ADD),
		.enable(ce_MAR_from_CTRL),
		.\output (dout_from_MAR),
		.clock(clk),
		.reset(rst)
	);

	//MDR
	register_8 mdr
	(
		.\input (data_out_c_from_RF),
		.enable(ce_MDR_from_CTRL),
		.\output (dout_from_MDR),
		.clock(clk),
		.reset(rst)
	);

	//IMM
	register_8 imm
	(
		.\input (dout_from_IR[7:0]),
		.enable(ce_IMM_from_CTRL),
		.\output (dout_from_IMM),
		.clock(clk),
		.reset(rst)
	);

	//MUX_IP_ADD
	mux2_8 mux_ip_add
	(
		.input0(input0_to_MUX_IP_ADD),
		.input1(dout_from_IMM),
		.select(sel_IP_ADD_from_CTRL),
		.\output (out_from_MUX_IP_ADD)
	);

	//IP
	register_8 ip
	(
		.\input (out_from_MUX_IP),
		.enable(ce_IP_from_CTRL),
		.\output (dout_from_IP),
		.clock(clk),
		.reset(rst)
	);

	//IP_ADD
	adder_8 ip_add
	(
		.input0(out_from_MUX_IP_ADD),
		.input1(dout_from_IP),
		.result(result_from_IP_ADD)
	);

	//MUX_IP
	mux2_8 mux_ip
	(
		.input0(input0_to_MUX_IP),
		.input1(result_from_IP_ADD),
		.select(sel_IP_from_CTRL),
		.\output (out_from_MUX_IP)
	);


	assign input0_to_MUX_IP_ADD = 8'b00000001;
	assign input0_to_MUX_IP = 8'b00000000;
	assign input3_to_MUX_ALU = 8'b00000000;

	assign instruction_memory_address = dout_from_IP;
	//assign instruction_memory_data = data_out_a_from_INSTRUCTION_CACHE;


	assign data_memory_data_in = dout_from_MDR;
	assign data_memory_write_address = dout_from_MAR;
	assign data_memory_write_enable = write_data_cache_from_CTRL;
	assign data_memory_read_address = dout_from_MAR;
	//assign data_memory_data_out = data_out_a_from_DATA_CACHE;

endmodule
