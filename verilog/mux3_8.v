
// Generated by Cadence Encounter(R) RTL Compiler RC11.21 - v11.20-s012_1

// Verification Directory fv/mux3 

module mux3_8(input0, input1, input2, select, \output );
  input [7:0] input0, input1, input2;
  input [1:0] select;
  output [7:0] \output ;
  wire [7:0] input0, input1, input2;
  wire [1:0] select;
  wire [7:0] \output ;
  MUX31 mux_0(.A (input0[0]), .B (input1[0]), .C (input2[0]), .S0 (select[0]), .S1(select[1]), .Q (\output [0]));
  MUX31 mux_1(.A (input0[1]), .B (input1[1]), .C (input2[1]), .S0 (select[0]), .S1(select[1]), .Q (\output [1]));
  MUX31 mux_2(.A (input0[2]), .B (input1[2]), .C (input2[2]), .S0 (select[0]), .S1(select[1]), .Q (\output [2]));
  MUX31 mux_3(.A (input0[3]), .B (input1[3]), .C (input2[3]), .S0 (select[0]), .S1(select[1]), .Q (\output [3]));
  MUX31 mux_4(.A (input0[4]), .B (input1[4]), .C (input2[4]), .S0 (select[0]), .S1(select[1]), .Q (\output [4]));
  MUX31 mux_5(.A (input0[5]), .B (input1[5]), .C (input2[5]), .S0 (select[0]), .S1(select[1]), .Q (\output [5]));
  MUX31 mux_6(.A (input0[6]), .B (input1[6]), .C (input2[6]), .S0 (select[0]), .S1(select[1]), .Q (\output [6]));
  MUX31 mux_7(.A (input0[7]), .B (input1[7]), .C (input2[7]), .S0 (select[0]), .S1(select[1]), .Q (\output [7]));
endmodule

