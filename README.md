Demo processzor SystemC-ben implementálva logi-termikus szimulációhoz SUNRED és SloTh termikus motorhoz.
A kapuszintű szimulációhoz szükséges az ams035 cellakönyvtárat a demoproc könyvtár mellé helyezni (vagy a Makefile módosítása szükséges).
A két termikus motor adapterének függvényei kompatibilisek egymással (csak a névtér különbözik), de az egyszerűség kedvéért külön branch áll rendelkezésre sunred és sloth motorhoz.
